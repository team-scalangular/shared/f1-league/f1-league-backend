package com.gitlab.f1league.f1LeagueBackend.raceResult.api;

import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.api.RaceResultsApiDelegate;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.DriverRaceResult;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.services.RaceResultModelConverter;
import com.gitlab.f1league.f1LeagueBackend.raceResult.services.RaceResultService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class RaceResultApiImplementation implements RaceResultsApiDelegate {

    private final RaceResultService raceResultService;
    private final RaceResultModelConverter raceResultModelConverter;
    private final AuthorizationService authorizationService;

    @Autowired
    public RaceResultApiImplementation(RaceResultService raceResultService,
                                       RaceResultModelConverter raceResultModelConverter,
                                       AuthorizationService authorizationService) {
        this.raceResultService = raceResultService;
        this.raceResultModelConverter = raceResultModelConverter;
        this.authorizationService = authorizationService;
    }

    @Override
    public ResponseEntity<Void> deleteRaceResultsFromRaceByRaceIdAndDriverId(String driverId,
                                                                             String raceId,
                                                                             String leagueEditorToken,
                                                                             String leagueId) {
        log.info("request to delete RaceResults from race with id {} and driver with id {}", raceId,
                driverId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                raceResultService.deleteRaceResultRaceIdAndDriverId(driverId, raceId);
                return ResponseEntity.ok().build();
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @Override
    public ResponseEntity<List<DriverRaceResult>> getRaceResultsByRaceId(String raceId,
                                                                         String driverId) {
        List<DriverRaceResultEntity> raceResults = null;
        if (driverId == null) {
            log.info("request to get all RaceResults for race with id {}", raceId);
            raceResults = raceResultService.getRaceResultsByRaceId(raceId);
        } else {
            log.info("request to get RaceResults for race with id {} and driver with id {}", raceId,
                    driverId);
            raceResults = raceResultService.getRaceResultsByRaceIdAndDriverId(raceId, driverId);
        }

        if (raceResults.isEmpty())
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body(new ArrayList<>());

        List<DriverRaceResult> responseBody = raceResults.stream()
                .map(raceResultModelConverter::internalModelToTransferModel)
                .collect(Collectors.toList());

        return ResponseEntity.ok(responseBody);
    }

    @Override
    public ResponseEntity<DriverRaceResult> patchRaceResult(String leagueEditorToken,
                                                            DriverRaceResult driverRaceResult,
                                                            String leagueId) {
        log.info("request to patch RaceResult {}", driverRaceResult);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                return performPatchRaceResult(driverRaceResult);
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    private ResponseEntity<DriverRaceResult> performPatchRaceResult(
            DriverRaceResult driverRaceResult) {
        DriverRaceResultEntity driverRaceResultEntity =
                raceResultService.getRaceResultByDriverRaceResultEntityId(
                        driverRaceResult.getRaceId().toString());

        if (driverRaceResultEntity == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        driverRaceResultEntity =
                raceResultService.updateRaceResult(driverRaceResultEntity, driverRaceResult);

        if (driverRaceResultEntity == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        driverRaceResultEntity = raceResultService.save(driverRaceResultEntity);

        return ResponseEntity
                .ok(raceResultModelConverter.internalModelToTransferModel(driverRaceResultEntity));
    }

    @Override
    public ResponseEntity<List<DriverRaceResult>> postRaceResultsToLeagueByLeagueIdAndRaceId(
            String leagueId, String raceId, String leagueEditorToken,
            List<DriverRaceResult> driverRaceResult) {
        log.info("request to post list of RaceResults {} to league with id {} and race with id {}",
                driverRaceResult, leagueId, raceId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                return performPostRaceResult(driverRaceResult, leagueId, raceId);
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    private ResponseEntity<List<DriverRaceResult>> performPostRaceResult(
            List<DriverRaceResult> driverRaceResults, String leagueId, String raceId) {
        List<DriverRaceResultEntity> driverRaceResultEntities = driverRaceResults.stream()
                .map(driverRaceResult1 -> raceResultService
                        .buildDriverRaceResultEntityFromPost(driverRaceResult1, leagueId, raceId))
                .collect(Collectors.toList());

        // will be bigger then 0 when bad request (because buildDriverRaceResultEntityFromPost return null if this is the case)
        long nullElementsCount = driverRaceResultEntities.stream()
                .filter(Objects::isNull)
                .count();

        if (nullElementsCount > 0)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        // delete raceResultsToOverride
        driverRaceResultEntities.stream()
                .filter(raceResultService::overrideRaceResult)
                .forEach(driverRaceResultEntity -> raceResultService
                        .deleteRaceResultRaceIdAndDriverId(
                                driverRaceResultEntity.getDriver().getDriverId(),
                                driverRaceResultEntity.getRaceEntity().getRaceId()));

        List<DriverRaceResultEntity> savedRaceResultEntities =
                raceResultService.saveAll(driverRaceResultEntities);

        if (!savedRaceResultEntities.isEmpty())
            savedRaceResultEntities.get(0).getRaceEntity().setHasBeenDriven(true);

        return ResponseEntity.ok(savedRaceResultEntities.stream()
                .map(raceResultModelConverter::internalModelToTransferModel)
                .collect(Collectors.toList()));
    }
}
