package com.gitlab.f1league.f1LeagueBackend.raceResult.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverClassificationCacheService;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.DriverRaceResult;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.repository.RaceResultRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Service
public class RaceResultService {

    private final RaceResultRepository raceResultRepository;
    private final DriverService driverService;
    private final RaceService raceService;
    private final LeagueService leagueService;
    private final DriverClassificationCacheService driverClassificationCacheService;

    @Autowired
    public RaceResultService(RaceResultRepository raceResultRepository, DriverService driverService,
                             RaceService raceService, LeagueService leagueService,
                             DriverClassificationCacheService driverClassificationCacheService) {
        this.raceResultRepository = raceResultRepository;
        this.driverService = driverService;
        this.raceService = raceService;
        this.leagueService = leagueService;
        this.driverClassificationCacheService = driverClassificationCacheService;
    }

    @PostConstruct
    public void init() {
        raceService.setRaceResultService(this);
        driverService.setRaceResultService(this);
    }

    public List<DriverRaceResultEntity> getRaceResultsByRaceId(String raceId) {
        RaceEntity raceEntity = raceService.getRaceByRaceId(raceId);

        if (raceEntity == null)
            return List.of();

        return raceResultRepository.findAllByRaceEntity(raceEntity);
    }

    public List<DriverRaceResultEntity> getRaceResultsByRaceIdAndDriverId(String raceId,
                                                                          String driverId) {
        RaceEntity raceEntity = raceService.getRaceByRaceId(raceId);
        DriverEntity driverEntity = driverService.getDriverByDriverId(driverId);

        return raceResultRepository.findAllByRaceEntityAndDriver(raceEntity, driverEntity);
    }

    public Integer calculatePointsForRaceResult(DriverRaceResultEntity driverRaceResultEntity) {
        Integer points = this.getPointsForPosition(driverRaceResultEntity.getEndpostion());
        if (raceResultEntityHasFastestLap(driverRaceResultEntity))
            points++;

        return points;
    }

    public boolean overrideRaceResult(DriverRaceResultEntity driverRaceResultEntityToCheck) {
        return raceResultRepository.findAllByDriver(driverRaceResultEntityToCheck.getDriver())
                .stream()
                .anyMatch(driverRaceResultEntity -> driverRaceResultEntity.getRaceEntity()
                        .equals(driverRaceResultEntityToCheck.getRaceEntity()));
    }

    public boolean raceResultEntityHasFastestLap(
            DriverRaceResultEntity raceResultEntityToCheck) {
        if (raceResultEntityToCheck.getEndpostion() > 10)
            return false;

        BigDecimal fastestLap = raceResultEntityToCheck.getFastestLap();

        if (fastestLap == null)
            return false;

        long fasterLapsCount =
                this.getRaceResultsByRaceId(
                        raceResultEntityToCheck.getRaceEntity().getRaceId())
                        .stream()
                        .filter(driverRaceResultEntity -> !driverRaceResultEntity
                                .equals(raceResultEntityToCheck))
                        .filter(driverRaceResultEntity ->
                                driverRaceResultEntity.getEndpostion() <= 10)
                        .filter(driverRaceResultEntity ->
                                driverRaceResultEntity.getFastestLap().compareTo(fastestLap) < 0)
                        .count();

        return fasterLapsCount <= 0;
    }

    public Integer getPointsForPosition(Integer position) {
        switch (position) {
            case 1:
                return 25;
            case 2:
                return 18;
            case 3:
                return 15;
            case 4:
                return 12;
            case 5:
                return 10;
            case 6:
                return 8;
            case 7:
                return 6;
            case 8:
                return 4;
            case 9:
                return 2;
            case 10:
                return 1;
            default:
                return 0;
        }
    }

    public List<DriverRaceResultEntity> saveAll(
            List<DriverRaceResultEntity> driverRaceResultEntities) {
        List<DriverRaceResultEntity> savedRaceResultEntities =
                raceResultRepository.saveAll(driverRaceResultEntities);
        log.info("saved DriverRaceResultEntities {} to database", savedRaceResultEntities);
        driverRaceResultEntities.forEach(driverRaceResultEntity -> driverClassificationCacheService
                .evalDriverClassification(driverRaceResultEntity.getRaceEntity().getLeague()));
        return savedRaceResultEntities;
    }

    public List<DriverRaceResultEntity> getAllResultsForGivenDriverEntity(
            DriverEntity driverEntityToGetResultsFrom) {
        return raceResultRepository.findAllByDriver(driverEntityToGetResultsFrom);
    }

    public DriverRaceResultEntity buildDriverRaceResultEntityFromPost(
            DriverRaceResult driverRaceResult, String leagueId, String raceId) {
        if (isInvalid(driverRaceResult))
            return null;

        LeagueEntity leagueEntityToAddResultsToo = leagueService.getLeagueById(leagueId);
        RaceEntity raceToAddResultsToo = raceService.getRaceByRaceId(raceId);
        DriverEntity driverEntity =
                driverService.getDriverByDriverId(driverRaceResult.getDriverId().toString());

        if (leagueEntityToAddResultsToo == null || raceToAddResultsToo == null ||
                driverEntity == null)
            return null;

        if (!raceToAddResultsToo.getLeague().equals(leagueEntityToAddResultsToo))
            return null;

        return DriverRaceResultEntity.builder()
                .carInRace(driverRaceResult.getCarInRace())
                .driver(driverEntity)
                .endpostion(driverRaceResult.getEndpostion())
                .fastestLap(driverRaceResult.getFastestLap())
                .startposition(driverRaceResult.getStartposition())
                .raceEntity(raceToAddResultsToo)
                .build();
    }

    public boolean isInvalid(
            DriverRaceResult driverRaceResult) {
        if (driverRaceResult.getDriverId() == null)
            return true;

        if (driverRaceResult.getStartposition() == null)
            return true;

        if (driverRaceResult.getEndpostion() == null)
            return true;

        if (driverRaceResult.getRaceId() == null)
            return true;

        return driverRaceResult.getCarInRace() == null;
    }

    @Transactional
    public void deleteRaceResultRaceIdAndDriverId(String driverId, String raceId) {
        RaceEntity raceEntity = raceService.getRaceByRaceId(raceId);
        DriverEntity driverEntity = driverService.getDriverByDriverId(driverId);

        raceResultRepository.deleteByRaceEntityAndDriver(raceEntity, driverEntity);
        driverClassificationCacheService.evalDriverClassification(raceEntity.getLeague());
        log.info("deleted DriverRaceResultEntity {} by it DriverEntity {}", raceEntity,
                driverEntity);
    }

    public DriverRaceResultEntity getRaceResultByDriverRaceResultEntityId(
            String driverRaceResultId) {
        return raceResultRepository.findById(driverRaceResultId).orElse(null);
    }

    public void deleteRaceResultByRaceResult(
            DriverRaceResultEntity driverRaceResultToDeleteEntity) {
        raceResultRepository.delete(driverRaceResultToDeleteEntity);
        driverClassificationCacheService.evalDriverClassification(
                driverRaceResultToDeleteEntity.getRaceEntity().getLeague());
        log.info("deleted DriverRaceResultEntity {}", driverRaceResultToDeleteEntity);
    }

    public DriverRaceResultEntity save(DriverRaceResultEntity driverRaceResultEntityToSave) {
        DriverRaceResultEntity driverRaceResultEntity =
                raceResultRepository.save(driverRaceResultEntityToSave);
        log.info("saved DriverRaceResultEntity {} to database", driverRaceResultEntity);
        return driverRaceResultEntity;
    }

    public DriverRaceResultEntity updateRaceResult(
            DriverRaceResultEntity driverRaceResultEntityToUpdate,
            DriverRaceResult driverRaceResultWithUpdates) {
        if (isInvalid(driverRaceResultWithUpdates))
            return null;

        DriverEntity driverEntity = driverService
                .getDriverByDriverId(driverRaceResultWithUpdates.getDriverId().toString());

        if (driverEntity == null)
            return null;

        RaceEntity raceEntity =
                raceService.getRaceByRaceId(driverRaceResultWithUpdates.getRaceId().toString());

        if (raceEntity == null)
            return null;

        driverRaceResultEntityToUpdate.setCarInRace(driverRaceResultWithUpdates.getCarInRace());
        driverRaceResultEntityToUpdate.setDriver(driverEntity);
        driverRaceResultEntityToUpdate.setEndpostion(driverRaceResultWithUpdates.getEndpostion());
        driverRaceResultEntityToUpdate.setFastestLap(driverRaceResultWithUpdates.getFastestLap());
        driverRaceResultEntityToUpdate.setRaceEntity(raceEntity);
        driverRaceResultEntityToUpdate
                .setStartposition(driverRaceResultWithUpdates.getStartposition());

        return driverRaceResultEntityToUpdate;
    }
}
