package com.gitlab.f1league.f1LeagueBackend.raceResult.entity;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
@EqualsAndHashCode
@Table(name = "raceResults")
@ToString
@Proxy(lazy = false)
public class DriverRaceResultEntity {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String driverRaceResultId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "driver_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private DriverEntity driver;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "race_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private RaceEntity raceEntity;

    @Column(precision = 10, scale = 3)
    private BigDecimal fastestLap;
    private Integer startposition;
    private Integer endpostion;
    private Car carInRace;
}
