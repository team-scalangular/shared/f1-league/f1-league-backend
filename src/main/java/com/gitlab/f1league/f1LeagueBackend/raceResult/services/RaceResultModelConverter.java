package com.gitlab.f1league.f1LeagueBackend.raceResult.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.etc.ModelConverter;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.DriverRaceResult;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RaceResultModelConverter
        implements ModelConverter<DriverRaceResultEntity, DriverRaceResult> {

    private final DriverService driverService;
    private final RaceService raceService;
    private final RaceResultService raceResultService;

    @Autowired
    public RaceResultModelConverter(DriverService driverService, RaceService raceService,
                                    RaceResultService raceResultService) {
        this.driverService = driverService;
        this.raceService = raceService;
        this.raceResultService = raceResultService;
    }

    @Override
    public DriverRaceResultEntity transferModelToInternalModel(DriverRaceResult driverRaceResult) {
        DriverEntity raceResultDriver =
                driverService.getDriverByDriverId(driverRaceResult.getDriverId().toString());
        RaceEntity raceEntity =
                raceService.getRaceByRaceId(driverRaceResult.getRaceId().toString());

        return DriverRaceResultEntity.builder()
                .carInRace(driverRaceResult.getCarInRace())
                .driver(raceResultDriver)
                .driverRaceResultId(driverRaceResult.getDriverRaceResultId().toString())
                .endpostion(driverRaceResult.getEndpostion())
                .fastestLap(driverRaceResult.getFastestLap())
                .raceEntity(raceEntity)
                .startposition(driverRaceResult.getStartposition())
                .build();
    }

    @Override
    public DriverRaceResult internalModelToTransferModel(
            DriverRaceResultEntity driverRaceResultEntity) {
        DriverRaceResult convertedDriverRaceResult = new DriverRaceResult();
        convertedDriverRaceResult
                .setRaceId(UUID.fromString(driverRaceResultEntity.getRaceEntity().getRaceId()));
        convertedDriverRaceResult.setFastestLap(driverRaceResultEntity.getFastestLap());
        convertedDriverRaceResult.setStartposition(driverRaceResultEntity.getStartposition());
        convertedDriverRaceResult.setEndpostion(driverRaceResultEntity.getEndpostion());
        convertedDriverRaceResult.setDriverRaceResultId(
                UUID.fromString(driverRaceResultEntity.getDriverRaceResultId()));
        convertedDriverRaceResult
                .setDriverId(UUID.fromString(driverRaceResultEntity.getDriver().getDriverId()));
        convertedDriverRaceResult.setCarInRace(driverRaceResultEntity.getCarInRace());
        convertedDriverRaceResult.setDriverName(
                String.format("%s %s", driverRaceResultEntity.getDriver().getName(),
                        driverRaceResultEntity.getDriver().getSurename()));
        convertedDriverRaceResult.setPoints(raceResultService
                .getPointsForPosition(driverRaceResultEntity.getEndpostion()));

        if (raceResultService.raceResultEntityHasFastestLap(driverRaceResultEntity)) {
            convertedDriverRaceResult.setPoints(convertedDriverRaceResult.getPoints() + 1);
            convertedDriverRaceResult.setHasFastestLap(true);
        } else {
            convertedDriverRaceResult.setHasFastestLap(false);
        }

        return convertedDriverRaceResult;
    }
}
