package com.gitlab.f1league.f1LeagueBackend.raceResult.repository;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RaceResultRepository extends JpaRepository<DriverRaceResultEntity, String> {
    List<DriverRaceResultEntity> findAllByRaceEntityAndDriver(RaceEntity raceEntity,
                                                              DriverEntity driverEntity);

    List<DriverRaceResultEntity> findAllByRaceEntity(RaceEntity raceEntity);

    List<DriverRaceResultEntity> findAllByDriver(DriverEntity driverEntity);

    void deleteByRaceEntityAndDriver(RaceEntity raceEntity, DriverEntity driverEntity);
}
