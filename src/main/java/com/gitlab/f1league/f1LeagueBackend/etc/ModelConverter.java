package com.gitlab.f1league.f1LeagueBackend.etc;

public interface ModelConverter<E, T> {
    E transferModelToInternalModel(T t);

    T internalModelToTransferModel(E e);
}
