package com.gitlab.f1league.f1LeagueBackend.etc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfiguration {
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedHeaders("*")
                        .exposedHeaders("X-LeagueEditorToken", "LeagueEditorToken",
                                "Access-Control-Expose-Headers")
                        .allowedMethods("POST", "PUT", "GET", "PATCH", "DELETE");
            }
        };
    }
}
