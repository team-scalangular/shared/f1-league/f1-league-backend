package com.gitlab.f1league.f1LeagueBackend.etc;

import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationService {
    private final LeagueService leagueService;

    @Autowired
    public AuthorizationService(LeagueService leagueService) {
        this.leagueService = leagueService;
    }

    public State isValidToken(String token, LeagueEntity leagueEntityToCheck) {
        return token.equals(leagueEntityToCheck.getLeagueEditorToken()) ? State.AUTHORIZED :
                State.UNAUTHORIZED;
    }

    public State isValidToken(String token, String leagueId) {
        LeagueEntity leagueEntity = leagueService.getLeagueById(leagueId);

        return (leagueEntity == null) ? State.UNKNOWN : isValidToken(token, leagueEntity);
    }

    public enum State {
        UNAUTHORIZED, UNKNOWN, AUTHORIZED
    }
}
