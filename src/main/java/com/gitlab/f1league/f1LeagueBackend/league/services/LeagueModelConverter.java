package com.gitlab.f1league.f1LeagueBackend.league.services;

import com.gitlab.f1league.f1LeagueBackend.etc.ModelConverter;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.League;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class LeagueModelConverter implements ModelConverter<LeagueEntity, League> {

    private final TeamService teamService;
    private final RaceService raceService;

    @Autowired
    public LeagueModelConverter(TeamService teamService, RaceService raceService) {
        this.teamService = teamService;
        this.raceService = raceService;
    }

    @Override
    public LeagueEntity transferModelToInternalModel(@NonNull League league) {
        return LeagueEntity.builder()
                .name(league.getName())
                .leagueId(league.getLeagueId().toString())
                .build();
    }

    @Override
    public League internalModelToTransferModel(@NonNull LeagueEntity leagueEntity) {
        List<String> teamIds = teamService.getAllTeamsByLeague(leagueEntity).stream()
                .map(TeamEntity::getTeamId)
                .collect(Collectors.toList());
        List<String> raceIds = raceService.getAllRacesByLeague(leagueEntity).stream()
                .map(RaceEntity::getRaceId)
                .collect(Collectors.toList());

        League league = new League();
        league.setLeagueId(UUID.fromString(leagueEntity.getLeagueId()));
        league.setName(leagueEntity.getName());
        league.setListOfTeamIds(teamIds);
        league.setListOfRaceIds(raceIds);

        return league;
    }
}
