package com.gitlab.f1league.f1LeagueBackend.league.repository;

import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LeagueRepository extends JpaRepository<LeagueEntity, String> {
}
