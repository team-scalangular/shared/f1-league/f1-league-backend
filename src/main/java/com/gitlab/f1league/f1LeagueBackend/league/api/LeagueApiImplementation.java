package com.gitlab.f1league.f1LeagueBackend.league.api;

import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.api.LeagueApiDelegate;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.League;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueModelConverter;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class LeagueApiImplementation implements LeagueApiDelegate {

    private final LeagueService leagueService;
    private final LeagueModelConverter leagueModelConverter;
    private final AuthorizationService authorizationService;
    private final RaceService raceService;

    @Autowired
    public LeagueApiImplementation(LeagueService leagueService,
                                   LeagueModelConverter leagueModelConverter,
                                   RaceService raceService,
                                   AuthorizationService authorizationService) {
        this.leagueService = leagueService;
        this.leagueModelConverter = leagueModelConverter;
        this.raceService = raceService;
        this.authorizationService = authorizationService;
    }

    @Override
    public ResponseEntity<Void> deleteLeagueByLeagueId(String leagueId, String leagueEditorToken) {
        log.info("request to delete league by id {}", leagueId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                leagueService.deleteLeagueBuId(leagueId);
                return ResponseEntity.ok().build();
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    // todo test
    @Override
    public ResponseEntity<List<League>> getLeagues(String leagueId) {
        List<LeagueEntity> leagueEntities = new ArrayList<>();

        if (leagueId == null) {
            log.info("request to get all leagues");
            leagueEntities = leagueService.getAllLeagues();
        } else {
            log.info("request to get league by id {}", leagueId);
            LeagueEntity foundLeagueEntity = leagueService.getLeagueById(leagueId);
            if (foundLeagueEntity != null)
                leagueEntities.add(foundLeagueEntity);
        }

        if (leagueEntities.isEmpty())
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        List<League> convertedLeagues =
                leagueService.sortLeaguesBasedOnLatestNextEvent(leagueEntities).stream()
                        .map(leagueModelConverter::internalModelToTransferModel)
                        .collect(Collectors.toList());

        return ResponseEntity.ok(convertedLeagues);
    }

    @Override
    public ResponseEntity<League> patchLeague(String leagueEditorToken, League league) {
        log.info("request to patch league");

        switch (authorizationService
                .isValidToken(leagueEditorToken, league.getLeagueId().toString())) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                return performPatch(league);
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return null;
    }

    private ResponseEntity<League> performPatch(League leagueWithUpdates) {
        LeagueEntity leagueEntityToUpdate =
                leagueService.getLeagueById(leagueWithUpdates.getLeagueId().toString());

        if (leagueEntityToUpdate == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        leagueEntityToUpdate = leagueService.updateLeagueEntity(leagueWithUpdates);

        if (leagueEntityToUpdate == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        leagueEntityToUpdate = leagueService.saveLeagueEntity(leagueEntityToUpdate);

        return ResponseEntity
                .ok(leagueModelConverter.internalModelToTransferModel(leagueEntityToUpdate));
    }

    @Override
    public ResponseEntity<League> postLeague(League league) {
        log.info("request to post league {}", league);
        LeagueEntity leagueEntityToCreate = leagueService.buildLeagueEntityFromPost(league);

        if (leagueEntityToCreate == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        leagueEntityToCreate = leagueService.saveLeagueEntity(leagueEntityToCreate);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("X-LeagueEditorToken", leagueEntityToCreate.getLeagueEditorToken());
        responseHeaders.set("Access-Control-Expose-Headers", "*");


        League savedLeague =
                leagueModelConverter.internalModelToTransferModel(leagueEntityToCreate);

        return ResponseEntity.status(HttpStatus.OK).headers(responseHeaders).body(savedLeague);
    }
}
