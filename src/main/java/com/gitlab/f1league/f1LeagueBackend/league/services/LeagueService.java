package com.gitlab.f1league.f1LeagueBackend.league.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverClassificationCacheService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.League;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.repository.LeagueRepository;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Slf4j
@Service
public class LeagueService {

    private final LeagueRepository leagueRepository;
    private final TeamService teamService;
    private final RaceService raceService;
    private final DriverClassificationCacheService driverClassificationCacheService;

    @Autowired
    public LeagueService(LeagueRepository leagueRepository, TeamService teamService,
                         RaceService raceService,
                         DriverClassificationCacheService driverClassificationCacheService) {
        this.leagueRepository = leagueRepository;
        this.teamService = teamService;
        this.raceService = raceService;
        this.driverClassificationCacheService = driverClassificationCacheService;
    }

    @PostConstruct
    public void init() {
        raceService.setLeagueService(this);
        teamService.setLeagueService(this);
    }

    public LeagueEntity getLeagueById(@NonNull String leagueId) {
        return leagueRepository.findById(leagueId).orElse(null);
    }

    public String generateLeagueEditorToken() {
        int leftLimit = 65; // numeral 'A'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 7;

        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i < 91 || i > 96))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public void deleteLeagueBuId(@NonNull String leagueId) {
        LeagueEntity leagueEntityToDelete = getLeagueById(leagueId);

        if (leagueEntityToDelete == null)
            return;

        teamService.getAllTeamsByLeague(leagueEntityToDelete)
                .forEach(teamEntity -> teamService.deleteTeamById(teamEntity.getTeamId()));

        raceService.getAllRacesByLeague(leagueEntityToDelete)
                .forEach(raceEntity -> raceService.deleteRaceById(raceEntity.getRaceId()));

        leagueRepository.deleteById(leagueId);
        driverClassificationCacheService.evalDriverClassification(leagueEntityToDelete);
        log.info("deleted LeagueEntity {}", leagueEntityToDelete);
    }

    public List<LeagueEntity> getAllLeagues() {
        return leagueRepository.findAll();
    }

    public LeagueEntity buildLeagueEntityFromPost(@NonNull League league) {
        if (isInvalid(league))
            return null;

        return LeagueEntity.builder()
                .name(league.getName())
                .leagueEditorToken(generateLeagueEditorToken())
                .build();
    }

    private boolean raceEntityFilter(RaceEntity raceEntity) {
        LocalDate raceDate = raceEntity.getRaceDate();

        if (raceDate == null)
            return false;

        return raceDate.isAfter(LocalDate.now());
    }

    public List<LeagueEntity> sortLeaguesBasedOnLatestNextEvent(
            List<LeagueEntity> leagueEntitiesToSort) {
        return leagueEntitiesToSort.stream()
                .sorted((leagueEntity1, leagueEntity2) -> {
                    long currentEpochTime = System.currentTimeMillis();

                    RaceEntity nextRaceInLeague1 = raceService.getAllRacesByLeague(leagueEntity1)
                            .stream()
                            .filter(this::raceEntityFilter)
                            .min(Comparator.comparing(RaceEntity::getRaceDate))
                            .orElse(null);

                    RaceEntity nextRaceInLeague2 = raceService.getAllRacesByLeague(leagueEntity2)
                            .stream()
                            .filter(this::raceEntityFilter)
                            .min(Comparator.comparing(RaceEntity::getRaceDate))
                            .orElse(null);

                    if (nextRaceInLeague1 == null && nextRaceInLeague2 == null)
                        return 0;
                    else if (nextRaceInLeague1 == null)
                        return 1;
                    else if (nextRaceInLeague2 == null)
                        return -1;

                    long timeTillRace1 =
                            nextRaceInLeague1.getRaceDate().plusDays(1).atStartOfDay(ZoneOffset.UTC)
                                    .toEpochSecond() - currentEpochTime;
                    long timeTillRace2 =
                            nextRaceInLeague2.getRaceDate().plusDays(1).atStartOfDay(ZoneOffset.UTC)
                                    .toEpochSecond() - currentEpochTime;

                    return Instant.ofEpochMilli(timeTillRace1)
                            .compareTo(Instant.ofEpochMilli(timeTillRace2));
                })
                .collect(Collectors.toList());
    }

    public LeagueEntity updateLeagueEntity(League leagueWithUpdates) {
        if (isInvalid(leagueWithUpdates))
            return null;

        LeagueEntity leagueEntityToUpdate =
                getLeagueById(leagueWithUpdates.getLeagueId().toString());

        if (leagueEntityToUpdate == null)
            return null;

        leagueEntityToUpdate.setName(leagueWithUpdates.getName());

        return leagueEntityToUpdate;
    }

    private boolean isInvalid(League league) {
        if (league.getName() == null)
            return true;

        return league.getName().trim().isEmpty();
    }

    public LeagueEntity saveLeagueEntity(LeagueEntity leagueEntityToSave) {
        LeagueEntity savedLeagueEntity = leagueRepository.save(leagueEntityToSave);
        log.info("saved LeagueEntity {} to database", savedLeagueEntity);
        return savedLeagueEntity;
    }
}
