package com.gitlab.f1league.f1LeagueBackend.drivers.service;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.etc.ModelConverter;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Driver;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class DriverModelConverter implements ModelConverter<DriverEntity, Driver> {

    private final DriverService driverService;

    @Autowired
    public DriverModelConverter(DriverService driverService) {
        this.driverService = driverService;
    }

    @Override
    public DriverEntity transferModelToInternalModel(@NonNull Driver driver) {
        Optional<DriverEntity> driverEntityOptional =
                driverService.findDriverEntityByDriver(driver);

        TeamEntity teamEntity = null;
        if (driverEntityOptional.isPresent()) {
            teamEntity = driverEntityOptional.get().getTeam();
        } else {
            return null;
        }

        return DriverEntity.builder()
                .surename(driver.getSurename())
                .name(driver.getName())
                .driverId(driver.getDriverId().toString())
                .team(teamEntity)
                .build();
    }

    @Override
    public Driver internalModelToTransferModel(@NonNull DriverEntity driverEntity) {
        Driver convertedDriver = new Driver();
        convertedDriver.setName(driverEntity.getName());
        convertedDriver.setSurename(driverEntity.getSurename());
        convertedDriver.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        convertedDriver.setPoints(driverService.calculateADriversTotalPoints(driverEntity));
        convertedDriver.setPosition(driverService.getDriverPosition(driverEntity));
        convertedDriver.setTeamId(driverEntity.getTeam().getTeamId());

        return convertedDriver;
    }


}
