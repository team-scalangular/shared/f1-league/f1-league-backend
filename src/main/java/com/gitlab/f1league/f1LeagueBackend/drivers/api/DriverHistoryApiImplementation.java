package com.gitlab.f1league.f1LeagueBackend.drivers.api;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.generated.api.DriverRaceHistoryApiDelegate;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.SingleRaceHistory;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.services.RaceResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Objects;

@Controller
public class DriverHistoryApiImplementation implements DriverRaceHistoryApiDelegate {

    private final DriverService driverService;
    private final RaceService raceService;
    private final RaceResultService raceResultService;

    @Autowired
    public DriverHistoryApiImplementation(DriverService driverService, RaceService raceService,
                                          RaceResultService raceResultService) {
        this.driverService = driverService;
        this.raceService = raceService;
        this.raceResultService = raceResultService;
    }

    @Override
    public ResponseEntity<SingleRaceHistory> getDriverRaceHistoryByDriverId(String driverId,
                                                                            String raceId) {
        DriverEntity driverEntity = driverService.getDriverByDriverId(driverId);

        if (driverEntity == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        RaceEntity raceEntity = raceService.getRaceByRaceId(raceId);

        if (raceEntity == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        List<DriverRaceResultEntity> driverRaceResultEntities =
                raceResultService.getAllResultsForGivenDriverEntity(driverEntity);

        if (driverRaceResultEntities == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        if (driverRaceResultEntities.isEmpty() ||
                driverRaceResultEntities.stream()
                        .anyMatch(Objects::isNull))
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        SingleRaceHistory singleRaceHistories = driverRaceResultEntities.stream()
                .filter(driverRaceResultEntity ->
                        driverRaceResultEntity.getRaceEntity().getRacePositionIndex()
                                .equals(raceEntity.getRacePositionIndex()))
                .map(driverRaceResultEntity -> {
                    SingleRaceHistory singleRaceHistory = new SingleRaceHistory();
                    singleRaceHistory.setCarInRace(driverRaceResultEntity.getCarInRace());
                    singleRaceHistory.setEndpostion(driverRaceResultEntity.getEndpostion());
                    singleRaceHistory.setFastestLap(driverRaceResultEntity.getFastestLap());
                    singleRaceHistory.setHasFastestLap(raceResultService
                            .raceResultEntityHasFastestLap(driverRaceResultEntity));
                    singleRaceHistory.setPointsForRace(raceResultService
                            .getPointsForPosition(driverRaceResultEntity.getEndpostion()));
                    singleRaceHistory.setTotalPointsAtRaceStanding(driverService
                            .calculateADriversTotalPoints(driverEntity,
                                    raceEntity.getRacePositionIndex()));

                    singleRaceHistory.setClassificationStandingAtRace(driverService
                            .getDriverPosition(driverEntity, raceEntity.getRacePositionIndex()));

                    return singleRaceHistory;
                })
                .findFirst()
                .orElse(null);

        if (singleRaceHistories == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        return ResponseEntity.ok(singleRaceHistories);
    }
}
