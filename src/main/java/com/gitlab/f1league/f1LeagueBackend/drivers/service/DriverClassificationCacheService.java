package com.gitlab.f1league.f1LeagueBackend.drivers.service;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamClassificationCacheService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class DriverClassificationCacheService {
    private final Map<LeagueEntity, List<DriverEntity>> driverClassificationCache;
    private final DriverService driverService;

    @Setter
    private TeamClassificationCacheService teamClassificationCacheService;

    @Autowired
    public DriverClassificationCacheService(DriverService driverService) {
        this.driverService = driverService;
        this.driverClassificationCache = new ConcurrentHashMap<>();
    }

    @PostConstruct
    public void init() {
        driverService.setDriverClassificationCacheService(this);
    }

    public List<DriverEntity> getDriverClassification(LeagueEntity leagueEntity) {
        if (driverClassificationCache.containsKey(leagueEntity)) {
            log.info("returning cached driverClassification for {}", leagueEntity);
            return driverClassificationCache.get(leagueEntity);
        }

        return calculateClassificationAndSaveIt(leagueEntity);
    }

    @Scheduled(fixedDelayString = "${caching.cacheUpdateScheduleInMillis:480000}",
            initialDelay = 60000)
    @Transactional
    public void updateCachedClassifications() {
        log.info("updating driver caches invoked by scheduler");
        Instant begin = Instant.now();

        final Map<LeagueEntity, List<DriverEntity>> updatedDriverClassificationCache =
                new ConcurrentHashMap<>();

        driverClassificationCache.forEach((leagueEntity, driverClassification) -> {
            updatedDriverClassificationCache
                    .put(leagueEntity, calculateClassificationAndSaveIt(leagueEntity));
        });
        this.driverClassificationCache.clear();
        this.driverClassificationCache.putAll(updatedDriverClassificationCache);
        log.info("finished updating caches in {} ms",
                Duration.between(begin, Instant.now()).toMillis());
    }

    private List<DriverEntity> calculateClassification(LeagueEntity leagueEntity) {
        log.info("calculating driverClassification for {}", leagueEntity);
        Instant begin = Instant.now();
        List<DriverEntity> driverClassification =
                driverService.calculateDriverClassification(leagueEntity);
        Instant end = Instant.now();
        log.info("calculation took {} ms. Caching result", Duration.between(begin, end).toMillis());
        return driverClassification;
    }


    private List<DriverEntity> calculateClassificationAndSaveIt(LeagueEntity leagueEntity) {
        log.info("calculating driverClassification for {}", leagueEntity);
        Instant begin = Instant.now();
        List<DriverEntity> driverClassification =
                driverService.calculateDriverClassification(leagueEntity);
        Instant end = Instant.now();
        log.info("calculation took {} ms. Caching result", Duration.between(begin, end).toMillis());
        driverClassificationCache.put(leagueEntity, driverClassification);
        return driverClassification;
    }

    public void evalDriverClassification(LeagueEntity leagueEntity) {
        log.info("evaling driverClassification-cache for {}", leagueEntity);
        driverClassificationCache.remove(leagueEntity);
        teamClassificationCacheService.evalTeamClassification(leagueEntity);
    }

}
