package com.gitlab.f1league.f1LeagueBackend.drivers.api;

import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverClassificationCacheService;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverModelConverter;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.generated.api.DriverClassificationApiDelegate;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Driver;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class DriverClassificationApiImplementation implements DriverClassificationApiDelegate {

    private final DriverClassificationCacheService driverClassificationCacheService;
    private final LeagueService leagueService;
    private final DriverModelConverter driverModelConverter;
    private final DriverService driverService;

    @Autowired
    public DriverClassificationApiImplementation(
            DriverClassificationCacheService driverClassificationCacheService,
            LeagueService leagueService,
            DriverModelConverter driverModelConverter,
            DriverService driverService) {
        this.driverClassificationCacheService = driverClassificationCacheService;
        this.leagueService = leagueService;
        this.driverModelConverter = driverModelConverter;
        this.driverService = driverService;
    }

    @Override
    public ResponseEntity<List<Driver>> getDriverClassificationByLeagueId(String leagueId,
                                                                          Integer raceIndex) {
        LeagueEntity leagueEntity = leagueService.getLeagueById(leagueId);

        if (leagueEntity == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        if (raceIndex == null) {
            List<Driver> drivers =
                    driverClassificationCacheService.getDriverClassification(leagueEntity).stream()
                            .map(driverModelConverter::internalModelToTransferModel)
                            .collect(Collectors.toList());

            return ResponseEntity.ok(drivers);
        }

        List<Driver> drivers =
                driverService.getDriverClassification(leagueEntity, raceIndex).stream()
                        .map(driverEntity -> {
                            Driver driver =
                                    driverModelConverter.internalModelToTransferModel(driverEntity);
                            driver.setPoints(
                                    driverService
                                            .calculateADriversTotalPoints(driverEntity, raceIndex));
                            driver.setPosition(
                                    driverService.getDriverPosition(driverEntity, raceIndex));
                            return driver;
                        })
                        .collect(Collectors.toList());

        return ResponseEntity.ok(drivers);
    }
}