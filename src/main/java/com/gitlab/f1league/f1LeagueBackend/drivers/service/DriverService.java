package com.gitlab.f1league.f1LeagueBackend.drivers.service;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.repository.DriverRepository;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Driver;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.services.RaceResultService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamService;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DriverService {

    private final DriverRepository driverRepository;

    @Setter
    private TeamService teamService;

    @Setter
    private RaceResultService raceResultService;

    @Setter
    private DriverClassificationCacheService driverClassificationCacheService;

    @Autowired
    public DriverService(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    public DriverEntity getDriverByDriverId(@NonNull String driverId) {
        return driverRepository.findById(driverId).orElse(null);
    }

    // todo test
    public Optional<DriverEntity> findDriverEntityByDriver(Driver driver) {
        return driverRepository.findById(driver.getDriverId().toString());
    }

    public DriverEntity buildDriverEntityFromPost(@NonNull String leagueId, @NonNull String teamId,
                                                  @NonNull Driver driverToSave) {
        TeamEntity teamToSaveDriveTo = teamService.getTeamEntityByTeamIdAndLeague(teamId, leagueId);

        if (teamToSaveDriveTo == null)
            return null;

        if (invalidDriver(driverToSave))
            return null;

        return DriverEntity.builder()
                .name(driverToSave.getName())
                .surename(driverToSave.getSurename())
                .team(teamToSaveDriveTo)
                .build();
    }

    private boolean invalidDriver(@NonNull Driver driver) {
        if (driver.getName() == null)
            return true;

        return driver.getName().trim().isEmpty();
    }

    public List<DriverEntity> getDriversByTeamEntity(@NonNull TeamEntity team) {
        return driverRepository.findAllByTeam(team);
    }

    public List<DriverEntity> getADriversTeamPartner(DriverEntity driverEntityToGetTeammates) {
        return getDriversByTeamEntity(driverEntityToGetTeammates.getTeam()).stream()
                .filter(driverEntity -> !driverEntity.equals(driverEntityToGetTeammates))
                .collect(Collectors.toList());
    }

    public Integer calculateADriversTotalPoints(@NonNull DriverEntity driverEntity) {
        return raceResultService.getAllResultsForGivenDriverEntity(driverEntity).stream()
                .map(raceResultService::calculatePointsForRaceResult)
                .mapToInt(value -> value)
                .sum();
    }

    public Integer calculateADriversTotalPoints(@NonNull DriverEntity driverEntity, int raceIndex) {
        return raceResultService.getAllResultsForGivenDriverEntity(driverEntity).stream()
                .filter(driverRaceResultEntity ->
                        driverRaceResultEntity.getRaceEntity().getRacePositionIndex() <= raceIndex)
                .map(raceResultService::calculatePointsForRaceResult)
                .mapToInt(value -> value)
                .sum();
    }

    public List<DriverEntity> calculateDriverClassification(LeagueEntity leagueEntity) {
        return getAllDriverEntitiesInLeague(leagueEntity).stream()
                .sorted((driverEntity1, driverEntity2) -> sortBasedOnPointsAndPosition(
                        driverEntity1, driverEntity2,
                        calculateADriversTotalPoints(driverEntity1),
                        calculateADriversTotalPoints(driverEntity2)))
                .collect(Collectors.toList());
    }

    public int sortBasedOnPointsAndPosition(DriverEntity driverEntity1, DriverEntity driverEntity2,
                                            Integer pointsOfDriver1, Integer pointsOfDriver2) {
        if (!pointsOfDriver1.equals(pointsOfDriver2))
            return -pointsOfDriver1.compareTo(pointsOfDriver2);

        for (int i = 1; i <= 20; i++) {
            Long amountOfPlacesForDriver1 =
                    getAmountsOfPositionsOnPlace(driverEntity1, i);
            Long amountOfPlacesForDriver2 =
                    getAmountsOfPositionsOnPlace(driverEntity2, i);

            if (!amountOfPlacesForDriver1.equals(amountOfPlacesForDriver2))
                return -amountOfPlacesForDriver1.compareTo(amountOfPlacesForDriver2);
        }

        return 0;
    }

    public Integer getDriverPosition(DriverEntity driverEntity) {
        return driverClassificationCacheService
                .getDriverClassification(driverEntity.getTeam().getLeague()).indexOf(driverEntity) +
                1;
    }

    public Integer getDriverPosition(DriverEntity driverEntity,
                                     Integer raceIndex) {
        return getDriverClassification(driverEntity.getTeam().getLeague(), raceIndex)
                .indexOf(driverEntity) + 1;
    }

    public List<DriverEntity> getDriverClassification(LeagueEntity leagueEntity,
                                                      Integer raceIndex) {
        log.info("classification for {}", leagueEntity);
        return getAllDriverEntitiesInLeague(leagueEntity).stream()
                .sorted((driverEntity1, driverEntity2) -> sortBasedOnPointsAndPosition(
                        driverEntity1, driverEntity2,
                        calculateADriversTotalPoints(driverEntity1, raceIndex),
                        calculateADriversTotalPoints(driverEntity2, raceIndex)))
                .collect(Collectors.toList());
    }

    public long getAmountsOfPositionsOnPlace(DriverEntity driverEntity, int positionToCount) {
        return raceResultService.getAllResultsForGivenDriverEntity(driverEntity).stream()
                .filter(driverRaceResultEntity -> driverRaceResultEntity.getEndpostion() ==
                        positionToCount
                )
                .count();
    }

    public DriverEntity saveDriver(@NonNull DriverEntity driverEntityToCreate) {
        DriverEntity savedDriverEntity = driverRepository.save(driverEntityToCreate);
        driverClassificationCacheService
                .evalDriverClassification(savedDriverEntity.getTeam().getLeague());
        log.info("saved driverEntity {} to database", savedDriverEntity);
        return savedDriverEntity;
    }

    public DriverEntity updateDriver(@NonNull DriverEntity driverEntity,
                                     @NonNull Driver driverWithUpdates,
                                     String teamId) {
        if (invalidDriver(driverWithUpdates))
            return null;

        driverEntity.setName(driverWithUpdates.getName());
        driverEntity.setSurename(driverWithUpdates.getSurename());

        if (teamId != null) {
            TeamEntity teamEntityToSet = teamService.getTeamById(teamId);
            driverEntity.setTeam(teamEntityToSet);
        } else {
            driverEntity.setTeam(null);
        }

        return driverEntity;
    }

    public List<DriverEntity> getAllDriverEntitiesInLeague(LeagueEntity leagueEntity) {
        return driverRepository.findAll().stream()
                .filter(driverEntity -> driverEntity.getTeam().getLeague().equals(leagueEntity))
                .collect(Collectors.toList());
    }

    // todo test
    public void deleteDriverById(String driverId) {
        DriverEntity driverEntityToDelete = getDriverByDriverId(driverId);

        if (driverEntityToDelete == null)
            return;

        driverRepository.deleteById(driverId);
        driverClassificationCacheService
                .evalDriverClassification(driverEntityToDelete.getTeam().getLeague());
        log.info("deleted driver by id {}", driverId);
    }
}
