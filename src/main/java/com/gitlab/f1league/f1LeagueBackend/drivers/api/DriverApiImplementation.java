package com.gitlab.f1league.f1LeagueBackend.drivers.api;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverModelConverter;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.api.DriverApiDelegate;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Driver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
public class DriverApiImplementation implements DriverApiDelegate {

    private final DriverService driverService;
    private final DriverModelConverter driverModelConverter;
    private final AuthorizationService authorizationService;

    @Autowired
    public DriverApiImplementation(DriverService driverService,
                                   DriverModelConverter driverModelConverter,
                                   AuthorizationService authorizationService) {
        this.driverService = driverService;
        this.driverModelConverter = driverModelConverter;
        this.authorizationService = authorizationService;
    }

    @Override
    public ResponseEntity<Void> deleteDriver(String driverId, String leagueEditorToken,
                                             String leagueId) {
        log.info("request to delete driver by id {}", driverId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                driverService.deleteDriverById(driverId);
                return ResponseEntity.status(HttpStatus.OK).build();
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @Override
    public ResponseEntity<Driver> getDriverById(String driverId) {
        log.info("request to get driver by id {}", driverId);
        DriverEntity driverEntity = driverService.getDriverByDriverId(driverId);

        if (driverEntity == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        Driver driver = driverModelConverter.internalModelToTransferModel(driverEntity);

        return ResponseEntity.ok(driver);
    }

    @Override
    public ResponseEntity<Driver> patchDriver(String leagueEditorToken, Driver driver,
                                              String teamId, String leagueId) {
        log.info("request to patch driver {} with teamId {}", driver, teamId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                return performPatchDriver(driver, teamId);
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    private ResponseEntity<Driver> performPatchDriver(Driver driver, String teamId) {
        DriverEntity driverEntity =
                driverService.getDriverByDriverId(driver.getDriverId().toString());
        if (driverEntity == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        driverEntity = driverService.updateDriver(driverEntity, driver, teamId);

        if (driverEntity == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        driverEntity = driverService.saveDriver(driverEntity);

        return ResponseEntity.ok(driverModelConverter.internalModelToTransferModel(driverEntity));
    }

    @Override
    public ResponseEntity<Driver> postDriverToTeamByLeagueIdAndTeamId(String leagueId,
                                                                      String teamId,
                                                                      String leagueEditorToken,
                                                                      Driver driver) {
        log.info("request to post driver {} to league with id {} and team with id {}", driver,
                leagueId, teamId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                return performPostDriver(leagueId, teamId, driver);
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    private ResponseEntity<Driver> performPostDriver(String leagueId, String teamId,
                                                     Driver driver) {
        DriverEntity driverEntityToCreate =
                driverService.buildDriverEntityFromPost(leagueId, teamId, driver);

        if (driverEntityToCreate == null)
            return ResponseEntity.badRequest().build();

        driverEntityToCreate = driverService.saveDriver(driverEntityToCreate);

        Driver savedDriver =
                driverModelConverter.internalModelToTransferModel(driverEntityToCreate);

        return ResponseEntity.ok(savedDriver);
    }
}
