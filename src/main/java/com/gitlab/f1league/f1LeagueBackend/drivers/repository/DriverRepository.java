package com.gitlab.f1league.f1LeagueBackend.drivers.repository;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DriverRepository extends JpaRepository<DriverEntity, String> {
    List<DriverEntity> findAllByTeam(TeamEntity teamEntity);
}
