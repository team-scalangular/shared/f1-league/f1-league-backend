package com.gitlab.f1league.f1LeagueBackend.drivers.entity;

import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
@EqualsAndHashCode
@Table(name = "driverEntities")
@ToString
@Proxy(lazy = false)
public class DriverEntity {
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "team_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    TeamEntity team;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String driverId;

    @Column(name = "driverSurename")
    private String surename;

    @Column(name = "driverName")
    private String name;
}
