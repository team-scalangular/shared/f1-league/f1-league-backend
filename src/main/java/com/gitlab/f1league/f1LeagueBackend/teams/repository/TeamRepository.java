package com.gitlab.f1league.f1LeagueBackend.teams.repository;

import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeamRepository extends JpaRepository<TeamEntity, String> {
    List<TeamEntity> findAllByLeague(LeagueEntity league);

    TeamEntity findByTeamIdAndLeague(String teamId, LeagueEntity league);
}
