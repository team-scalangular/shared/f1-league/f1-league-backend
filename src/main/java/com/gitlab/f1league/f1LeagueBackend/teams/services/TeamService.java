package com.gitlab.f1league.f1LeagueBackend.teams.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Team;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.repository.TeamRepository;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TeamService {

    private final TeamRepository teamRepository;
    private final DriverService driverService;

    @Setter
    private LeagueService leagueService;

    @Setter
    private TeamClassificationCacheService teamClassificationCacheService;

    @Autowired
    public TeamService(TeamRepository teamRepository,
                       DriverService driverService) {
        this.teamRepository = teamRepository;
        this.driverService = driverService;
    }

    @PostConstruct
    public void init() {
        driverService.setTeamService(this);
    }

    public Optional<TeamEntity> findTeamEntityByTeam(@NonNull Team team) {
        return teamRepository.findById(team.getTeamId().toString());
    }

    public Integer getTeamPoints(@NonNull TeamEntity teamEntityToGetPointsOf) {
        return getATeamsDrivers(teamEntityToGetPointsOf).stream()
                .map(driverService::calculateADriversTotalPoints)
                .mapToInt(value -> value)
                .sum();
    }

    public Integer getTeamPoints(@NonNull TeamEntity teamEntityToGetPointsOf, int raceIndex) {
        return getATeamsDrivers(teamEntityToGetPointsOf).stream()
                .map(driverEntity -> driverService
                        .calculateADriversTotalPoints(driverEntity, raceIndex))
                .mapToInt(value -> value)
                .sum();
    }

    public Car getTeamsNextCar(@NonNull TeamEntity teamEntityToGetCarFrom) {
        Integer teamPosition = getTeamsSortedByStandingInLeague(teamEntityToGetCarFrom.getLeague())
                .indexOf(teamEntityToGetCarFrom) + 1;

        return getCarBelongingToPosition(teamPosition);
    }

    public Car getTeamsNextCar(@NonNull TeamEntity teamEntityToGetCarFrom, int raceIndex) {
        Integer teamPosition =
                getTeamsSortedByStandingInLeague(teamEntityToGetCarFrom.getLeague(), raceIndex)
                        .indexOf(teamEntityToGetCarFrom) + 1;

        return getCarBelongingToPosition(teamPosition);
    }

    public Car getCarBelongingToPosition(Integer positionToCheck) {
        switch (positionToCheck) {
            case 1:
                return Car.WILLIAMS;
            case 2:
                return Car.ALPHA_ROMEO;
            case 3:
                return Car.TORRO_ROSSO;
            case 4:
                return Car.RENAULT;
            case 5:
                return Car.RACING_POINT;
            case 6:
                return Car.MCLAREN;
            case 7:
                return Car.HAAS;
            case 8:
                return Car.RED_BULL;
            case 9:
                return Car.FERRARI;
            case 10:
                return Car.MERCEDES;
            default:
                return Car.WILLIAMS;
        }
    }

    public List<TeamEntity> getTeamsSortedByStandingInLeague(LeagueEntity leagueEntity) {
        return teamClassificationCacheService.getTeamClassification(leagueEntity);
    }


    public List<TeamEntity> getTeamsSortedByStandingInLeague(LeagueEntity leagueEntity,
                                                             int raceIndex) {
        return getAllTeamsByLeague(leagueEntity).stream()
                .sorted((teamEntity1, teamEntity2) -> {
                    Integer pointsOfTeam1 = getTeamPoints(teamEntity1, raceIndex);
                    Integer pointsOfTeam2 = getTeamPoints(teamEntity2, raceIndex);

                    return -pointsOfTeam1.compareTo(pointsOfTeam2);
                })
                .collect(Collectors.toList());
    }

    public List<DriverEntity> getATeamsDrivers(@NonNull TeamEntity teamEntityToGetDriversFrom) {
        return driverService.getDriversByTeamEntity(teamEntityToGetDriversFrom);
    }

    public TeamEntity buildTeamEntityFromTeam(@NonNull Team team, @NonNull String leagueId) {
        if (isInvalid(team))
            return null;

        LeagueEntity leagueEntity = leagueService.getLeagueById(leagueId);

        if (leagueEntity == null)
            return null;

        return TeamEntity.builder()
                .name(team.getName())
                .logoUrl(team.getLogoUrl())
                .league(leagueEntity)
                .build();
    }

    private boolean isInvalid(@NonNull Team teamToValidate) {
        if (teamToValidate.getName() == null)
            return true;

        return teamToValidate.getName().trim().isEmpty();
    }

    public TeamEntity getTeamEntityByTeamIdAndLeague(String teamId,
                                                     @NonNull String leagueId) {
        LeagueEntity league = leagueService.getLeagueById(leagueId);
        if (league == null)
            return null;

        return teamRepository.findByTeamIdAndLeague(teamId, league);
    }

    public TeamEntity saveTeamEntity(@NonNull TeamEntity teamEntityToSave) {
        TeamEntity savedTeamEntity = teamRepository.save(teamEntityToSave);
        log.info("saved TeamEntity {} to database", savedTeamEntity);
        teamClassificationCacheService.evalTeamClassification(savedTeamEntity.getLeague());
        return savedTeamEntity;
    }

    // todo test
    public TeamEntity getTeamById(@NonNull String teamId) {
        return teamRepository.findById(teamId).orElse(null);
    }

    public TeamEntity updateTeam(
            @NonNull TeamEntity teamEntityToUpdate, @NonNull Team teamWithUpdates) {
        if (isInvalid(teamWithUpdates)) {
            return null;
        }

        teamEntityToUpdate.setName(teamWithUpdates.getName());
        teamEntityToUpdate.setLogoUrl(teamWithUpdates.getLogoUrl());
        return teamEntityToUpdate;
    }

    public void deleteTeamById(@NonNull String teamId) {
        TeamEntity teamToDelete = teamRepository.findById(teamId).orElse(null);

        if (teamToDelete == null)
            return;

        driverService.getDriversByTeamEntity(teamToDelete)
                .forEach(driverEntity -> driverEntity.setTeam(null));

        teamClassificationCacheService.evalTeamClassification(teamToDelete.getLeague());
        teamRepository.deleteById(teamId);
        log.info("deleted TeamEntity {}", teamToDelete);
    }

    public List<TeamEntity> getAllTeamsByLeague(LeagueEntity leagueEntity) {
        return teamRepository.findAllByLeague(leagueEntity);
    }

    public List<TeamEntity> calculateTeamClassification(LeagueEntity leagueEntity) {
        return getAllTeamsByLeague(leagueEntity).stream()
                .sorted((o1, o2) -> -getTeamPoints(o1).compareTo(getTeamPoints(o2)))
                .collect(Collectors.toList());
    }

    public List<TeamEntity> getTeamClassification(LeagueEntity leagueEntity, Integer raceIndex) {
        return getAllTeamsByLeague(leagueEntity).stream()
                .sorted((o1, o2) -> -getTeamPoints(o1, raceIndex)
                        .compareTo(getTeamPoints(o2, raceIndex)))
                .collect(Collectors.toList());
    }

    public Integer getTeamPosition(TeamEntity teamEntity) {
        return teamClassificationCacheService.getTeamClassification(teamEntity.getLeague())
                .indexOf(teamEntity) + 1;
    }

    public Integer getTeamPosition(TeamEntity teamEntity, Integer raceIndex) {
        return getTeamClassification(teamEntity.getLeague(), raceIndex).indexOf(teamEntity) + 1;
    }
}
