package com.gitlab.f1league.f1LeagueBackend.teams.api;

import com.gitlab.f1league.f1LeagueBackend.generated.api.TeamClassificationApiDelegate;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Team;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamModelConverter;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class TeamClassificationApiImplementation implements TeamClassificationApiDelegate {

    private final TeamService teamService;
    private final TeamModelConverter teamModelConverter;
    private final LeagueService leagueService;

    @Autowired
    public TeamClassificationApiImplementation(TeamService teamService,
                                               TeamModelConverter teamModelConverter,
                                               LeagueService leagueService) {
        this.teamService = teamService;
        this.teamModelConverter = teamModelConverter;
        this.leagueService = leagueService;
    }

    @Override
    public ResponseEntity<List<Team>> getTeamClassificationByLeagueId(String leagueId,
                                                                      Integer raceIndex) {
        LeagueEntity leagueEntity = leagueService.getLeagueById(leagueId);

        if (leagueEntity == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        if (raceIndex == null) {
            List<TeamEntity> teamEntitiesOrdered =
                    teamService.getTeamsSortedByStandingInLeague(leagueEntity);

            List<Team> convertedTeams = teamEntitiesOrdered.stream()
                    .map(teamModelConverter::internalModelToTransferModel)
                    .collect(Collectors.toList());

            return ResponseEntity.ok(convertedTeams);
        }
        List<TeamEntity> teamEntitiesOrdered =
                teamService.getTeamsSortedByStandingInLeague(leagueEntity, raceIndex);

        List<Team> convertedTeams = teamEntitiesOrdered.stream()
                .map(teamEntity -> {
                    Team team = teamModelConverter.internalModelToTransferModel(teamEntity);
                    team.setPoints(teamService.getTeamPoints(teamEntity, raceIndex));
                    team.setPosition(teamService.getTeamPosition(teamEntity, raceIndex));
                    team.setNextCar(teamService.getTeamsNextCar(teamEntity, raceIndex));
                    return team;
                })
                .collect(Collectors.toList());

        return ResponseEntity.ok(convertedTeams);
    }
}
