package com.gitlab.f1league.f1LeagueBackend.teams.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverClassificationCacheService;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class TeamClassificationCacheService {
    private final Map<LeagueEntity, List<TeamEntity>> teamClassificationCache;
    private final TeamService teamService;
    private final DriverClassificationCacheService driverClassificationCacheService;

    @Autowired
    public TeamClassificationCacheService(TeamService teamService,
                                          DriverClassificationCacheService driverClassificationCacheService) {
        this.teamService = teamService;
        this.driverClassificationCacheService = driverClassificationCacheService;
        this.teamClassificationCache = new ConcurrentHashMap<>();
    }

    @PostConstruct
    public void init() {
        driverClassificationCacheService.setTeamClassificationCacheService(this);
        teamService.setTeamClassificationCacheService(this);
    }

    @Scheduled(fixedDelayString = "${caching.cacheUpdateScheduleInMillis:480000}",
            initialDelay = 60000)
    @Transactional
    public void updateCachedClassifications() {
        log.info("updating driver caches invoked by scheduler");
        Instant begin = Instant.now();
        final Map<LeagueEntity, List<TeamEntity>> updatedTeamClassificationCache =
                new ConcurrentHashMap<>();

        teamClassificationCache.forEach((leagueEntity, teamClassification) -> {
            updatedTeamClassificationCache.put(leagueEntity, calculateClassification(leagueEntity));
        });

        this.teamClassificationCache.clear();
        this.teamClassificationCache.putAll(updatedTeamClassificationCache);
        log.info("finished updating caches in {} ms",
                Duration.between(begin, Instant.now()).toMillis());
    }

    public List<TeamEntity> getTeamClassification(LeagueEntity leagueEntity) {
        if (teamClassificationCache.containsKey(leagueEntity)) {
            log.info("returning cached TeamClassification for {}", leagueEntity);
            return teamClassificationCache.get(leagueEntity);
        }

        return calculateClassificationAndSaveIt(leagueEntity);
    }

    private List<TeamEntity> calculateClassification(LeagueEntity leagueEntity) {
        log.info("calculating TeamClassification for {}", leagueEntity);
        Instant begin = Instant.now();
        List<TeamEntity> teamClassification =
                teamService.calculateTeamClassification(leagueEntity);
        Instant end = Instant.now();
        log.info("calculation took {} ms. Caching result", Duration.between(begin, end).toMillis());
        return teamClassification;
    }

    private List<TeamEntity> calculateClassificationAndSaveIt(LeagueEntity leagueEntity) {
        log.info("calculating TeamClassification for {}", leagueEntity);
        Instant begin = Instant.now();
        List<TeamEntity> teamClassification =
                teamService.calculateTeamClassification(leagueEntity);
        Instant end = Instant.now();
        log.info("calculation took {} ms. Caching result", Duration.between(begin, end).toMillis());
        teamClassificationCache.put(leagueEntity, teamClassification);
        return teamClassification;
    }

    public void evalTeamClassification(LeagueEntity leagueEntity) {
        log.info("evaling TeamClassification-cache for {}", leagueEntity);
        teamClassificationCache.remove(leagueEntity);
    }

}
