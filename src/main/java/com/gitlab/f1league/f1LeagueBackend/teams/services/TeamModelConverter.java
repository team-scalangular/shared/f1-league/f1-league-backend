package com.gitlab.f1league.f1LeagueBackend.teams.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.etc.ModelConverter;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Team;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TeamModelConverter implements ModelConverter<TeamEntity, Team> {

    private final TeamService teamService;

    @Autowired
    public TeamModelConverter(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public TeamEntity transferModelToInternalModel(Team team) {
        TeamEntity convertedTeamEntity = TeamEntity.builder()
                .name(team.getName())
                .logoUrl(team.getLogoUrl())
                .teamId(team.getTeamId().toString())
                .build();

        teamService.findTeamEntityByTeam(team)
                .ifPresentOrElse(
                        teamEntity -> convertedTeamEntity.setLeague(teamEntity.getLeague()),
                        () -> convertedTeamEntity.setLeague(null));

        return convertedTeamEntity;
    }

    @Override
    public Team internalModelToTransferModel(TeamEntity teamEntity) {
        Team convertedTeam = new Team();

        convertedTeam.setTeamId(UUID.fromString(teamEntity.getTeamId()));
        convertedTeam.setLogoUrl(teamEntity.getLogoUrl());
        convertedTeam.setName(teamEntity.getName());
        convertedTeam.setPoints(teamService.getTeamPoints(teamEntity));
        convertedTeam.setNextCar(teamService.getTeamsNextCar(teamEntity));
        convertedTeam.setListOfDriverIds(teamService.getATeamsDrivers(teamEntity).stream()
                .map(DriverEntity::getDriverId)
                .collect(Collectors.toList())
        );
        convertedTeam.setPosition(teamService.getTeamPosition(teamEntity));
        return convertedTeam;
    }
}
