package com.gitlab.f1league.f1LeagueBackend.teams.api;

import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.api.TeamApiDelegate;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Team;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamModelConverter;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class TeamApiImplementation implements TeamApiDelegate {

    private final TeamService teamService;
    private final TeamModelConverter teamModelConverter;
    private final LeagueService leagueService;
    private final AuthorizationService authorizationService;

    @Autowired
    public TeamApiImplementation(TeamService teamService,
                                 TeamModelConverter teamModelConverter,
                                 LeagueService leagueService,
                                 AuthorizationService authorizationService) {
        this.teamService = teamService;
        this.teamModelConverter = teamModelConverter;
        this.leagueService = leagueService;
        this.authorizationService = authorizationService;
    }

    @Override
    public ResponseEntity<Void> deleteTeam(String teamId, String leagueEditorToken,
                                           String leagueId) {
        log.info("request to delete team by id {}", teamId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                teamService.deleteTeamById(teamId);
                return ResponseEntity.status(HttpStatus.OK).build();
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @Override
    public ResponseEntity<List<Team>> getTeamsByLeagueId(String leagueId, String teamId) {
        log.info("request to get team with id {} from league with id {}", teamId, leagueId);

        LeagueEntity leagueEntity = leagueService.getLeagueById(leagueId);

        if (leagueEntity == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        List<TeamEntity> teamEntities = null;

        if (teamId == null) {
            teamEntities = teamService.getAllTeamsByLeague(leagueEntity);

            if (teamEntities == null)
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

            if (teamEntities.isEmpty())
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            teamEntities = new ArrayList<>();
            TeamEntity teamEntity = teamService.getTeamEntityByTeamIdAndLeague(teamId, leagueId);

            if (teamEntity == null)
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

            teamEntities.add(teamEntity);
        }

        List<Team> convertedTeams = teamEntities.stream()
                .map(teamModelConverter::internalModelToTransferModel)
                .collect(Collectors.toList());

        return ResponseEntity.ok(convertedTeams);
    }

    @Override
    public ResponseEntity<Team> patchTeam(String leagueEditorToken, Team team, String leagueId) {
        log.info("request to patch team {}", team);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                return performPatchTeam(team);
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    private ResponseEntity<Team> performPatchTeam(Team team) {
        TeamEntity foundTeamEntity = teamService.getTeamById(team.getTeamId().toString());

        if (foundTeamEntity == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        foundTeamEntity = teamService.updateTeam(foundTeamEntity, team);

        if (foundTeamEntity == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        foundTeamEntity = teamService.saveTeamEntity(foundTeamEntity);

        return ResponseEntity.ok(teamModelConverter.internalModelToTransferModel(foundTeamEntity));
    }

    @Override
    public ResponseEntity<Team> postTeamToLeague(String leagueId, String leagueEditorToken,
                                                 Team team) {
        log.info("request to post Team {} to league with id {}", team, leagueId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                return performPostTeam(leagueId, team);
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    private ResponseEntity<Team> performPostTeam(String leagueId, Team team) {
        TeamEntity teamEntityToCreate = teamService.buildTeamEntityFromTeam(team, leagueId);

        if (teamEntityToCreate == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        teamEntityToCreate = teamService.saveTeamEntity(teamEntityToCreate);

        return ResponseEntity
                .ok(teamModelConverter.internalModelToTransferModel(teamEntityToCreate));
    }
}
