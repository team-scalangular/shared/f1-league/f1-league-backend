package com.gitlab.f1league.f1LeagueBackend.race.entity;

import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Circuit;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
@EqualsAndHashCode
@Table(name = "races")
@ToString
@Proxy(lazy = false)
public class RaceEntity {
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "league_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    LeagueEntity league;
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String raceId;
    private Circuit circuit;

    private LocalDate raceDate;

    private Boolean hasBeenDriven;

    private Integer racePositionIndex;
}
