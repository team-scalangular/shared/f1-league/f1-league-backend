package com.gitlab.f1league.f1LeagueBackend.race.services;

import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Race;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.repository.RaceRepository;
import com.gitlab.f1league.f1LeagueBackend.raceResult.services.RaceResultService;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class RaceService {

    private final RaceRepository raceRepository;

    @Setter
    private LeagueService leagueService;

    @Setter
    private RaceResultService raceResultService;

    @Autowired
    public RaceService(RaceRepository raceRepository) {
        this.raceRepository = raceRepository;
    }

    public RaceEntity getRaceByRaceId(@NonNull String raceId) {
        return raceRepository.findById(raceId).orElse(null);
    }

    public List<RaceEntity> getAllRacesByLeague(@NonNull LeagueEntity leagueEntity) {
        return raceRepository.findAllByLeague(leagueEntity);
    }

    public Optional<RaceEntity> findRaceEntityByRace(Race race) {
        return raceRepository.findById(race.getRaceId().toString());
    }

    public void deleteRaceById(@NonNull String raceId) {
        RaceEntity raceEntity = raceRepository.findById(raceId).orElse(null);

        if (raceEntity == null)
            return;

        deleteRaceEntityAndItsDriverRaceResults(raceEntity);
    }

    private void deleteRaceEntityAndItsDriverRaceResults(@NonNull RaceEntity raceEntityToDelete) {
        raceResultService.getRaceResultsByRaceId(raceEntityToDelete.getRaceId()).forEach(
                raceResultService::deleteRaceResultByRaceResult);

        raceRepository.delete(raceEntityToDelete);
        log.info("deleted RaceEntity {}", raceEntityToDelete);
    }

    public RaceEntity buildRaceEntityFromPost(String leagueId, Race race) {
        if (isInvalid(race))
            return null;

        LeagueEntity leagueEntity = leagueService.getLeagueById(leagueId);

        Integer racePositionIndex = null;

        if (leagueEntity == null)
            return null;

        racePositionIndex = findRacePositionIndexAndSwapIfNeeded(race, leagueEntity);

        return RaceEntity.builder()
                .circuit(race.getCircuit())
                .raceDate(race.getRaceDate())
                .hasBeenDriven(race.getHasBeenDriven())
                .league(leagueEntity)
                .racePositionIndex(racePositionIndex)
                .build();
    }

    public Integer findRacePositionIndexAndSwapIfNeeded(Race race, LeagueEntity leagueEntity) {
        Integer racePositionIndex;
        if (race.getRacePositionIndex() == null)
            racePositionIndex = calculateRacePositionIndex(leagueEntity);
        else {
            if (raceIndexCollision(race.getRacePositionIndex(), leagueEntity)) {
                RaceEntity raceEntityWithCollision = raceRepository
                        .getByLeagueAndRacePositionIndex(leagueEntity, race.getRacePositionIndex());
                raceEntityWithCollision
                        .setRacePositionIndex(calculateRacePositionIndex(leagueEntity));
            }

            racePositionIndex = race.getRacePositionIndex();
        }
        return racePositionIndex;
    }

    public boolean raceIndexCollision(int indexToCheck, LeagueEntity leagueEntity) {
        return getAllRacesByLeague(leagueEntity).stream()
                .anyMatch(raceEntity -> raceEntity.getRacePositionIndex() == indexToCheck);
    }

    public Integer calculateRacePositionIndex(LeagueEntity leagueEntity) {
        return getAllRacesByLeague(leagueEntity).size();
    }

    public boolean isInvalid(Race race) {
        if (race == null)
            return true;

        if (race.getCircuit() == null)
            return true;

        return race.getHasBeenDriven() == null;
    }

    public RaceEntity updateRace(RaceEntity raceEntityToUpdate, Race raceWithUpdates) {
        if (isInvalid(raceWithUpdates))
            return null;

        raceEntityToUpdate.setCircuit(raceWithUpdates.getCircuit());
        raceEntityToUpdate.setHasBeenDriven(raceWithUpdates.getHasBeenDriven());
        raceEntityToUpdate.setRaceDate(raceWithUpdates.getRaceDate());

        int racePositionIndex = findRacePositionIndexAndSwapIfNeeded(raceWithUpdates,
                raceEntityToUpdate.getLeague());

        raceEntityToUpdate.setRacePositionIndex(racePositionIndex);

        return raceEntityToUpdate;
    }

    public RaceEntity save(RaceEntity raceEntityToSave) {
        RaceEntity savedRaceEntity = raceRepository.save(raceEntityToSave);

        log.info("saved RaceEntity {} to database", savedRaceEntity);
        return savedRaceEntity;
    }
}
