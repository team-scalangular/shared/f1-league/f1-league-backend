package com.gitlab.f1league.f1LeagueBackend.race.repository;

import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RaceRepository extends JpaRepository<RaceEntity, String> {
    List<RaceEntity> findAllByLeague(LeagueEntity leagueEntity);

    RaceEntity getByLeagueAndRacePositionIndex(LeagueEntity leagueEntity, int racePositionIndex);
}
