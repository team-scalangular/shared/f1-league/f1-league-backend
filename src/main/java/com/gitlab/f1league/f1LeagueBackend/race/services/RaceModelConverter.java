package com.gitlab.f1league.f1LeagueBackend.race.services;

import com.gitlab.f1league.f1LeagueBackend.etc.ModelConverter;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Race;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RaceModelConverter implements ModelConverter<RaceEntity, Race> {

    private final RaceService raceService;

    @Autowired
    public RaceModelConverter(RaceService raceService) {
        this.raceService = raceService;
    }

    @Override
    public RaceEntity transferModelToInternalModel(Race race) {
        RaceEntity convertedRaceEntity = RaceEntity.builder()
                .raceId(race.getRaceId().toString())
                .hasBeenDriven(race.getHasBeenDriven())
                .raceDate(race.getRaceDate())
                .racePositionIndex(race.getRacePositionIndex())
                .build();

        raceService.findRaceEntityByRace(race)
                .ifPresentOrElse(
                        raceEntity -> convertedRaceEntity.setLeague(raceEntity.getLeague()),
                        () -> convertedRaceEntity.setLeague(null));

        return convertedRaceEntity;
    }

    @Override
    public Race internalModelToTransferModel(RaceEntity raceEntity) {
        Race expectedResult = new Race();
        expectedResult.setCircuit(raceEntity.getCircuit());
        expectedResult.setHasBeenDriven(raceEntity.getHasBeenDriven());
        expectedResult.setRaceDate(raceEntity.getRaceDate());
        expectedResult.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        expectedResult.setRacePositionIndex(raceEntity.getRacePositionIndex());

        return expectedResult;
    }
}
