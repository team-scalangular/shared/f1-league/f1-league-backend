package com.gitlab.f1league.f1LeagueBackend.race.api;

import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.api.RaceApiDelegate;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Race;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceModelConverter;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class RaceApiImplementation implements RaceApiDelegate {

    private final RaceService raceService;
    private final RaceModelConverter raceModelConverter;
    private final AuthorizationService authorizationService;
    private final LeagueService leagueService;

    @Autowired
    public RaceApiImplementation(RaceService raceService, RaceModelConverter raceModelConverter,
                                 AuthorizationService authorizationService,
                                 LeagueService leagueService) {
        this.raceService = raceService;
        this.raceModelConverter = raceModelConverter;
        this.authorizationService = authorizationService;
        this.leagueService = leagueService;
    }

    @Override
    public ResponseEntity<Void> deleteRace(String raceId, String leagueEditorToken,
                                           String leagueId) {
        log.info("request to delete race by id {}", raceId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                raceService.deleteRaceById(raceId);
                return ResponseEntity.ok().build();
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @Override
    public ResponseEntity<List<Race>> getRaces(String leagueId, String raceId) {
        if (raceId == null)
            log.info("request to get races by league with leagueId {}", leagueId);
        else
            log.info("request to get race by raceId {} from league with leagueId {} ", raceId,
                    leagueId);

        LeagueEntity leagueEntity = leagueService.getLeagueById(leagueId);

        if (leagueEntity == null) {
            log.warn("no league is known to leagueId {}", leagueId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        List<RaceEntity> raceEntities = new ArrayList<>();

        if (raceId == null) {
            raceEntities = raceService.getAllRacesByLeague(leagueEntity);
        } else {
            RaceEntity foundRaceEntity = raceService.getRaceByRaceId(raceId);
            if (foundRaceEntity != null)
                raceEntities.add(raceService.getRaceByRaceId(raceId));
        }

        if (raceEntities == null) {
            log.info("no races found for league {}", leagueEntity);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else if (raceEntities.isEmpty()) {
            log.info("no races found for raceId {}", raceId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return ResponseEntity.ok().body(raceEntities.stream()
                .map(raceModelConverter::internalModelToTransferModel)
                .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<Race> patchRace(String leagueEditorToken, Race race, String leagueId) {
        log.info("request to patch race {}", race);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                return performPatchRace(race);
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    private ResponseEntity<Race> performPatchRace(
            Race race) {
        RaceEntity raceEntity = raceService.getRaceByRaceId(race.getRaceId().toString());

        if (raceEntity == null)
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        raceEntity = raceService.updateRace(raceEntity, race);

        if (raceEntity == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        raceEntity = raceService.save(raceEntity);

        return ResponseEntity.ok(raceModelConverter.internalModelToTransferModel(raceEntity));
    }

    @Override
    public ResponseEntity<Race> postRaceToLeagueByLeagueId(String leagueId,
                                                           String leagueEditorToken, Race race) {
        log.info("request to post race {} to league with id {}", race, leagueId);

        switch (authorizationService.isValidToken(leagueEditorToken, leagueId)) {
            case UNAUTHORIZED:
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            case AUTHORIZED:
                return performPostRaceToLeague(leagueId, race);
            case UNKNOWN:
                return ResponseEntity.notFound().build();
        }

        return ResponseEntity.badRequest().build();
    }

    private ResponseEntity<Race> performPostRaceToLeague(String leagueId, Race race) {
        RaceEntity raceEntityToCreate = raceService.buildRaceEntityFromPost(leagueId, race);

        if (raceEntityToCreate == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        raceEntityToCreate = raceService.save(raceEntityToCreate);

        Race savedConvertedRace =
                raceModelConverter.internalModelToTransferModel(raceEntityToCreate);

        return ResponseEntity.ok(savedConvertedRace);
    }
}
