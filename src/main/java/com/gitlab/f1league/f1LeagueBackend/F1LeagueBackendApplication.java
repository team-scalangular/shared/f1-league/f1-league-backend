package com.gitlab.f1league.f1LeagueBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class F1LeagueBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(F1LeagueBackendApplication.class, args);
    }
}
