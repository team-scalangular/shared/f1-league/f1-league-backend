package com.gitlab.f1league.f1LeagueBackend.raceResult.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.DriverRaceResult;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RaceResultModelConverterTest {

    RaceResultModelConverter raceResultModelConverter;

    @MockBean
    DriverService driverService;

    @MockBean
    RaceService raceService;

    @MockBean
    RaceResultService raceResultService;

    @BeforeEach
    public void setUp() {
        raceResultModelConverter =
                new RaceResultModelConverter(driverService, raceService, raceResultService);
    }

    @Test
    public void convertExternalToInternal_GivenValidExternal_ExpectSuccess() {
        DriverRaceResult driverRaceResultToConvert = new DriverRaceResult();
        driverRaceResultToConvert.setCarInRace(null);
        driverRaceResultToConvert.setDriverId(UUID.randomUUID());
        driverRaceResultToConvert.setDriverRaceResultId(UUID.randomUUID());
        driverRaceResultToConvert.setEndpostion(5);
        driverRaceResultToConvert.setStartposition(3);
        driverRaceResultToConvert.setFastestLap(BigDecimal.valueOf(112.453));
        driverRaceResultToConvert.setRaceId(UUID.randomUUID());

        RaceEntity raceEntity =
                RaceEntity.builder().raceId(driverRaceResultToConvert.getRaceId().toString())
                        .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(driverRaceResultToConvert.getDriverId().toString())
                .build();

        DriverRaceResultEntity expectedDriverRaceResultEntity = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(driverRaceResultToConvert.getStartposition())
                .fastestLap(driverRaceResultToConvert.getFastestLap())
                .endpostion(driverRaceResultToConvert.getEndpostion())
                .driverRaceResultId(driverRaceResultToConvert.getDriverRaceResultId().toString())
                .driver(driverEntity)
                .carInRace(null)
                .build();

        when(driverService.getDriverByDriverId(driverRaceResultToConvert.getDriverId().toString()))
                .thenReturn(driverEntity);
        when(raceService.getRaceByRaceId(driverRaceResultToConvert.getRaceId().toString()))
                .thenReturn(raceEntity);

        assertEquals(expectedDriverRaceResultEntity,
                raceResultModelConverter.transferModelToInternalModel(driverRaceResultToConvert));
    }

    @Test
    public void convertInternalToExternal_GivenValidInternal_ExpectSuccess() {
        RaceEntity raceEntity =
                RaceEntity.builder().raceId(UUID.randomUUID().toString())
                        .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntityToConvert = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(117.3))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driverEntity)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResult expectedDriverRaceResult = new DriverRaceResult();
        expectedDriverRaceResult.setCarInRace(driverRaceResultEntityToConvert.getCarInRace());
        expectedDriverRaceResult.setDriverId(
                UUID.fromString(driverRaceResultEntityToConvert.getDriver().getDriverId()));
        expectedDriverRaceResult.setDriverRaceResultId(
                UUID.fromString(driverRaceResultEntityToConvert.getDriverRaceResultId()));
        expectedDriverRaceResult.setEndpostion(1);
        expectedDriverRaceResult.setDriverName("Max Bieleke");
        expectedDriverRaceResult.setHasFastestLap(false);
        expectedDriverRaceResult.setPoints(0);
        expectedDriverRaceResult.setStartposition(2);
        expectedDriverRaceResult.setFastestLap(driverRaceResultEntityToConvert.getFastestLap());
        expectedDriverRaceResult.setRaceId(
                UUID.fromString(driverRaceResultEntityToConvert.getRaceEntity().getRaceId()));

        assertEquals(expectedDriverRaceResult,
                raceResultModelConverter
                        .internalModelToTransferModel(driverRaceResultEntityToConvert));
    }
}