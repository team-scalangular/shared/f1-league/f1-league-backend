package com.gitlab.f1league.f1LeagueBackend.raceResult.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverClassificationCacheService;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.DriverRaceResult;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.repository.RaceResultRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RaceResultServiceTest {

    @MockBean
    DriverService driverService;

    @MockBean
    RaceService raceService;

    @MockBean
    LeagueService leagueService;

    @MockBean
    RaceResultRepository raceResultRepository;
    RaceResultService raceResultService;
    @MockBean
    private DriverClassificationCacheService driverClassificationCacheService;

    @BeforeEach
    public void setUp() {
        driverService.setDriverClassificationCacheService(driverClassificationCacheService);
        raceResultService = new RaceResultService(raceResultRepository, driverService, raceService,
                leagueService, driverClassificationCacheService);
        when(driverClassificationCacheService.getDriverClassification(any())).thenReturn(null);
    }

    @Test
    public void getRaceResultByRaceIdAndDriverId_ValidIds_ReturnCorrectEntity() {
        RaceEntity raceEntity =
                RaceEntity.builder().raceId(UUID.randomUUID().toString())
                        .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntityToConvert = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(117.3))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driverEntity)
                .carInRace(Car.HAAS)
                .build();

        when(driverService.getDriverByDriverId(driverEntity.getDriverId()))
                .thenReturn(driverEntity);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);
        when(raceResultRepository.findAllByRaceEntityAndDriver(raceEntity, driverEntity))
                .thenReturn(List.of(driverRaceResultEntityToConvert));

        assertEquals(List.of(driverRaceResultEntityToConvert),
                raceResultService.getRaceResultsByRaceIdAndDriverId(raceEntity.getRaceId(),
                        driverEntity.getDriverId()));
    }

    @Test
    public void getRaceResultByRaceIdAndDriverId_InvalidDriverId_Null() {
        RaceEntity raceEntity =
                RaceEntity.builder().raceId(UUID.randomUUID().toString())
                        .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntityToConvert = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(117.3))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driverEntity)
                .carInRace(Car.HAAS)
                .build();

        when(driverService.getDriverByDriverId(driverEntity.getDriverId()))
                .thenReturn(null);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);
        when(raceResultRepository.findAllByRaceEntityAndDriver(raceEntity, driverEntity))
                .thenReturn(List.of(driverRaceResultEntityToConvert));

        assertEquals(new ArrayList<>(),
                raceResultService.getRaceResultsByRaceIdAndDriverId(raceEntity.getRaceId(),
                        driverEntity.getDriverId()));
    }

    @Test
    public void getRaceResultByRaceIdAndDriverId_InvalidRaceId_Null() {
        RaceEntity raceEntity =
                RaceEntity.builder().raceId(UUID.randomUUID().toString())
                        .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntityToConvert = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(117.3))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driverEntity)
                .carInRace(Car.HAAS)
                .build();

        when(driverService.getDriverByDriverId(driverEntity.getDriverId()))
                .thenReturn(driverEntity);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(null);
        when(raceResultRepository.findAllByRaceEntityAndDriver(raceEntity, driverEntity))
                .thenReturn(List.of(driverRaceResultEntityToConvert));

        assertEquals(new ArrayList<>(),
                raceResultService.getRaceResultsByRaceIdAndDriverId(raceEntity.getRaceId(),
                        driverEntity.getDriverId()));
    }

    @Test
    public void getRaceResultsByRaceId_ValidRaceId_Success() {
        RaceEntity raceEntity =
                RaceEntity.builder().raceId(UUID.randomUUID().toString())
                        .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntity1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(117.3))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driverEntity)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResultEntity driverRaceResultEntity2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(1)
                .fastestLap(BigDecimal.valueOf(118.1))
                .endpostion(2)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driverEntity)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);
        when(raceResultRepository.findAllByRaceEntity(raceEntity))
                .thenReturn(List.of(driverRaceResultEntity1, driverRaceResultEntity2));

        assertEquals(List.of(driverRaceResultEntity1, driverRaceResultEntity2),
                raceResultService.getRaceResultsByRaceId(raceEntity.getRaceId()));
    }

    @Test
    public void getRaceResultsByRaceId_invalidRaceId_EmptyList() {
        RaceEntity raceEntity =
                RaceEntity.builder().raceId(UUID.randomUUID().toString())
                        .build();


        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(null);
        assertEquals(new ArrayList<>(),
                raceResultService.getRaceResultsByRaceId(raceEntity.getRaceId()));
    }

    @Test
    public void isInvalid_GivenValidDriverRaceResult_False() {
        DriverRaceResult driverRaceResult = new DriverRaceResult();
        driverRaceResult.setDriverId(UUID.randomUUID());
        driverRaceResult.setFastestLap(BigDecimal.valueOf(112.38));
        driverRaceResult.setStartposition(4);
        driverRaceResult.setEndpostion(2);
        driverRaceResult.setRaceId(UUID.randomUUID());
        driverRaceResult.setCarInRace(Car.MCLAREN);

        assertFalse(raceResultService.isInvalid(driverRaceResult));
    }

    @Test
    public void isInvalid_GivenInValidDriverRaceResult_False() {
        DriverRaceResult driverRaceResult = new DriverRaceResult();
        driverRaceResult.setDriverId(UUID.randomUUID());
        driverRaceResult.setFastestLap(BigDecimal.valueOf(112.38));
        driverRaceResult.setStartposition(4);
        driverRaceResult.setRaceId(UUID.randomUUID());
        driverRaceResult.setCarInRace(Car.MCLAREN);

        assertTrue(raceResultService.isInvalid(driverRaceResult));
    }

    @Test
    public void saveAll_givenDriverRaceResultEntities_Correct() {
        RaceEntity raceEntity =
                RaceEntity.builder().raceId(UUID.randomUUID().toString())
                        .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntity1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(117.3))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driverEntity)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResultEntity driverRaceResultEntity2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(1)
                .fastestLap(BigDecimal.valueOf(118.1))
                .endpostion(2)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driverEntity)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        List<DriverRaceResultEntity> driverRaceResultEntities =
                List.of(driverRaceResultEntity1, driverRaceResultEntity2);

        when(raceResultRepository.saveAll(driverRaceResultEntities))
                .thenReturn(driverRaceResultEntities);

        assertEquals(driverRaceResultEntities, raceResultService.saveAll(driverRaceResultEntities));
    }

    @Test
    public void buildDriverRaceResultEntityFromPost_GivenValidDriverRaceResult_ExpectSuccess() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity =
                RaceEntity.builder()
                        .raceId(UUID.randomUUID().toString())
                        .league(leagueEntity)
                        .build();

        DriverRaceResult givenDriverRaceResult = new DriverRaceResult();
        givenDriverRaceResult.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        givenDriverRaceResult.setStartposition(2);
        givenDriverRaceResult.setFastestLap(BigDecimal.valueOf(117.3));
        givenDriverRaceResult.setEndpostion(1);
        givenDriverRaceResult.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        givenDriverRaceResult.setCarInRace(Car.HAAS);

        DriverRaceResultEntity expectedDriverResultEntity = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(givenDriverRaceResult.getStartposition())
                .fastestLap(givenDriverRaceResult.getFastestLap())
                .endpostion(givenDriverRaceResult.getEndpostion())
                .driver(driverEntity)
                .carInRace(givenDriverRaceResult.getCarInRace())
                .build();

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);
        when(driverService.getDriverByDriverId(driverEntity.getDriverId()))
                .thenReturn(driverEntity);

        assertEquals(expectedDriverResultEntity, raceResultService
                .buildDriverRaceResultEntityFromPost(givenDriverRaceResult,
                        leagueEntity.getLeagueId(), raceEntity.getRaceId()));
    }

    @Test
    public void buildDriverRaceResultEntityFromPost_GivenValidDriverRaceResultButInvalidIds_ExpectNull() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity =
                RaceEntity.builder()
                        .raceId(UUID.randomUUID().toString())
                        .league(leagueEntity)
                        .build();

        DriverRaceResult givenDriverRaceResult = new DriverRaceResult();
        givenDriverRaceResult.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        givenDriverRaceResult.setStartposition(2);
        givenDriverRaceResult.setFastestLap(BigDecimal.valueOf(117.3));
        givenDriverRaceResult.setEndpostion(1);
        givenDriverRaceResult.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        givenDriverRaceResult.setCarInRace(Car.HAAS);

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(null);
        when(driverService.getDriverByDriverId(driverEntity.getDriverId()))
                .thenReturn(driverEntity);

        assertNull(raceResultService
                .buildDriverRaceResultEntityFromPost(givenDriverRaceResult,
                        leagueEntity.getLeagueId(), raceEntity.getRaceId()));
    }

    @Test
    public void buildDriverRaceResultEntityFromPost_GiverDriverRaceResultInvalidLeagueId_ExpectNull() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        LeagueEntity leagueEntity2 = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();


        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity =
                RaceEntity.builder()
                        .raceId(UUID.randomUUID().toString())
                        .league(leagueEntity)
                        .build();

        DriverRaceResult givenDriverRaceResult = new DriverRaceResult();
        givenDriverRaceResult.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        givenDriverRaceResult.setStartposition(2);
        givenDriverRaceResult.setFastestLap(BigDecimal.valueOf(117.3));
        givenDriverRaceResult.setEndpostion(1);
        givenDriverRaceResult.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        givenDriverRaceResult.setCarInRace(Car.HAAS);

        when(leagueService.getLeagueById(leagueEntity2.getLeagueId())).thenReturn(leagueEntity2);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);
        when(driverService.getDriverByDriverId(driverEntity.getDriverId()))
                .thenReturn(driverEntity);

        assertNull(raceResultService
                .buildDriverRaceResultEntityFromPost(givenDriverRaceResult,
                        leagueEntity.getLeagueId(), raceEntity.getRaceId()));
    }

    @Test
    public void buildDriverRaceResultEntityFromPost_GiverRaceWithWrongLeague_ExpectNull() {
        LeagueEntity leagueEntity1 = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        LeagueEntity leagueEntity2 = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(null)
                .surename("Bieleke")
                .name("Max")
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity =
                RaceEntity.builder()
                        .raceId(UUID.randomUUID().toString())
                        .league(leagueEntity1)
                        .build();

        DriverRaceResult givenDriverRaceResult = new DriverRaceResult();
        givenDriverRaceResult.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        givenDriverRaceResult.setStartposition(2);
        givenDriverRaceResult.setFastestLap(BigDecimal.valueOf(117.3));
        givenDriverRaceResult.setEndpostion(1);
        givenDriverRaceResult.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        givenDriverRaceResult.setCarInRace(Car.HAAS);

        when(leagueService.getLeagueById(leagueEntity2.getLeagueId())).thenReturn(leagueEntity2);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);
        when(driverService.getDriverByDriverId(driverEntity.getDriverId()))
                .thenReturn(driverEntity);

        assertNull(raceResultService
                .buildDriverRaceResultEntityFromPost(givenDriverRaceResult,
                        leagueEntity2.getLeagueId(), raceEntity.getRaceId()));
    }

    @Test
    public void overrideRaceResult_AlreadyExists_True() {
        DriverEntity driverToCheck = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(null)
                .build();

        RaceEntity raceEntity1 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity2 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity result1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.56))
                .endpostion(3)
                .driver(driverToCheck)
                .carInRace(Car.FERRARI)
                .build();

        DriverRaceResultEntity result2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(117.358))
                .endpostion(2)
                .driver(driverToCheck)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result3 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(117.358))
                .endpostion(2)
                .driver(driverToCheck)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        when(raceResultRepository.findAllByDriver(driverToCheck))
                .thenReturn(List.of(result1, result2));

        assertAll(
                () -> assertTrue(raceResultService.overrideRaceResult(result2)),
                () -> assertFalse(raceResultService.overrideRaceResult(result3))
        );
    }

    @Test
    public void getAllResultsForGivenDriverEntity_ValidDriverEntity_Success() {
        DriverEntity driverEntityToGetResultsFrom = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(null)
                .build();

        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(null)
                .build();

        RaceEntity raceEntity1 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity2 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity result1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.56))
                .endpostion(3)
                .driver(driverEntityToGetResultsFrom)
                .carInRace(Car.FERRARI)
                .build();

        DriverRaceResultEntity result3 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(117.358))
                .endpostion(2)
                .driver(driverEntityToGetResultsFrom)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        when(raceResultRepository.findAllByDriver(driverEntityToGetResultsFrom))
                .thenReturn(List.of(result1, result3));

        assertEquals(List.of(result1, result3),
                raceResultService.getAllResultsForGivenDriverEntity(driverEntityToGetResultsFrom));
    }

    @ParameterizedTest
    @CsvSource(
            {"1, 25", "2, 18", "3, 15", "4, 12", "5, 10", "6, 8", "7, 6", "8, 4", "9, 2", "10, 1",
                    "14, 0"})
    public void getPointsBelongingToPosition_givenSamplePositions_Success(int position,
                                                                          int pointsToGet) {
        assertEquals(pointsToGet, raceResultService.getPointsForPosition(position));
    }

    @Test
    public void raceResultEntityHasFastesLap_HasFastesLap_Success() {
        DriverEntity driverEntity1 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(null)
                .build();

        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(null)
                .build();

        DriverEntity driverEntity3 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Tom")
                .surename("Waldautttt")
                .team(null)
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity result1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.560))
                .endpostion(3)
                .driver(driverEntity1)
                .carInRace(Car.FERRARI)
                .build();

        DriverRaceResultEntity result2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(73.112))
                .endpostion(2)
                .driver(driverEntity2)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result3 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.358))
                .endpostion(2)
                .driver(driverEntity3)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result4 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(71.358))
                .endpostion(11)
                .driver(driverEntity3)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);
        when(raceResultRepository.findAllByRaceEntity(raceEntity))
                .thenReturn(List.of(result1, result2, result3, result4));

        assertAll(
                () -> assertTrue(raceResultService.raceResultEntityHasFastestLap(result3)),
                () -> assertFalse(raceResultService.raceResultEntityHasFastestLap(result2)),
                () -> assertFalse(raceResultService.raceResultEntityHasFastestLap(result1)),
                () -> assertFalse(raceResultService.raceResultEntityHasFastestLap(result4))
        );
    }

    @Test
    public void calculatePointsForRaceResult_GivenRaceResults_Success() {
        DriverEntity driverEntity1 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(null)
                .build();

        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(null)
                .build();

        DriverEntity driverEntity3 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Tom")
                .surename("Waldautttt")
                .team(null)
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity result1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.560))
                .endpostion(3)
                .driver(driverEntity1)
                .carInRace(Car.FERRARI)
                .build();

        DriverRaceResultEntity result2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(73.112))
                .endpostion(8)
                .driver(driverEntity2)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result3 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.358))
                .endpostion(2)
                .driver(driverEntity3)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result4 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(71.358))
                .endpostion(11)
                .driver(driverEntity3)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);
        when(raceResultRepository.findAllByRaceEntity(raceEntity))
                .thenReturn(List.of(result1, result2, result3, result4));

        assertAll(
                () -> assertEquals(19, raceResultService.calculatePointsForRaceResult(result3)),
                () -> assertEquals(4, raceResultService.calculatePointsForRaceResult(result2)),
                () -> assertEquals(15, raceResultService.calculatePointsForRaceResult(result1)),
                () -> assertEquals(0, raceResultService.calculatePointsForRaceResult(result4))
        );
    }

    @Test
    public void deleteRaceResultByRaceResult_validEntity_Success() {
        DriverRaceResultEntity driverRaceResultEntity = DriverRaceResultEntity.builder()
                .driverRaceResultId(UUID.randomUUID().toString())
                .raceEntity(RaceEntity.builder().league(LeagueEntity.builder().build()).build())
                .build();
        raceResultService.deleteRaceResultByRaceResult(driverRaceResultEntity);

        verify(raceResultRepository).delete(driverRaceResultEntity);
    }

    @Test
    public void getRaceResultByDriverRaceResultEntityId_validId_Success() {
        DriverRaceResultEntity driverRaceResultEntity = DriverRaceResultEntity.builder()
                .driverRaceResultId(UUID.randomUUID().toString())
                .build();

        when(raceResultRepository.findById(driverRaceResultEntity.getDriverRaceResultId()))
                .thenReturn(java.util.Optional.of(driverRaceResultEntity));

        assertEquals(driverRaceResultEntity, raceResultService
                .getRaceResultByDriverRaceResultEntityId(
                        driverRaceResultEntity.getDriverRaceResultId()));
    }

    @Test
    public void save_validEntity_Success() {
        DriverRaceResultEntity driverRaceResultEntity = DriverRaceResultEntity.builder()
                .driverRaceResultId(UUID.randomUUID().toString())
                .build();

        when(raceResultRepository.save(driverRaceResultEntity)).thenReturn(driverRaceResultEntity);

        assertEquals(driverRaceResultEntity, raceResultService.save(driverRaceResultEntity));
    }

    @Test
    public void updateRaceResult_validRaceResult_Success() {
        DriverEntity driverEntity = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntityToUpdate = DriverRaceResultEntity.builder()
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.FERRARI)
                .driver(driverEntity)
                .endpostion(4)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(76.111))
                .raceEntity(raceEntity)
                .build();

        DriverRaceResult driverRaceResultWithUpdates = new DriverRaceResult();
        driverRaceResultWithUpdates.setStartposition(2);
        driverRaceResultWithUpdates.setEndpostion(3);
        driverRaceResultWithUpdates.setFastestLap(BigDecimal.valueOf(75.112));
        driverRaceResultWithUpdates.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        driverRaceResultWithUpdates.setCarInRace(Car.HAAS);
        driverRaceResultWithUpdates.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        driverRaceResultWithUpdates.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        driverRaceResultWithUpdates.setDriverRaceResultId(
                UUID.fromString(driverRaceResultEntityToUpdate.getDriverRaceResultId()));

        DriverRaceResultEntity expectedDriverEntity = DriverRaceResultEntity.builder()
                .driverRaceResultId(driverRaceResultWithUpdates.getDriverRaceResultId().toString())
                .carInRace(driverRaceResultWithUpdates.getCarInRace())
                .driver(driverEntity)
                .endpostion(driverRaceResultWithUpdates.getEndpostion())
                .startposition(driverRaceResultWithUpdates.getStartposition())
                .fastestLap(driverRaceResultWithUpdates.getFastestLap())
                .raceEntity(raceEntity)
                .build();

        when(driverService
                .getDriverByDriverId(driverRaceResultWithUpdates.getDriverId().toString()))
                .thenReturn(driverEntity);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);

        assertEquals(expectedDriverEntity, raceResultService
                .updateRaceResult(driverRaceResultEntityToUpdate, driverRaceResultWithUpdates));
    }

    @Test
    public void updateRaceResult_invalidRaceResult_Null() {
        DriverEntity driverEntity = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntityToUpdate = DriverRaceResultEntity.builder()
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.FERRARI)
                .driver(driverEntity)
                .startposition(2)
                .endpostion(3)
                .fastestLap(BigDecimal.valueOf(76.111))
                .raceEntity(raceEntity)
                .build();

        DriverRaceResult driverRaceResultWithUpdates = new DriverRaceResult();
        driverRaceResultWithUpdates.setEndpostion(3);
        driverRaceResultWithUpdates.setFastestLap(BigDecimal.valueOf(75.112));
        driverRaceResultWithUpdates.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        driverRaceResultWithUpdates.setCarInRace(Car.HAAS);
        driverRaceResultWithUpdates.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        driverRaceResultWithUpdates.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        driverRaceResultWithUpdates.setDriverRaceResultId(
                UUID.fromString(driverRaceResultEntityToUpdate.getDriverRaceResultId()));

        when(driverService
                .getDriverByDriverId(driverRaceResultWithUpdates.getDriverId().toString()))
                .thenReturn(driverEntity);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);

        assertNull(raceResultService
                .updateRaceResult(driverRaceResultEntityToUpdate, driverRaceResultWithUpdates));
    }

    @Test
    public void updateRaceResult_unknownDriverId_Null() {
        DriverEntity driverEntity = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntityToUpdate = DriverRaceResultEntity.builder()
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.FERRARI)
                .driver(driverEntity)
                .endpostion(4)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(76.111))
                .raceEntity(raceEntity)
                .build();

        DriverRaceResult driverRaceResultWithUpdates = new DriverRaceResult();
        driverRaceResultWithUpdates.setStartposition(2);
        driverRaceResultWithUpdates.setEndpostion(3);
        driverRaceResultWithUpdates.setFastestLap(BigDecimal.valueOf(75.112));
        driverRaceResultWithUpdates.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        driverRaceResultWithUpdates.setCarInRace(Car.HAAS);
        driverRaceResultWithUpdates.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        driverRaceResultWithUpdates.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        driverRaceResultWithUpdates.setDriverRaceResultId(
                UUID.fromString(driverRaceResultEntityToUpdate.getDriverRaceResultId()));

        when(driverService
                .getDriverByDriverId(driverRaceResultWithUpdates.getDriverId().toString()))
                .thenReturn(null);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(raceEntity);

        assertNull(raceResultService
                .updateRaceResult(driverRaceResultEntityToUpdate, driverRaceResultWithUpdates));
    }

    @Test
    public void updateRaceResult_unknownRaceEntityId_Null() {
        DriverEntity driverEntity = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntityToUpdate = DriverRaceResultEntity.builder()
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.FERRARI)
                .driver(driverEntity)
                .endpostion(4)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(76.111))
                .raceEntity(raceEntity)
                .build();

        DriverRaceResult driverRaceResultWithUpdates = new DriverRaceResult();
        driverRaceResultWithUpdates.setStartposition(2);
        driverRaceResultWithUpdates.setEndpostion(3);
        driverRaceResultWithUpdates.setFastestLap(BigDecimal.valueOf(75.112));
        driverRaceResultWithUpdates.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        driverRaceResultWithUpdates.setCarInRace(Car.HAAS);
        driverRaceResultWithUpdates.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        driverRaceResultWithUpdates.setRaceId(UUID.fromString(raceEntity.getRaceId()));
        driverRaceResultWithUpdates.setDriverRaceResultId(
                UUID.fromString(driverRaceResultEntityToUpdate.getDriverRaceResultId()));

        when(driverService
                .getDriverByDriverId(driverRaceResultWithUpdates.getDriverId().toString()))
                .thenReturn(driverEntity);
        when(raceService.getRaceByRaceId(raceEntity.getRaceId())).thenReturn(null);

        assertNull(raceResultService
                .updateRaceResult(driverRaceResultEntityToUpdate, driverRaceResultWithUpdates));
    }

}