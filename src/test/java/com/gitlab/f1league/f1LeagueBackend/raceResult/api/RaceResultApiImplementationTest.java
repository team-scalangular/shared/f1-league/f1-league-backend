package com.gitlab.f1league.f1LeagueBackend.raceResult.api;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.DriverRaceResult;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.services.RaceResultService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RaceResultApiImplementationTest {

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @MockBean
    RaceResultService raceResultService;
    @LocalServerPort
    private int port;

    @MockBean
    private AuthorizationService authorizationService;

    @BeforeEach
    public void setUp() {
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("leagueEditorToken", "UhNwklP");
        when(authorizationService.isValidToken(any(), (LeagueEntity) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
        when(authorizationService.isValidToken(any(), (String) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
    }

    @Test
    public void postListOfRaceResults_validRaceResults_200() {
        UUID driverId = UUID.randomUUID();
        UUID driver2Id = UUID.randomUUID();
        UUID raceId = UUID.randomUUID();

        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(raceId.toString())
                .league(leagueEntity)
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueEntity)
                .logoUrl("www.google.com")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamEntity)
                .driverId(driverId.toString())
                .build();

        DriverEntity driver2 = DriverEntity.builder()
                .name("Lindner")
                .surename("Moritz")
                .team(teamEntity)
                .driverId(driver2Id.toString())
                .build();

        DriverRaceResult driverRaceResult = new DriverRaceResult();
        driverRaceResult.setDriverId(driverId);
        driverRaceResult.setDriverName("Bieleke Max");
        driverRaceResult.setPoints(0);
        driverRaceResult.hasFastestLap(false);
        driverRaceResult.setCarInRace(Car.MERCEDES);
        driverRaceResult.setEndpostion(4);
        driverRaceResult.setFastestLap(BigDecimal.valueOf(78.3));
        driverRaceResult.setStartposition(4);
        driverRaceResult.setRaceId(raceId);
        driverRaceResult.setDriverRaceResultId(UUID.randomUUID());

        DriverRaceResultEntity entity1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(driverRaceResult.getStartposition())
                .fastestLap(driverRaceResult.getFastestLap())
                .endpostion(driverRaceResult.getEndpostion())
                .driver(driver1)
                .carInRace(driverRaceResult.getCarInRace())
                .driverRaceResultId(driverRaceResult.getDriverRaceResultId().toString())
                .build();

        DriverRaceResult driver2RaceResult = new DriverRaceResult();
        driver2RaceResult.setDriverName("Lindner Moritz");
        driver2RaceResult.setPoints(0);
        driver2RaceResult.setDriverId(driver2Id);
        driver2RaceResult.setCarInRace(Car.MERCEDES);
        driver2RaceResult.setEndpostion(5);
        driver2RaceResult.setFastestLap(BigDecimal.valueOf(77.3));
        driver2RaceResult.setHasFastestLap(false);
        driver2RaceResult.setStartposition(3);
        driver2RaceResult.setRaceId(raceId);
        driver2RaceResult.setDriverRaceResultId(UUID.randomUUID());

        DriverRaceResultEntity entity2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(driver2RaceResult.getStartposition())
                .fastestLap(driver2RaceResult.getFastestLap())
                .endpostion(driver2RaceResult.getEndpostion())
                .driver(driver2)
                .carInRace(driver2RaceResult.getCarInRace())
                .driverRaceResultId(driver2RaceResult.getDriverRaceResultId().toString())
                .build();

        when(raceResultService
                .buildDriverRaceResultEntityFromPost(driverRaceResult, leagueEntity.getLeagueId(),
                        raceId.toString()))
                .thenReturn(entity1);
        when(raceResultService
                .buildDriverRaceResultEntityFromPost(driver2RaceResult, leagueEntity.getLeagueId(),
                        raceId.toString()))
                .thenReturn(entity2);
        when(raceResultService.saveAll(anyList())).thenReturn(List.of(entity1, entity2));

        List<DriverRaceResult> resultListToPost = List.of(driverRaceResult, driver2RaceResult);

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(resultListToPost, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("raceResults"))
                        .queryParam("leagueId", leagueEntity.getLeagueId())
                        .queryParam("raceId", raceId);

        ResponseEntity<List<DriverRaceResult>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity,
                        new ParameterizedTypeReference<List<DriverRaceResult>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(resultListToPost, response.getBody())
        );
    }

    @Test
    public void postListOfRaceResults_wrongRaceId_400() {
        UUID driverId = UUID.randomUUID();
        UUID driver2Id = UUID.randomUUID();
        UUID raceId = UUID.randomUUID();

        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(raceId.toString())
                .league(leagueEntity)
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueEntity)
                .logoUrl("www.google.com")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamEntity)
                .driverId(driverId.toString())
                .build();

        DriverEntity driver2 = DriverEntity.builder()
                .name("Lindner")
                .surename("Moritz")
                .team(teamEntity)
                .driverId(driver2Id.toString())
                .build();

        DriverRaceResult driverRaceResult = new DriverRaceResult();
        driverRaceResult.setDriverId(driverId);
        driverRaceResult.setCarInRace(Car.MERCEDES);
        driverRaceResult.setEndpostion(4);
        driverRaceResult.setFastestLap(BigDecimal.valueOf(78.3));
        driverRaceResult.setStartposition(4);
        driverRaceResult.setRaceId(raceId);
        driverRaceResult.setDriverRaceResultId(UUID.randomUUID());

        DriverRaceResultEntity entity1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(driverRaceResult.getStartposition())
                .fastestLap(driverRaceResult.getFastestLap())
                .endpostion(driverRaceResult.getEndpostion())
                .driver(driver1)
                .carInRace(driverRaceResult.getCarInRace())
                .driverRaceResultId(driverRaceResult.getDriverRaceResultId().toString())
                .build();

        DriverRaceResult driver2RaceResult = new DriverRaceResult();
        driver2RaceResult.setDriverId(driver2Id);
        driver2RaceResult.setCarInRace(Car.MERCEDES);
        driver2RaceResult.setEndpostion(5);
        driver2RaceResult.setFastestLap(BigDecimal.valueOf(77.3));
        driver2RaceResult.setStartposition(3);
        driver2RaceResult.setRaceId(raceId);
        driver2RaceResult.setDriverRaceResultId(UUID.randomUUID());

        DriverRaceResultEntity entity2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(driver2RaceResult.getStartposition())
                .fastestLap(driver2RaceResult.getFastestLap())
                .endpostion(driver2RaceResult.getEndpostion())
                .driver(driver2)
                .carInRace(driver2RaceResult.getCarInRace())
                .driverRaceResultId(driver2RaceResult.getDriverRaceResultId().toString())
                .build();

        when(raceResultService
                .buildDriverRaceResultEntityFromPost(any(), any(),
                        any())).thenReturn(null);
        when(raceResultService.saveAll(anyList())).thenReturn(List.of(entity1, entity2));

        List<DriverRaceResult> resultListToPost = List.of(driverRaceResult, driver2RaceResult);

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(resultListToPost, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("raceResults"))
                        .queryParam("leagueId", leagueEntity.getLeagueId())
                        .queryParam("raceId", raceId);

        ResponseEntity<List<DriverRaceResult>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity,
                        new ParameterizedTypeReference<List<DriverRaceResult>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void getRaceResultsByRaceId_validIds_200() {
        UUID driverId = UUID.randomUUID();
        UUID raceId = UUID.randomUUID();

        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(raceId.toString())
                .league(leagueEntity)
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueEntity)
                .logoUrl("www.google.com")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamEntity)
                .driverId(driverId.toString())
                .build();

        DriverRaceResult driverRaceResult = new DriverRaceResult();
        driverRaceResult.setDriverId(driverId);
        driverRaceResult.setPoints(18);
        driverRaceResult.setCarInRace(Car.MERCEDES);
        driverRaceResult.setEndpostion(4);
        driverRaceResult.setFastestLap(BigDecimal.valueOf(78.3));
        driverRaceResult.setStartposition(4);
        driverRaceResult.setRaceId(raceId);
        driverRaceResult.setHasFastestLap(false);
        driverRaceResult.setDriverName("Bieleke Max");
        driverRaceResult.setPoints(0);
        driverRaceResult.setDriverRaceResultId(UUID.randomUUID());

        DriverRaceResultEntity driverRaceResultEntity = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(driverRaceResult.getStartposition())
                .fastestLap(driverRaceResult.getFastestLap())
                .endpostion(driverRaceResult.getEndpostion())
                .driver(driver1)
                .carInRace(driverRaceResult.getCarInRace())
                .driverRaceResultId(driverRaceResult.getDriverRaceResultId().toString())
                .build();

        when(raceResultService
                .getRaceResultsByRaceIdAndDriverId(raceId.toString(), driverId.toString()))
                .thenReturn(List.of(driverRaceResultEntity));

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("raceResults"))
                        .queryParam("raceId", raceEntity.getRaceId())
                        .queryParam("driverId", driver1.getDriverId());

        ResponseEntity<List<DriverRaceResult>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<DriverRaceResult>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(List.of(driverRaceResult), response.getBody())
        );
    }

    @Test
    public void getRaceResultsByRaceId_NotByDriverId_200() {
        UUID driverId = UUID.randomUUID();
        UUID raceId = UUID.randomUUID();

        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(raceId.toString())
                .league(leagueEntity)
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueEntity)
                .logoUrl("www.google.com")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamEntity)
                .driverId(driverId.toString())
                .build();

        DriverEntity driver2 = DriverEntity.builder()
                .name("Moritz")
                .surename("Lindner")
                .team(teamEntity)
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverRaceResult driverRaceResult = new DriverRaceResult();
        driverRaceResult.setDriverName("Bieleke Max");
        driverRaceResult.setPoints(0);
        driverRaceResult.setDriverId(driverId);
        driverRaceResult.setCarInRace(Car.MERCEDES);
        driverRaceResult.setEndpostion(4);
        driverRaceResult.setFastestLap(BigDecimal.valueOf(78.3));
        driverRaceResult.hasFastestLap(false);
        driverRaceResult.setStartposition(4);
        driverRaceResult.setRaceId(raceId);
        driverRaceResult.setDriverRaceResultId(UUID.randomUUID());

        DriverRaceResult driverRaceResult2 = new DriverRaceResult();
        driverRaceResult2.setDriverName("Moritz Lindner");
        driverRaceResult2.setPoints(0);
        driverRaceResult2.setDriverId(UUID.fromString(driver2.getDriverId()));
        driverRaceResult2.setCarInRace(Car.MERCEDES);
        driverRaceResult2.setEndpostion(4);
        driverRaceResult2.hasFastestLap(false);
        driverRaceResult2.setFastestLap(BigDecimal.valueOf(78.3));
        driverRaceResult2.setStartposition(4);
        driverRaceResult2.setRaceId(raceId);
        driverRaceResult2.setDriverRaceResultId(UUID.randomUUID());

        DriverRaceResultEntity driverRaceResultEntity = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(driverRaceResult.getStartposition())
                .fastestLap(driverRaceResult.getFastestLap())
                .endpostion(driverRaceResult.getEndpostion())
                .driver(driver1)
                .carInRace(driverRaceResult.getCarInRace())
                .driverRaceResultId(driverRaceResult.getDriverRaceResultId().toString())
                .build();

        DriverRaceResultEntity driverRaceResultEntity2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(driverRaceResult.getStartposition())
                .fastestLap(driverRaceResult.getFastestLap())
                .endpostion(driverRaceResult.getEndpostion())
                .driver(driver2)
                .carInRace(driverRaceResult.getCarInRace())
                .driverRaceResultId(driverRaceResult2.getDriverRaceResultId().toString())
                .build();

        when(raceResultService
                .getRaceResultsByRaceId(raceId.toString()))
                .thenReturn(List.of(driverRaceResultEntity, driverRaceResultEntity2));

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("raceResults"))
                        .queryParam("raceId", raceEntity.getRaceId());

        ResponseEntity<List<DriverRaceResult>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<DriverRaceResult>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(List.of(driverRaceResult, driverRaceResult2),
                        response.getBody())
        );
    }

    @Test
    public void getRaceResultsByRaceId_ByDriverIdWithInvalidId_NoContent() {
        UUID driverId = UUID.randomUUID();
        UUID raceId = UUID.randomUUID();

        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(raceId.toString())
                .league(leagueEntity)
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueEntity)
                .logoUrl("www.google.com")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamEntity)
                .driverId(driverId.toString())
                .build();

        DriverEntity driver2 = DriverEntity.builder()
                .name("Moritz")
                .surename("Lindner")
                .team(teamEntity)
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverRaceResult driverRaceResult = new DriverRaceResult();
        driverRaceResult.setDriverId(driverId);
        driverRaceResult.setCarInRace(Car.MERCEDES);
        driverRaceResult.setEndpostion(4);
        driverRaceResult.setFastestLap(BigDecimal.valueOf(78.3));
        driverRaceResult.setStartposition(4);
        driverRaceResult.setRaceId(raceId);
        driverRaceResult.setDriverRaceResultId(UUID.randomUUID());

        DriverRaceResult driverRaceResult2 = new DriverRaceResult();
        driverRaceResult2.setDriverId(UUID.fromString(driver2.getDriverId()));
        driverRaceResult2.setCarInRace(Car.MERCEDES);
        driverRaceResult2.setEndpostion(4);
        driverRaceResult2.setFastestLap(BigDecimal.valueOf(78.3));
        driverRaceResult2.setStartposition(4);
        driverRaceResult2.setRaceId(raceId);
        driverRaceResult2.setDriverRaceResultId(UUID.randomUUID());

        when(raceResultService
                .getRaceResultsByRaceIdAndDriverId(raceId.toString(), driver1.getDriverId()))
                .thenReturn(new ArrayList<>());

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("raceResults"))
                        .queryParam("raceId", raceEntity.getRaceId())
                        .queryParam("driverId", driver1.getDriverId());

        ResponseEntity<List<DriverRaceResult>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<DriverRaceResult>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void deleteRaceResult_GivenValidId_Expect200() {
        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .league(leagueEntity)
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueEntity)
                .logoUrl("www.google.com")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamEntity)
                .driverId(UUID.randomUUID().toString())
                .build();

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("raceResults"))
                        .queryParam("raceId", raceEntity.getRaceId())
                        .queryParam("driverId", driver1.getDriverId())
                        .queryParam("leagueId", leagueEntity.getLeagueId());

        ResponseEntity<DriverRaceResult> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity,
                        DriverRaceResult.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/" + uri;
    }
}