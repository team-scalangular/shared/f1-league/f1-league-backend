package com.gitlab.f1league.f1LeagueBackend.raceResult.repository;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.repository.DriverRepository;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.repository.LeagueRepository;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.repository.RaceRepository;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.repository.TeamRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class RaceResultRepositoryTest {
    @Autowired
    RaceResultRepository raceResultRepository;

    @Autowired
    RaceRepository raceRepository;

    @Autowired
    DriverRepository driverRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    LeagueRepository leagueRepository;

    RaceEntity raceEntity;
    DriverEntity driverEntity;

    @BeforeEach
    public void setUp() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .build();

        leagueRepository.save(leagueA);

        TeamEntity teamA = TeamEntity
                .builder()
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();

        teamRepository.save(teamA);

        raceEntity = RaceEntity.builder()
                .league(leagueA)
                .build();

        raceRepository.save(raceEntity);

        driverEntity = DriverEntity.builder()
                .team(teamA)
                .surename("Bieleke")
                .name("Max")
                .build();

        driverRepository.save(driverEntity);
    }

    @Test
    public void findByRaceEntityAndDriver_GivenValidIds_Success() {
        DriverRaceResultEntity driverRaceResultEntityToSave = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(117.3))
                .endpostion(1)
                .driver(driverEntity)
                .carInRace(Car.HAAS)
                .build();

        raceResultRepository.save(driverRaceResultEntityToSave);

        assertAll(
                () -> assertEquals(1,
                        raceResultRepository.findAllByRaceEntityAndDriver(raceEntity, driverEntity)
                                .size()),
                () -> assertEquals(List.of(driverRaceResultEntityToSave),
                        raceResultRepository.findAllByRaceEntityAndDriver(raceEntity, driverEntity))
        );
    }

}