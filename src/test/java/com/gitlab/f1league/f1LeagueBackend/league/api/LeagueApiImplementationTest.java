package com.gitlab.f1league.f1LeagueBackend.league.api;

import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.League;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.repository.LeagueRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LeagueApiImplementationTest {

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @MockBean
    LeagueRepository leagueRepository;
    @LocalServerPort
    private int port;
    @MockBean
    private AuthorizationService authorizationService;

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/" + uri;
    }

    @BeforeEach
    public void setUp() {
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("leagueEditorToken", "UhNwklP");
        when(authorizationService.isValidToken(any(), (LeagueEntity) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
        when(authorizationService.isValidToken(any(), (String) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
    }

    @Test
    public void patchLeague_ValidLeague_Success() {
        LeagueEntity leagueEntityBeforeUpdate = LeagueEntity.builder()
                .name("MyLeague")
                .leagueId(UUID.randomUUID().toString())
                .leagueEditorToken("UhNwklP")
                .build();

        League leagueWithUpdates = new League();
        leagueWithUpdates.setListOfRaceIds(new ArrayList<>());
        leagueWithUpdates.setListOfTeamIds(new ArrayList<>());
        leagueWithUpdates.setName("NEw LEague");
        leagueWithUpdates.setLeagueId(UUID.fromString(leagueEntityBeforeUpdate.getLeagueId()));

        when(leagueRepository.findById(leagueEntityBeforeUpdate.getLeagueId()))
                .thenReturn(java.util.Optional.of(leagueEntityBeforeUpdate));
        when(leagueRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("league"))
                        .queryParam("leagueId", leagueEntityBeforeUpdate.getLeagueId());

        HttpEntity<League> entity = new HttpEntity<>(leagueWithUpdates, headers);

        ResponseEntity<League> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.PATCH, entity,
                        League.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(leagueWithUpdates, response.getBody())
        );
    }

    @Test
    public void patchLeague_InvalidLeague_400() {
        LeagueEntity leagueEntityBeforeUpdate = LeagueEntity.builder()
                .name("MyLeague")
                .leagueId(UUID.randomUUID().toString())
                .leagueEditorToken("UhNwklP")
                .build();

        League leagueWithUpdates = new League();
        leagueWithUpdates.setListOfRaceIds(new ArrayList<>());
        leagueWithUpdates.setListOfTeamIds(new ArrayList<>());
        leagueWithUpdates.setLeagueId(UUID.fromString(leagueEntityBeforeUpdate.getLeagueId()));

        when(leagueRepository.findById(leagueEntityBeforeUpdate.getLeagueId()))
                .thenReturn(java.util.Optional.of(leagueEntityBeforeUpdate));
        when(leagueRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("league"))
                        .queryParam("leagueId", leagueEntityBeforeUpdate.getLeagueId());

        HttpEntity<League> entity = new HttpEntity<>(leagueWithUpdates, headers);

        ResponseEntity<League> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.PATCH, entity,
                        League.class);

        assertAll(
                () -> assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }
}