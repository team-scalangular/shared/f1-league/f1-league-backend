package com.gitlab.f1league.f1LeagueBackend.league.services;

import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.League;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class LeagueModelConverterTest {
    private LeagueModelConverter leagueModelConverter;
    @MockBean
    private TeamService teamService;
    @MockBean
    private RaceService raceService;

    @BeforeEach
    public void setUp() {
        this.leagueModelConverter = new LeagueModelConverter(teamService, raceService);
    }

    @Test
    public void transferModelToInternalModel_givenTransferModel_expectSuccess() {
        League leagueToConvert = new League();
        leagueToConvert.setLeagueId(UUID.randomUUID());
        leagueToConvert.setListOfRaceIds(
                List.of(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        leagueToConvert.setListOfTeamIds(
                List.of(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        leagueToConvert.setName("MyTeam");

        LeagueEntity expectedResult = LeagueEntity.builder()
                .leagueId(leagueToConvert.getLeagueId().toString())
                .name(leagueToConvert.getName())
                .build();

        assertEquals(expectedResult,
                leagueModelConverter.transferModelToInternalModel(leagueToConvert));
    }

    @Test
    public void internalModelToTransferModel_givenInternalModel_expectSuccess() {
        LeagueEntity leagueEntityToConvert = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .name("MyTeam")
                .leagueEditorToken("RandomToken")
                .build();

        List<RaceEntity> raceEntities = List.of(
                RaceEntity.builder()
                        .raceId(UUID.randomUUID().toString())
                        .build(),
                RaceEntity.builder()
                        .raceId(UUID.randomUUID().toString())
                        .build());
        List<TeamEntity> teamEntities = List.of(
                TeamEntity.builder()
                        .teamId(UUID.randomUUID().toString())
                        .build()
        );

        League expectedLeague = new League();
        expectedLeague.setLeagueId(UUID.fromString(leagueEntityToConvert.getLeagueId()));
        expectedLeague.setListOfRaceIds(raceEntities.stream()
                .map(RaceEntity::getRaceId)
                .collect(Collectors.toList()));
        expectedLeague.setListOfTeamIds(teamEntities.stream()
                .map(TeamEntity::getTeamId)
                .collect(Collectors.toList()));
        expectedLeague.setName("MyTeam");

        when(teamService.getAllTeamsByLeague(leagueEntityToConvert)).thenReturn(teamEntities);
        when(raceService.getAllRacesByLeague(leagueEntityToConvert)).thenReturn(raceEntities);

        assertEquals(expectedLeague,
                leagueModelConverter.internalModelToTransferModel(leagueEntityToConvert));
    }
}