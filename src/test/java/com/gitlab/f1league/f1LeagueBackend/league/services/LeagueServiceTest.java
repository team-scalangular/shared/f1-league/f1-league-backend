package com.gitlab.f1league.f1LeagueBackend.league.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverClassificationCacheService;
import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.League;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.repository.LeagueRepository;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class LeagueServiceTest {

    LeagueService leagueService;

    @MockBean
    LeagueRepository leagueRepository;

    @MockBean
    TeamService teamService;

    @MockBean
    RaceService raceService;

    @MockBean
    private AuthorizationService authorizationService;

    @MockBean
    private DriverClassificationCacheService driverClassificationCacheService;

    @BeforeEach
    public void setUp() {
        leagueService = new LeagueService(leagueRepository, teamService, raceService,
                driverClassificationCacheService);
    }

    @Test
    public void getLeagueById_KnownId_ExpectedLeague() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();
        when(leagueRepository.findById(leagueEntity.getLeagueId())).thenReturn(
                java.util.Optional.of(leagueEntity));

        assertEquals(leagueEntity, leagueService.getLeagueById(leagueEntity.getLeagueId()));
    }

    @Test
    public void getLeagueById_UnknownId_ExpectedLeague() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();
        when(leagueRepository.findById(leagueEntity.getLeagueId())).thenReturn(Optional.empty());

        assertNull(leagueService.getLeagueById(leagueEntity.getLeagueId()));
    }

    @Test
    public void deleteLeagueBuId_knownId_EverythingBelongingToLeagueDeleted() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        List<TeamEntity> teamEntities =
                List.of(TeamEntity.builder().build(), TeamEntity.builder().build());

        List<RaceEntity> raceEntities =
                List.of(RaceEntity.builder().build(), RaceEntity.builder().build());

        when(leagueRepository.findById(leagueEntity.getLeagueId())).thenReturn(
                Optional.of(leagueEntity));
        when(teamService.getAllTeamsByLeague(leagueEntity)).thenReturn(teamEntities);
        when(raceService.getAllRacesByLeague(leagueEntity)).thenReturn(raceEntities);

        leagueService.deleteLeagueBuId(leagueEntity.getLeagueId());

        assertAll(
                () -> verify(leagueRepository).deleteById(any()),
                () -> verify(teamService, times(2)).deleteTeamById(any()),
                () -> verify(raceService, times(2)).deleteRaceById(any())
        );
    }

    @Test
    public void deleteLeagueBuId_unknownId_EverythingBelongingToLeagueDeleted() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        when(leagueRepository.findById(leagueEntity.getLeagueId())).thenReturn(
                Optional.empty());

        leagueService.deleteLeagueBuId(leagueEntity.getLeagueId());

        verify(leagueRepository).findById(any());
    }

    @Test
    public void getAllLeagues_GivenLeagues_Success() {
        List<LeagueEntity> leagueEntities = List.of(LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build(), LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build());

        when(leagueRepository.findAll()).thenReturn(leagueEntities);

        assertEquals(leagueEntities, leagueService.getAllLeagues());
    }

    @Test
    public void buildLeagueEntityFromPost_ValidLeague_False() {
        League validLeague = new League();
        validLeague.setName("My League");

        LeagueEntity buildLeagueEntity = leagueService.buildLeagueEntityFromPost(validLeague);

        LeagueEntity expected = LeagueEntity.builder()
                .name(validLeague.getName())
                .leagueEditorToken(buildLeagueEntity.getLeagueEditorToken())
                .build();

        assertEquals(expected, buildLeagueEntity);
    }

    @Test
    public void buildLeagueEntityFromPost_InvalidLeague_True() {
        League invalidLeague = new League();

        assertNull(leagueService.buildLeagueEntityFromPost(invalidLeague));
    }

    @Test
    public void saveLeagueEntity_LeagueEntity_Success() {
        LeagueEntity expected = LeagueEntity.builder()
                .name("MyLeague")
                .leagueId(UUID.randomUUID().toString())
                .build();

        when(leagueRepository.save(expected)).thenReturn(expected);

        assertEquals(expected, leagueService.saveLeagueEntity(expected));
    }

    @Test
    public void updateLeagueEntity_ValidLeagueWithUpdates_Success() {
        League leagueWithUpdates = new League();
        leagueWithUpdates.setName("MyLeague");
        leagueWithUpdates.setLeagueId(UUID.randomUUID());

        LeagueEntity savedLeagueEntity = LeagueEntity.builder()
                .leagueId(leagueWithUpdates.getLeagueId().toString())
                .name("The League")
                .leagueEditorToken("MyToken")
                .build();

        LeagueEntity expectedLeagueEntity = LeagueEntity.builder()
                .leagueId(leagueWithUpdates.getLeagueId().toString())
                .name(leagueWithUpdates.getName())
                .leagueEditorToken(savedLeagueEntity.getLeagueEditorToken())
                .build();

        when(leagueRepository.findById(leagueWithUpdates.getLeagueId().toString()))
                .thenReturn(Optional.ofNullable(savedLeagueEntity));

        assertEquals(expectedLeagueEntity, leagueService.updateLeagueEntity(leagueWithUpdates));
    }

    @Test
    public void updateLeagueEntity_InvalidLeagueWithUpdates_Null() {
        League leagueWithUpdates = new League();
        leagueWithUpdates.setLeagueId(UUID.randomUUID());

        LeagueEntity savedLeagueEntity = LeagueEntity.builder()
                .leagueId(leagueWithUpdates.getLeagueId().toString())
                .name("The League")
                .leagueEditorToken("MyOldToken")
                .build();

        when(leagueRepository.findById(leagueWithUpdates.getLeagueId().toString()))
                .thenReturn(Optional.ofNullable(savedLeagueEntity));

        assertNull(leagueService.updateLeagueEntity(leagueWithUpdates));
    }

    @Test
    public void updateLeagueEntity_ValidLeagueWithUpdatesButUnknownLeague_Null() {
        League leagueWithUpdates = new League();
        leagueWithUpdates.setName("RandomName");
        leagueWithUpdates.setLeagueId(UUID.randomUUID());

        when(leagueRepository.findById(leagueWithUpdates.getLeagueId().toString()))
                .thenReturn(Optional.empty());

        assertNull(leagueService.updateLeagueEntity(leagueWithUpdates));
    }

    @RepeatedTest(30)
    public void generateLeagueEditorToken_GivenLeague_Success() {
        String token = leagueService.generateLeagueEditorToken();

        assertAll(
                () -> assertNotNull(token),
                () -> assertEquals(7, token.length()),
                () -> assertTrue(Pattern.matches("[a-zA-Z]+", token)),
                () -> assertFalse(Pattern.matches("[0-9]+", token))
        );
    }

    @Test
    public void sortRaceBasedOnLatestNextEvent_givenList_Success() {
        LeagueEntity leagueEntity1 = LeagueEntity.builder()
                .name("League1")
                .build();

        LeagueEntity leagueEntity2 = LeagueEntity.builder()
                .name("League2")
                .build();

        LeagueEntity leagueEntity3 = LeagueEntity.builder()
                .name("League3")
                .build();

        RaceEntity latestRaceEntity = RaceEntity.builder()
                .raceDate(LocalDate.now().plusDays(5))
                .build();

        RaceEntity thridRaceEntity = RaceEntity.builder()
                .raceDate(LocalDate.now().plusDays(4))
                .build();

        RaceEntity secondRaceEntity = RaceEntity.builder()
                .raceDate(LocalDate.now().plusDays(3))
                .build();

        RaceEntity nextRaceEntity = RaceEntity.builder()
                .raceDate(LocalDate.now().plusDays(2))
                .build();

        RaceEntity passedRaceEntity = RaceEntity.builder()
                .raceDate(LocalDate.now().minusDays(2))
                .build();

        when(raceService.getAllRacesByLeague(leagueEntity1))
                .thenReturn(List.of(passedRaceEntity, latestRaceEntity));
        when(raceService.getAllRacesByLeague(leagueEntity2))
                .thenReturn(List.of(thridRaceEntity, nextRaceEntity));
        when(raceService.getAllRacesByLeague(leagueEntity3))
                .thenReturn(List.of(secondRaceEntity));

        assertAll(
                () -> assertEquals(List.of(leagueEntity2, leagueEntity3, leagueEntity1),
                        leagueService
                                .sortLeaguesBasedOnLatestNextEvent(
                                        List.of(leagueEntity1, leagueEntity2, leagueEntity3))),
                () -> assertEquals(List.of(leagueEntity2, leagueEntity3, leagueEntity1),
                        leagueService
                                .sortLeaguesBasedOnLatestNextEvent(
                                        List.of(leagueEntity3, leagueEntity1, leagueEntity2))),
                () -> assertEquals(List.of(leagueEntity2, leagueEntity3, leagueEntity1),
                        leagueService
                                .sortLeaguesBasedOnLatestNextEvent(
                                        List.of(leagueEntity2, leagueEntity3, leagueEntity1))),
                () -> assertEquals(List.of(leagueEntity2, leagueEntity3, leagueEntity1),
                        leagueService
                                .sortLeaguesBasedOnLatestNextEvent(
                                        List.of(leagueEntity3, leagueEntity2, leagueEntity1))),
                () -> assertEquals(List.of(leagueEntity2, leagueEntity3, leagueEntity1),
                        leagueService
                                .sortLeaguesBasedOnLatestNextEvent(
                                        List.of(leagueEntity2, leagueEntity3, leagueEntity1)))
        );
    }
}