package com.gitlab.f1league.f1LeagueBackend.race.services;

import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Circuit;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Race;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RaceModelConverterTest {

    RaceModelConverter raceModelConverter;

    @MockBean
    RaceService raceService;

    @BeforeEach
    public void setUp() {
        raceModelConverter = new RaceModelConverter(raceService);
    }

    @Test
    public void transferModelToInternalModel_givenRace_Success() {
        Race race = new Race();
        race.setCircuit(Circuit.BELGIUM);
        race.setHasBeenDriven(false);
        race.setRaceDate(LocalDate.now());
        race.setRaceId(UUID.randomUUID());

        LeagueEntity leagueEntity = LeagueEntity.builder().build();

        RaceEntity expectedRaceEntity = RaceEntity.builder()
                .raceId(race.getRaceId().toString())
                .league(leagueEntity)
                .hasBeenDriven(race.getHasBeenDriven())
                .raceDate(race.getRaceDate())
                .build();

        when(raceService.findRaceEntityByRace(race)).thenReturn(
                java.util.Optional.ofNullable(expectedRaceEntity));

        assertEquals(expectedRaceEntity, raceModelConverter.transferModelToInternalModel(race));
    }

    @Test
    public void transferModelToInternalModel_givenRaceWithUnknownLeague_Success() {
        Race race = new Race();
        race.setCircuit(Circuit.BELGIUM);
        race.setHasBeenDriven(false);
        race.setRaceDate(LocalDate.now());
        race.setRaceId(UUID.randomUUID());
        race.setRacePositionIndex(4);

        RaceEntity expectedRaceEntity = RaceEntity.builder()
                .raceId(race.getRaceId().toString())
                .league(null)
                .hasBeenDriven(race.getHasBeenDriven())
                .raceDate(race.getRaceDate())
                .racePositionIndex(race.getRacePositionIndex())
                .build();

        when(raceService.findRaceEntityByRace(race)).thenReturn(
                Optional.empty());

        assertEquals(expectedRaceEntity, raceModelConverter.transferModelToInternalModel(race));
    }

    @Test
    public void internalModelToTransferModel_givenRaceEntity_Success() {
        LeagueEntity leagueEntity = LeagueEntity.builder().build();

        RaceEntity raceEntityToConvert = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .league(leagueEntity)
                .circuit(Circuit.BELGIUM)
                .hasBeenDriven(true)
                .raceDate(LocalDate.now())
                .racePositionIndex(2)
                .build();

        Race expectedResult = new Race();
        expectedResult.setCircuit(raceEntityToConvert.getCircuit());
        expectedResult.setHasBeenDriven(raceEntityToConvert.getHasBeenDriven());
        expectedResult.setRaceDate(raceEntityToConvert.getRaceDate());
        expectedResult.setRaceId(UUID.fromString(raceEntityToConvert.getRaceId()));
        expectedResult.setRacePositionIndex(raceEntityToConvert.getRacePositionIndex());

        assertEquals(expectedResult,
                raceModelConverter.internalModelToTransferModel(raceEntityToConvert));
    }
}