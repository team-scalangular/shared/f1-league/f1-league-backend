package com.gitlab.f1league.f1LeagueBackend.race.api;

import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.DriverRaceResult;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Race;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.services.RaceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RaceApiImplementationTest {

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @MockBean
    RaceService raceService;

    @MockBean
    LeagueService leagueService;

    @MockBean
    private AuthorizationService authorizationService;

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("leagueEditorToken", "UhNwklP");
        when(authorizationService.isValidToken(any(), (LeagueEntity) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
        when(authorizationService.isValidToken(any(), (String) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
    }

    @Test
    public void deleteRace_validId_200() {
        Race raceToDelete = new Race();
        raceToDelete.setRaceId(UUID.randomUUID());

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("race"))
                        .queryParam("leagueId", UUID.randomUUID())
                        .queryParam("raceId", raceToDelete.getRaceId());

        HttpEntity<Race> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Race> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity,
                        Race.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertNull(response.getBody()),
                () -> verify(raceService).deleteRaceById(any())
        );
    }

    @Test
    public void getRaces_validLeagueId_200() {
        String raceUUID = UUID.randomUUID().toString();
        Race expectedRace = new Race();
        expectedRace.setRaceId(UUID.fromString(raceUUID));
        List<Race> expectedResult = List.of(expectedRace);
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);
        when(raceService.getAllRacesByLeague(leagueEntity))
                .thenReturn(List.of(RaceEntity.builder().raceId(raceUUID).build()));

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("race"))
                        .queryParam("leagueId", leagueEntity.getLeagueId());

        ResponseEntity<List<Race>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Race>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(expectedResult, response.getBody())
        );
    }

    @Test
    public void getRaces_validLeagueIdNoContent_204() {
        String raceUUID = UUID.randomUUID().toString();
        Race expectedRace = new Race();
        expectedRace.setRaceId(UUID.fromString(raceUUID));
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);
        when(raceService.getAllRacesByLeague(leagueEntity))
                .thenReturn(List.of());

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("race"))
                        .queryParam("leagueId", leagueEntity.getLeagueId());

        ResponseEntity<List<Race>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Race>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void getRaces_invalidLeagueId_NotFound() {
        String raceUUID = UUID.randomUUID().toString();
        Race expectedRace = new Race();
        expectedRace.setRaceId(UUID.fromString(raceUUID));
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(null);

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("race"))
                        .queryParam("leagueId", leagueEntity.getLeagueId());

        ResponseEntity<List<Race>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Race>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void getRaces_NoLeagueIdProvided_400() {
        String raceUUID = UUID.randomUUID().toString();
        Race expectedRace = new Race();
        expectedRace.setRaceId(UUID.fromString(raceUUID));
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(null);

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("race"));

        ResponseEntity<Race> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        Race.class);

        assertAll(
                () -> assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode())
        );
    }

    @Test
    public void getRaces_validLeagueIdAndValidRaceId_200() {
        Race raceToGet = new Race();
        raceToGet.setRaceId(UUID.randomUUID());

        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(raceToGet.getRaceId().toString())
                .build();

        List<Race> expectedResult = List.of(raceToGet);
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();


        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);
        when(raceService.getRaceByRaceId(raceToGet.getRaceId().toString())).thenReturn(raceEntity);

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("race"))
                        .queryParam("leagueId", leagueEntity.getLeagueId())
                        .queryParam("raceId", raceToGet.getRaceId());

        ResponseEntity<List<Race>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Race>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(expectedResult, response.getBody())
        );
    }

    @Test
    public void getRaces_validLeagueIdAndInvalidRaceId_204() {
        Race raceToGet = new Race();
        raceToGet.setRaceId(UUID.randomUUID());

        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);
        when(raceService.getRaceByRaceId(raceToGet.getRaceId().toString())).thenReturn(null);

        HttpEntity<List<DriverRaceResult>> entity = new HttpEntity<>(null, headers);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("race"))
                        .queryParam("leagueId", leagueEntity.getLeagueId())
                        .queryParam("raceId", raceToGet.getRaceId());

        ResponseEntity<List<Race>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Race>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/" + uri;
    }

}