package com.gitlab.f1league.f1LeagueBackend.race.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Circuit;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Race;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.race.repository.RaceRepository;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.services.RaceResultService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class RaceServiceTest {
    RaceService raceService;

    @MockBean
    RaceRepository raceRepository;

    @MockBean
    RaceResultService raceResultService;

    @MockBean
    LeagueService leagueService;

    @BeforeEach
    public void setUp() {
        raceService = new RaceService(raceRepository);
        raceService.setRaceResultService(raceResultService);
        raceService.setLeagueService(leagueService);
    }

    @Test
    public void getRaceByRaceId_KnownId_Success() {
        RaceEntity raceEntity = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        when(raceRepository.findById(raceEntity.getRaceId())).thenReturn(
                java.util.Optional.of(raceEntity));

        assertEquals(raceEntity, raceService.getRaceByRaceId(raceEntity.getRaceId()));
    }

    @Test
    public void getRaceByRaceId_UnknownId_Null() {
        when(raceRepository.findById(any())).thenReturn(Optional.empty());
        assertNull(raceService.getRaceByRaceId(UUID.randomUUID().toString()));
    }

    @Test
    public void getRaceByLeague_League_ListOfRaces() {
        LeagueEntity leagueEntityToFindRacesFor = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        List<RaceEntity> raceEntities = List.of(RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build());

        when(raceRepository.findAllByLeague(leagueEntityToFindRacesFor)).thenReturn(raceEntities);

        assertEquals(raceEntities, raceService.getAllRacesByLeague(leagueEntityToFindRacesFor));
    }

    @Test
    public void deleteRaceById_GivenRaceEntityWithResults_Success() {
        RaceEntity raceEntityToDelete = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverRaceResultEntity result1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntityToDelete)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.560))
                .endpostion(2)
                .driver(DriverEntity.builder().build())
                .carInRace(Car.FERRARI)
                .build();

        when(raceRepository.findById(raceEntityToDelete.getRaceId())).thenReturn(
                Optional.of(raceEntityToDelete));
        when(raceResultService.getRaceResultsByRaceId(raceEntityToDelete.getRaceId()))
                .thenReturn(List.of(result1));

        raceService.deleteRaceById(raceEntityToDelete.getRaceId());

        verify(raceResultService, times(1)).deleteRaceResultByRaceResult(result1);
        verify(raceRepository, times(1)).delete(raceEntityToDelete);
    }

    @Test
    public void deleteRaceById_GivenUnknownRace_Success() {
        RaceEntity raceEntityToDelete = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .build();

        when(raceRepository.findById(raceEntityToDelete.getRaceId())).thenReturn(
                Optional.empty());

        raceService.deleteRaceById(raceEntityToDelete.getRaceId());
    }

    @Test
    public void findRaceEntityByRace_GivenRace_Success() {
        Race race = new Race();
        race.setRaceId(UUID.randomUUID());

        RaceEntity raceEntity = RaceEntity.builder().raceId(UUID.randomUUID().toString()).build();

        when(raceRepository.findById(race.getRaceId().toString())).thenReturn(
                Optional.ofNullable(raceEntity));

        assertEquals(Optional.of(raceEntity), raceService.findRaceEntityByRace(race));
    }

    @Test
    public void buildRaceEntityFromPost_givenValidIdAndRace_Success() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        Race raceToBuild = new Race();
        raceToBuild.setHasBeenDriven(true);
        raceToBuild.setCircuit(Circuit.BELGIUM);
        raceToBuild.setRacePositionIndex(3);
        raceToBuild.setRaceDate(LocalDate.now());

        RaceEntity expected = RaceEntity.builder()
                .league(leagueEntity)
                .hasBeenDriven(raceToBuild.getHasBeenDriven())
                .circuit(raceToBuild.getCircuit())
                .raceDate(raceToBuild.getRaceDate())
                .racePositionIndex(raceToBuild.getRacePositionIndex())
                .build();

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);

        assertEquals(expected,
                raceService.buildRaceEntityFromPost(leagueEntity.getLeagueId(), raceToBuild));
    }

    @Test
    public void buildRaceEntityFromPost_givenValidIdAndRaceWithoutIndex_Success() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        Race raceToBuild = new Race();
        raceToBuild.setHasBeenDriven(true);
        raceToBuild.setCircuit(Circuit.BELGIUM);
        raceToBuild.setRaceDate(LocalDate.now());

        RaceEntity expected = RaceEntity.builder()
                .league(leagueEntity)
                .hasBeenDriven(raceToBuild.getHasBeenDriven())
                .circuit(raceToBuild.getCircuit())
                .raceDate(raceToBuild.getRaceDate())
                .racePositionIndex(2)
                .build();

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);
        when(raceRepository.findAllByLeague(leagueEntity))
                .thenReturn(List.of(RaceEntity.builder().build(), RaceEntity.builder().build()));

        assertEquals(expected,
                raceService.buildRaceEntityFromPost(leagueEntity.getLeagueId(), raceToBuild));
    }

    @Test
    public void buildRaceEntityFromPost_withIndexCollision_SwappingSuccessful() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        Race raceToBuild = new Race();
        raceToBuild.setHasBeenDriven(true);
        raceToBuild.setCircuit(Circuit.BELGIUM);
        raceToBuild.setRaceDate(LocalDate.now());
        raceToBuild.setRacePositionIndex(1);

        RaceEntity race1 = RaceEntity.builder()
                .league(leagueEntity)
                .hasBeenDriven(raceToBuild.getHasBeenDriven())
                .circuit(raceToBuild.getCircuit())
                .raceDate(raceToBuild.getRaceDate())
                .racePositionIndex(0)
                .build();

        RaceEntity race2 = RaceEntity.builder()
                .league(leagueEntity)
                .hasBeenDriven(raceToBuild.getHasBeenDriven())
                .circuit(raceToBuild.getCircuit())
                .raceDate(raceToBuild.getRaceDate())
                .racePositionIndex(1)
                .build();

        RaceEntity expected = RaceEntity.builder()
                .league(leagueEntity)
                .hasBeenDriven(raceToBuild.getHasBeenDriven())
                .circuit(raceToBuild.getCircuit())
                .raceDate(raceToBuild.getRaceDate())
                .racePositionIndex(1)
                .build();

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);
        when(raceRepository.findAllByLeague(leagueEntity))
                .thenReturn(List.of(race1, race2));
        when(raceRepository.getByLeagueAndRacePositionIndex(leagueEntity, 1)).thenReturn(race2);

        assertEquals(expected,
                raceService.buildRaceEntityFromPost(leagueEntity.getLeagueId(), raceToBuild));

        when(raceRepository.findAllByLeague(leagueEntity))
                .thenReturn(List.of(race1, race2, expected));

        assertEquals(List.of(race1, race2, expected),
                raceService.getAllRacesByLeague(leagueEntity));
    }

    @Test
    public void buildRaceEntityFromPost_givenValidIdAndInvalidRace_Success() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        Race invalidRaceToBuild = new Race();
        invalidRaceToBuild.setCircuit(Circuit.BELGIUM);
        invalidRaceToBuild.setRaceDate(LocalDate.now());

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(leagueEntity);

        assertNull(
                raceService
                        .buildRaceEntityFromPost(leagueEntity.getLeagueId(), invalidRaceToBuild));
    }

    @Test
    public void buildRaceEntityFromPost_givenInValidIdAndRace_Null() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        Race raceToBuild = new Race();
        raceToBuild.setHasBeenDriven(true);
        raceToBuild.setCircuit(Circuit.BELGIUM);
        raceToBuild.setRacePositionIndex(3);
        raceToBuild.setRaceDate(LocalDate.now());

        when(leagueService.getLeagueById(leagueEntity.getLeagueId())).thenReturn(null);

        assertNull(raceService.buildRaceEntityFromPost(leagueEntity.getLeagueId(), raceToBuild));
    }

    @Test
    public void isInvalid_givenValidRace_False() {
        Race validRace = new Race();
        validRace.setCircuit(Circuit.BELGIUM);
        validRace.setHasBeenDriven(false);

        assertFalse(raceService.isInvalid(validRace));
    }

    @Test
    public void isInvalid_givenInvalidRace_True() {
        Race invalidRace = new Race();
        invalidRace.setCircuit(Circuit.BELGIUM);

        assertTrue(raceService.isInvalid(invalidRace));
    }

    @Test
    public void updateRace_ValidRaceWithUpdates_Success() {
        Race raceWithUpdates = new Race();
        raceWithUpdates.setRaceId(UUID.randomUUID());
        raceWithUpdates.setRaceDate(LocalDate.now());
        raceWithUpdates.setCircuit(Circuit.BELGIUM);
        raceWithUpdates.setHasBeenDriven(false);

        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntityToUpdate = RaceEntity.builder()
                .raceId(raceWithUpdates.getRaceId().toString())
                .circuit(Circuit.GERMANY)
                .league(leagueEntity)
                .hasBeenDriven(true)
                .raceDate(LocalDate.EPOCH)
                .build();

        RaceEntity expectedRaceEntity = RaceEntity.builder()
                .raceId(raceWithUpdates.getRaceId().toString())
                .circuit(raceWithUpdates.getCircuit())
                .league(leagueEntity)
                .hasBeenDriven(raceWithUpdates.getHasBeenDriven())
                .raceDate(raceWithUpdates.getRaceDate())
                .racePositionIndex(0)
                .build();

        assertEquals(expectedRaceEntity,
                raceService.updateRace(raceEntityToUpdate, raceWithUpdates));
    }

    @Test
    public void updateRace_InvalidRaceWithUpdates_Null() {
        Race raceWithUpdates = new Race();
        raceWithUpdates.setRaceId(UUID.randomUUID());
        raceWithUpdates.setRaceDate(LocalDate.now());
        raceWithUpdates.setHasBeenDriven(false);

        RaceEntity raceEntityToUpdate = RaceEntity.builder()
                .raceId(raceWithUpdates.getRaceId().toString())
                .circuit(Circuit.GERMANY)
                .hasBeenDriven(true)
                .raceDate(LocalDate.EPOCH)
                .build();

        assertNull(raceService.updateRace(raceEntityToUpdate, raceWithUpdates));
    }

    @Test
    public void saveRace_GivenRace_Success() {
        RaceEntity raceEntityToSave = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .circuit(Circuit.GERMANY)
                .hasBeenDriven(true)
                .raceDate(LocalDate.EPOCH)
                .build();

        when(raceRepository.save(raceEntityToSave)).thenReturn(raceEntityToSave);


        assertEquals(raceEntityToSave, raceService.save(raceEntityToSave));
    }
}