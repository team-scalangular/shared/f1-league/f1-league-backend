package com.gitlab.f1league.f1LeagueBackend;

import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class F1LeagueBackendApplicationIntegrationTest {
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    }

    @Test
    public void defaultCreationFlow() {
        League leagueToCreate = new League();
        leagueToCreate.setName("MyLeague");

        HttpEntity<League> leagueHttpEntity = new HttpEntity<>(leagueToCreate, headers);

        UriComponentsBuilder leagueBuilder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("league"));

        ResponseEntity<League> leagueResponse =
                restTemplate
                        .exchange(leagueBuilder.toUriString(), HttpMethod.POST, leagueHttpEntity,
                                League.class);

        leagueToCreate = leagueResponse.getBody();

        headers.set("leagueEditorToken",
                leagueResponse.getHeaders().get("X-leagueEditorToken").get(0));

        League finalLeagueToCreate = leagueToCreate;
        assertAll(
                () -> assertEquals(HttpStatus.OK, leagueResponse.getStatusCode()),
                () -> assertNotNull(finalLeagueToCreate)
        );

        Team team1ToCreate = new Team();
        team1ToCreate.setLogoUrl("www.google.com");
        team1ToCreate.setName("Team Deutsche Bahn");

        HttpEntity<Team> entity = new HttpEntity<>(team1ToCreate, headers);

        UriComponentsBuilder teamBuilder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueToCreate.getLeagueId());

        ResponseEntity<Team> teamResponse =
                restTemplate
                        .exchange(teamBuilder.toUriString(), HttpMethod.POST, entity, Team.class);

        team1ToCreate = teamResponse.getBody();

        Team finalTeam = team1ToCreate;
        assertAll(
                () -> assertEquals(HttpStatus.OK, teamResponse.getStatusCode()),
                () -> assertNotNull(finalTeam)
        );

        Team team2ToCreate = new Team();
        team2ToCreate.setLogoUrl("www.facebook.com");
        team2ToCreate.setName("Team Was Anderes");

        HttpEntity<Team> entityForTeam2 = new HttpEntity<>(team2ToCreate, headers);

        UriComponentsBuilder team1Builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueToCreate.getLeagueId());

        ResponseEntity<Team> team2Response =
                restTemplate
                        .exchange(team1Builder.toUriString(), HttpMethod.POST, entityForTeam2,
                                Team.class);

        team2ToCreate = team2Response.getBody();

        Team finalTeam2 = team2ToCreate;
        assertAll(
                () -> assertEquals(HttpStatus.OK, team2Response.getStatusCode()),
                () -> assertNotNull(finalTeam2)
        );

        Driver driver1 = new Driver();
        driver1.setName("Lindner");
        driver1.setSurename("Moritz");

        Driver driver2 = new Driver();
        driver2.setName("Bieleke");
        driver2.setSurename("Max");

        Driver driver3 = new Driver();
        driver3.setName("Waldau");
        driver3.setSurename("Tom");

        Driver driver4 = new Driver();
        driver4.setName("Heime");
        driver4.setSurename("Mark Alexander");

        List<Driver> driversToPostForTeam1 = List.of(driver1, driver2);
        League finalLeagueToCreate1 = leagueToCreate;
        Team finalTeam1ToCreate = team1ToCreate;

        driversToPostForTeam1 = driversToPostForTeam1.stream().map(driver -> {
            HttpEntity<Driver> driverHttpEntity = new HttpEntity<>(driver, headers);

            UriComponentsBuilder driverBuilder =
                    UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                            .queryParam("leagueId", finalLeagueToCreate1.getLeagueId())
                            .queryParam("teamId", finalTeam1ToCreate.getTeamId());

            ResponseEntity<Driver> driverResponse =
                    restTemplate
                            .exchange(driverBuilder.toUriString(), HttpMethod.POST,
                                    driverHttpEntity,
                                    Driver.class);

            driver = driverResponse.getBody();

            Driver finalDriver = driver;
            assertAll(
                    () -> assertEquals(HttpStatus.OK, driverResponse.getStatusCode()),
                    () -> assertNotNull(finalDriver)
            );
            return finalDriver;
        }).collect(Collectors.toList());

        driver1 = driversToPostForTeam1.get(0);
        driver2 = driversToPostForTeam1.get(1);

        List<Driver> driversToPostForTeam2 = List.of(driver3, driver4);

        Team finalTeam2ToCreate = team2ToCreate;
        driversToPostForTeam2 = driversToPostForTeam2.stream().map(driver -> {
            HttpEntity<Driver> driverHttpEntity = new HttpEntity<>(driver, headers);

            UriComponentsBuilder driverBuilder =
                    UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                            .queryParam("leagueId", finalLeagueToCreate1.getLeagueId())
                            .queryParam("teamId", finalTeam2ToCreate.getTeamId());

            ResponseEntity<Driver> driverResponse =
                    restTemplate
                            .exchange(driverBuilder.toUriString(), HttpMethod.POST,
                                    driverHttpEntity,
                                    Driver.class);

            driver = driverResponse.getBody();

            Driver finalDriver = driver;
            assertAll(
                    () -> assertEquals(HttpStatus.OK, driverResponse.getStatusCode()),
                    () -> assertNotNull(finalDriver)
            );
            return driver;
        }).collect(Collectors.toList());

        driver3 = driversToPostForTeam2.get(0);
        driver4 = driversToPostForTeam2.get(1);

        Race race1 = new Race();
        race1.setRacePositionIndex(0);
        race1.setHasBeenDriven(true);
        race1.setCircuit(Circuit.MONACO);
        race1.setRaceDate(LocalDate.now());

        Race race2 = new Race();
        race2.setRacePositionIndex(1);
        race2.setHasBeenDriven(true);
        race2.setCircuit(Circuit.BELGIUM);
        race2.setRaceDate(LocalDate.now());

        List<Race> races = List.of(race1, race2);

        League finalLeagueToCreate2 = leagueToCreate;
        races = races.stream()
                .map(race -> postRace(race, finalLeagueToCreate2)).collect(Collectors.toList());

        race1 = races.get(0);
        race2 = races.get(1);

        DriverRaceResult driver1RaceResult1 = new DriverRaceResult();
        driver1RaceResult1.setRaceId(race1.getRaceId());
        driver1RaceResult1.setStartposition(4);
        driver1RaceResult1.setEndpostion(1);
        driver1RaceResult1.setCarInRace(Car.FERRARI);
        driver1RaceResult1.setFastestLap(BigDecimal.valueOf(83.1304));
        driver1RaceResult1.setDriverId(driver1.getDriverId());

        DriverRaceResult driver2RaceResult1 = new DriverRaceResult();
        driver2RaceResult1.setRaceId(race1.getRaceId());
        driver2RaceResult1.setStartposition(5);
        driver2RaceResult1.setEndpostion(7);
        driver2RaceResult1.setCarInRace(Car.FERRARI);
        driver2RaceResult1.setFastestLap(BigDecimal.valueOf(83.345));
        driver2RaceResult1.setDriverId(driver2.getDriverId());

        DriverRaceResult driver3RaceResult1 = new DriverRaceResult();
        driver3RaceResult1.setRaceId(race1.getRaceId());
        driver3RaceResult1.setStartposition(2);
        driver3RaceResult1.setEndpostion(8);
        driver3RaceResult1.setCarInRace(Car.HAAS);
        driver3RaceResult1.setFastestLap(BigDecimal.valueOf(83.441));
        driver3RaceResult1.setDriverId(driver3.getDriverId());

        DriverRaceResult driver4RaceResult1 = new DriverRaceResult();
        driver4RaceResult1.setRaceId(race1.getRaceId());
        driver4RaceResult1.setStartposition(3);
        driver4RaceResult1.setEndpostion(12);
        driver4RaceResult1.setCarInRace(Car.HAAS);
        driver4RaceResult1.setFastestLap(BigDecimal.valueOf(83.551));
        driver4RaceResult1.setDriverId(driver4.getDriverId());


        DriverRaceResult driver1RaceResult2 = new DriverRaceResult();
        driver1RaceResult2.setRaceId(race1.getRaceId());
        driver1RaceResult2.setStartposition(4);
        driver1RaceResult2.setEndpostion(7);
        driver1RaceResult2.setCarInRace(Car.FERRARI);
        driver1RaceResult2.setFastestLap(BigDecimal.valueOf(71.001));
        driver1RaceResult2.setDriverId(driver1.getDriverId());

        DriverRaceResult driver2RaceResult2 = new DriverRaceResult();
        driver2RaceResult2.setRaceId(race1.getRaceId());
        driver2RaceResult2.setStartposition(5);
        driver2RaceResult2.setEndpostion(2);
        driver2RaceResult2.setCarInRace(Car.FERRARI);
        driver2RaceResult2.setFastestLap(BigDecimal.valueOf(71.011));
        driver2RaceResult2.setDriverId(driver2.getDriverId());

        DriverRaceResult driver3RaceResult2 = new DriverRaceResult();
        driver3RaceResult2.setRaceId(race1.getRaceId());
        driver3RaceResult2.setStartposition(2);
        driver3RaceResult2.setEndpostion(11);
        driver3RaceResult2.setCarInRace(Car.HAAS);
        driver3RaceResult2.setFastestLap(BigDecimal.valueOf(70.221));
        driver3RaceResult2.setDriverId(driver3.getDriverId());

        DriverRaceResult driver4RaceResult2 = new DriverRaceResult();
        driver4RaceResult2.setRaceId(race1.getRaceId());
        driver4RaceResult2.setStartposition(3);
        driver4RaceResult2.setEndpostion(6);
        driver4RaceResult2.setCarInRace(Car.HAAS);
        driver4RaceResult2.setFastestLap(BigDecimal.valueOf(70.345));
        driver4RaceResult2.setDriverId(driver4.getDriverId());

        List<DriverRaceResult> driverRaceResults1 =
                List.of(driver1RaceResult1, driver2RaceResult1, driver3RaceResult1,
                        driver4RaceResult1);

        HttpEntity<List<DriverRaceResult>> driverRaceResultsHttpEntity =
                new HttpEntity<>(driverRaceResults1, headers);

        UriComponentsBuilder builderForRaceResults1 =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("raceResults"))
                        .queryParam("leagueId", leagueToCreate.getLeagueId())
                        .queryParam("raceId", race1.getRaceId());

        ResponseEntity<List<DriverRaceResult>> responseForRaceResults1 =
                restTemplate.exchange(builderForRaceResults1.toUriString(), HttpMethod.POST,
                        driverRaceResultsHttpEntity,
                        new ParameterizedTypeReference<List<DriverRaceResult>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, responseForRaceResults1.getStatusCode()),
                () -> assertNotNull(responseForRaceResults1.getBody())
        );

        List<DriverRaceResult> driverRaceResults2 =
                List.of(driver1RaceResult2, driver2RaceResult2, driver3RaceResult2,
                        driver4RaceResult2);

        HttpEntity<List<DriverRaceResult>> driverRaceResults2HttpEntity =
                new HttpEntity<>(driverRaceResults2, headers);

        UriComponentsBuilder builderForRaceResults2 =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("raceResults"))
                        .queryParam("leagueId", leagueToCreate.getLeagueId())
                        .queryParam("raceId", race2.getRaceId());

        ResponseEntity<List<DriverRaceResult>> responseForRaceResults2 =
                restTemplate.exchange(builderForRaceResults2.toUriString(), HttpMethod.POST,
                        driverRaceResults2HttpEntity,
                        new ParameterizedTypeReference<List<DriverRaceResult>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, responseForRaceResults2.getStatusCode()),
                () -> assertNotNull(responseForRaceResults2.getBody())
        );

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                        .queryParam("driverId", driver1.getDriverId());

        HttpEntity<Driver> driverHttpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Driver> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, driverHttpEntity,
                        Driver.class);

        ResponseEntity<Driver> finalResponse3 = response;
        assertAll(
                () -> assertEquals(HttpStatus.OK, finalResponse3.getStatusCode()),
                () -> assertEquals(32, finalResponse3.getBody().getPoints())
        );

        builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                .queryParam("driverId", driver2.getDriverId());

        driverHttpEntity = new HttpEntity<>(null, headers);

        response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, driverHttpEntity,
                        Driver.class);

        ResponseEntity<Driver> finalResponse = response;
        assertAll(
                () -> assertEquals(HttpStatus.OK, finalResponse.getStatusCode()),
                () -> assertEquals(24, finalResponse.getBody().getPoints())
        );

        builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                .queryParam("driverId", driver3.getDriverId());

        driverHttpEntity = new HttpEntity<>(null, headers);

        response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, driverHttpEntity,
                        Driver.class);

        ResponseEntity<Driver> finalResponse1 = response;
        assertAll(
                () -> assertEquals(HttpStatus.OK, finalResponse.getStatusCode()),
                () -> assertEquals(4, finalResponse1.getBody().getPoints())
        );

        builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                .queryParam("driverId", driver4.getDriverId());

        driverHttpEntity = new HttpEntity<>(null, headers);

        response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, driverHttpEntity,
                        Driver.class);

        ResponseEntity<Driver> finalResponse2 = response;
        assertAll(
                () -> assertEquals(HttpStatus.OK, finalResponse2.getStatusCode()),
                () -> assertEquals(9, finalResponse2.getBody().getPoints())
        );

        HttpEntity<List<Team>> teamHttpEntities =
                new HttpEntity<>(null, headers);

        UriComponentsBuilder builderForTeamClassification =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("teamClassification"))
                        .queryParam("leagueId", leagueToCreate.getLeagueId());

        ResponseEntity<List<Team>> teamClassficifactionResponse =
                restTemplate.exchange(builderForTeamClassification.toUriString(), HttpMethod.GET,
                        teamHttpEntities,
                        new ParameterizedTypeReference<List<Team>>() {
                        });

        Team finalTeam1ToCreate1 = team1ToCreate;
        Team finalTeam2ToCreate1 = team2ToCreate;
        assertAll(
                () -> assertEquals(HttpStatus.OK, teamClassficifactionResponse.getStatusCode()),
                () -> assertEquals(
                        List.of(finalTeam1ToCreate1.getTeamId(), finalTeam2ToCreate1.getTeamId()),
                        teamClassficifactionResponse.getBody().stream()
                                .map(Team::getTeamId).collect(Collectors.toList()))
        );

        HttpEntity<List<Team>> driverHttpEntities =
                new HttpEntity<>(null, headers);

        UriComponentsBuilder builderForDriverClassification =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("driverClassification"))
                        .queryParam("leagueId", leagueToCreate.getLeagueId());

        ResponseEntity<List<Driver>> driverClassficifactionResponse =
                restTemplate.exchange(builderForDriverClassification.toUriString(), HttpMethod.GET,
                        driverHttpEntities,
                        new ParameterizedTypeReference<List<Driver>>() {
                        });

        Driver finalDriver1 = driver1;
        Driver finalDriver2 = driver2;
        Driver finalDriver3 = driver3;
        Driver finalDriver4 = driver4;

        assertAll(
                () -> assertEquals(HttpStatus.OK, driverClassficifactionResponse.getStatusCode()),
                () -> assertEquals(List.of(finalDriver1.getDriverId(), finalDriver2.getDriverId(),
                        finalDriver4.getDriverId(), finalDriver3.getDriverId()),
                        driverClassficifactionResponse.getBody().stream()
                                .map(Driver::getDriverId).collect(Collectors.toList()))
        );
    }

    private Race postRace(Race raceToPost, League leagueToPostTo) {
        HttpEntity<Race> httpEntity = new HttpEntity<>(raceToPost, headers);

        UriComponentsBuilder driverBuilder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("race"))
                        .queryParam("leagueId", leagueToPostTo.getLeagueId());

        ResponseEntity<Race> raceResponseEntity =
                restTemplate
                        .exchange(driverBuilder.toUriString(), HttpMethod.POST,
                                httpEntity,
                                Race.class);

        raceToPost = raceResponseEntity.getBody();

        Race finalRace = raceToPost;
        assertAll(
                () -> assertEquals(HttpStatus.OK, raceResponseEntity.getStatusCode()),
                () -> assertNotNull(finalRace)
        );

        return raceToPost;
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/" + uri;
    }
}