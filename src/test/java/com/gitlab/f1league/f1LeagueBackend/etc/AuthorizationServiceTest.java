package com.gitlab.f1league.f1LeagueBackend.etc;

import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
class AuthorizationServiceTest {
    @MockBean
    LeagueService leagueService;

    AuthorizationService authorizationService;

    @BeforeEach
    public void setUp() {
        authorizationService = new AuthorizationService(leagueService);
    }

    @Test
    public void isValidToken_InvalidTokenAndKnownLeague_Success() {
        String savedToken = "abcdefg";
        String invalidToken = "aaaaaaa";

        LeagueEntity leagueEntityToGet = LeagueEntity.builder()
                .leagueEditorToken(savedToken)
                .leagueId(UUID.randomUUID().toString())
                .build();

        when(leagueService.getLeagueById(leagueEntityToGet.getLeagueId()))
                .thenReturn(leagueEntityToGet);

        assertEquals(AuthorizationService.State.UNAUTHORIZED,
                authorizationService.isValidToken(invalidToken, leagueEntityToGet.getLeagueId()));
    }

    @Test
    public void isValidToken_ValidTokenAndKnownLeague_Success() {
        String savedToken = "abcdefg";

        LeagueEntity leagueEntityToGet = LeagueEntity.builder()
                .leagueEditorToken(savedToken)
                .leagueId(UUID.randomUUID().toString())
                .build();

        when(leagueService.getLeagueById(leagueEntityToGet.getLeagueId()))
                .thenReturn(leagueEntityToGet);

        assertEquals(AuthorizationService.State.AUTHORIZED,
                authorizationService.isValidToken(savedToken, leagueEntityToGet.getLeagueId()));
    }
}