package com.gitlab.f1league.f1LeagueBackend.drivers.api;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Driver;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DriverApiImplementationTest {
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @LocalServerPort
    private int port;
    @MockBean
    private DriverService driverService;

    @MockBean
    private AuthorizationService authorizationService;

    @BeforeEach
    public void setUp() {
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("leagueEditorToken", "UhNwklP");
        when(authorizationService.isValidToken(any(), (LeagueEntity) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
        when(authorizationService.isValidToken(any(), (String) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
    }

    @Test
    public void getDriverById_correctDriverId_200AndDriver() {
        String driverId = UUID.randomUUID().toString();
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();

        DriverEntity expectedDriver = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamA)
                .driverId(driverId)
                .build();

        Driver expectedResponse = new Driver();
        expectedResponse.setName("Bieleke");
        expectedResponse.setSurename("Max");
        expectedResponse.setPoints(5);
        expectedResponse.setDriverId(UUID.fromString(driverId));
        expectedResponse.setPosition(0);
        expectedResponse.setTeamId(teamA.getTeamId());

        when(driverService.getDriverByDriverId(driverId)).thenReturn(expectedDriver);
        when(driverService.calculateADriversTotalPoints(any())).thenReturn(5);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                        .queryParam("driverId", driverId);

        HttpEntity<Driver> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Driver> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        Driver.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(expectedResponse, response.getBody())
        );
    }

    @Test
    public void getDriverById_unknownDriverId_204andNull() {
        when(driverService.getDriverByDriverId(any())).thenReturn(null);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                        .queryParam("driverId", UUID.randomUUID());

        HttpEntity<Driver> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Driver> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        Driver.class);

        assertAll(
                () -> assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void postDriver_ValidDriver_200AndCorrectDriver() {
        Driver driverToCreate = new Driver();
        driverToCreate.setName("Moritz");
        driverToCreate.setSurename("Lindner");

        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("Deutsche Bahn")
                .build();

        DriverEntity driverEntity = DriverEntity.builder()
                .team(teamA)
                .name(driverToCreate.getName())
                .surename(driverToCreate.getSurename())
                .driverId(UUID.randomUUID().toString())
                .build();

        Driver expectedDriver = new Driver();
        expectedDriver.setName("Moritz");
        expectedDriver.setSurename("Lindner");
        expectedDriver.setPoints(0);
        expectedDriver.setDriverId(UUID.fromString(driverEntity.getDriverId()));
        expectedDriver.setPosition(0);
        expectedDriver.setTeamId(teamA.getTeamId());

        when(driverService.buildDriverEntityFromPost(leagueA.getLeagueId(), teamA.getTeamId(),
                driverToCreate)).thenReturn(driverEntity);
        when(driverService.saveDriver(driverEntity)).thenReturn(driverEntity);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                        .queryParam("leagueId", leagueA.getLeagueId())
                        .queryParam("teamId", teamA.getTeamId());

        HttpEntity<Driver> entity = new HttpEntity<>(driverToCreate, headers);

        ResponseEntity<Driver> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity,
                        Driver.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(expectedDriver, response.getBody())
        );
    }

    @Test
    public void patchDriver_ValidDriverNoTeam_200AndCorrectDriver() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("Deutsche Bahn")
                .build();

        DriverEntity savedDriver = DriverEntity.builder()
                .team(teamA)
                .name("Max")
                .surename("Bieleke")
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverEntity updatedSavedDriver = DriverEntity.builder()
                .team(teamA)
                .name("Moritz")
                .surename("Bieleke")
                .driverId(savedDriver.getDriverId())
                .build();

        Driver driverWithUpdates = new Driver();
        driverWithUpdates.setName("Moritz");
        driverWithUpdates.setSurename("Bieleke");
        driverWithUpdates.setDriverId(UUID.fromString(savedDriver.getDriverId()));

        Driver expectedDriver = new Driver();
        expectedDriver.setName("Moritz");
        expectedDriver.setSurename("Bieleke");
        expectedDriver.setPoints(0);
        expectedDriver.setDriverId(UUID.fromString(savedDriver.getDriverId()));
        expectedDriver.setPosition(0);
        expectedDriver.setTeamId(teamA.getTeamId());

        when(driverService.updateDriver(savedDriver, driverWithUpdates, null))
                .thenReturn(updatedSavedDriver);
        when(driverService.saveDriver(updatedSavedDriver)).thenReturn(updatedSavedDriver);
        when(driverService.calculateADriversTotalPoints(any())).thenReturn(0);
        when(driverService.getDriverByDriverId(driverWithUpdates.getDriverId().toString()))
                .thenReturn(savedDriver);
        when(driverService.saveDriver(updatedSavedDriver)).thenReturn(updatedSavedDriver);

        HttpEntity<Driver> entity = new HttpEntity<>(driverWithUpdates, headers);
        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);

        ResponseEntity<Driver> response = restTemplate
                .exchange(createURLWithPort("driver"), HttpMethod.PATCH, entity, Driver.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(expectedDriver, response.getBody())
        );
    }

    @Test
    public void patchDriver_UnknownDriver_200AndCorrectDriver() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("Deutsche Bahn")
                .build();

        DriverEntity savedDriver = DriverEntity.builder()
                .team(teamA)
                .name("Max")
                .surename("Bieleke")
                .driverId(UUID.randomUUID().toString())
                .build();

        Driver driverWithUpdates = new Driver();
        driverWithUpdates.setName("Moritz");
        driverWithUpdates.setSurename("Bieleke");
        driverWithUpdates.setDriverId(UUID.fromString(savedDriver.getDriverId()));

        when(driverService.getDriverByDriverId(driverWithUpdates.getDriverId().toString()))
                .thenReturn(null);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                        .queryParam("leagueId", UUID.randomUUID());

        HttpEntity<Driver> entity = new HttpEntity<>(driverWithUpdates, headers);

        ResponseEntity<Driver> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.PATCH, entity,
                        Driver.class);

        assertAll(
                () -> assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void deleteDriver_UnknownDriver_200() {
        Driver driverToDelete = new Driver();
        driverToDelete.setName("Moritz");
        driverToDelete.setSurename("Bieleke");
        driverToDelete.setDriverId(UUID.randomUUID());

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("driver"))
                        .queryParam("leagueId", UUID.randomUUID())
                        .queryParam("driverId", driverToDelete.getDriverId());

        headers.add("leagueEditorToken", "");
        HttpEntity<Driver> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Driver> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity,
                        Driver.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/" + uri;
    }
}