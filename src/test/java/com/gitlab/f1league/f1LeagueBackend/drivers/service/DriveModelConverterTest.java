package com.gitlab.f1league.f1LeagueBackend.drivers.service;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Driver;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class DriveModelConverterTest {

    DriverModelConverter driverModelConverter;

    @MockBean
    DriverService driverService;
    private TeamEntity teamA;
    private LeagueEntity leagueA;

    @BeforeEach
    public void setUp() {
        driverModelConverter = new DriverModelConverter(driverService);

        leagueA = LeagueEntity
                .builder()
                .build();

        teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();
    }

    @Test
    public void convertTToE_KnownTeam_CorrectlyConverted() {
        Driver driverToConvert = new Driver();
        driverToConvert.setDriverId(UUID.randomUUID());
        driverToConvert.setName("Bieleke");
        driverToConvert.setSurename("Max");
        driverToConvert.setPoints(5);

        DriverEntity expectedDriver = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamA)
                .driverId(driverToConvert.getDriverId().toString())
                .build();

        when(driverService.findDriverEntityByDriver(driverToConvert))
                .thenReturn(Optional.ofNullable(expectedDriver));

        DriverEntity convertedTeam =
                driverModelConverter.transferModelToInternalModel(driverToConvert);

        assertEquals(expectedDriver, convertedTeam);
    }

    @Test
    public void convertTToE_UnknownTeam_NullResult() {
        Driver driverToConvert = new Driver();
        driverToConvert.setDriverId(UUID.randomUUID());
        driverToConvert.setName("Bieleke");
        driverToConvert.setSurename("Max");
        driverToConvert.setPoints(5);

        when(driverService.findDriverEntityByDriver(driverToConvert))
                .thenReturn(Optional.empty());

        DriverEntity convertedTeam =
                driverModelConverter.transferModelToInternalModel(driverToConvert);

        assertNull(convertedTeam);
    }

    @Test
    public void convertERoR_Given() {
        DriverEntity driverToConvert = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .driverId(UUID.randomUUID().toString())
                .team(teamA)
                .build();

        Driver expectedDriver = new Driver();
        expectedDriver.setDriverId(UUID.fromString(driverToConvert.getDriverId()));
        expectedDriver.setName("Bieleke");
        expectedDriver.setSurename("Max");
        expectedDriver.setPoints(5);
        expectedDriver.setPosition(1);
        expectedDriver.setTeamId(teamA.getTeamId());

        when(driverService.calculateADriversTotalPoints(driverToConvert)).thenReturn(5);
        when(driverService.getDriverPosition(driverToConvert)).thenReturn(1);

        Driver convertedTeam = driverModelConverter.internalModelToTransferModel(driverToConvert);

        assertEquals(expectedDriver, convertedTeam);
    }

}