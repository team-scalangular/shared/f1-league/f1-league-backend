package com.gitlab.f1league.f1LeagueBackend.drivers.repository;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.repository.LeagueRepository;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.repository.TeamRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class DriverRepositoryTest {
    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private LeagueRepository leagueRepository;

    private TeamEntity teamA, teamB;

    @BeforeEach
    public void setUp() {
        driverRepository.deleteAll();
        teamRepository.deleteAll();

        LeagueEntity leagueA = LeagueEntity
                .builder()
                .build();
        LeagueEntity leagueB = LeagueEntity
                .builder()
                .build();

        leagueRepository.save(leagueA);
        leagueRepository.save(leagueB);

        teamA = TeamEntity
                .builder()
                .league(leagueA)
                .build();
        teamB = TeamEntity
                .builder()
                .league(leagueB)
                .build();

        teamA = teamRepository.save(teamA);
        teamB = teamRepository.save(teamB);
    }

    @Test
    public void getDriverByTeamId_GivenSomeEntries_ExpectCorrectDrivers() {
        List<DriverEntity> driverEntities = Arrays.asList(DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamA)
                .build(), DriverEntity.builder()
                .name("Lindner")
                .surename("Moritz")
                .team(teamA)
                .build(), DriverEntity.builder()
                .name("Wurst")
                .surename("Hans")
                .team(teamB)
                .build());

        driverRepository.saveAll(driverEntities);

        assertAll(
                () -> assertEquals(2, driverRepository.findAllByTeam(teamA).size()),
                () -> assertEquals(1, driverRepository.findAllByTeam(teamB).size()),
                () -> assertEquals(driverEntities.subList(0, 2),
                        driverRepository.findAllByTeam(teamA)),
                () -> assertEquals(driverEntities.subList(2, 3),
                        driverRepository.findAllByTeam(teamB))
        );
    }
}