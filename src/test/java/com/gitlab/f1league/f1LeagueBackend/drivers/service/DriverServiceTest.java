package com.gitlab.f1league.f1LeagueBackend.drivers.service;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.repository.DriverRepository;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Driver;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.race.entity.RaceEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.entity.DriverRaceResultEntity;
import com.gitlab.f1league.f1LeagueBackend.raceResult.services.RaceResultService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class DriverServiceTest {
    DriverService driverService;

    @Mock
    DriverRepository driverRepository;

    @Mock
    RaceResultService raceResultService;

    @Mock
    DriverClassificationCacheService driverClassificationCacheService;

    @Mock
    TeamService teamService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        driverService = new DriverService(driverRepository);
        driverService.setTeamService(teamService);
        driverService.setRaceResultService(raceResultService);
        driverService.setDriverClassificationCacheService(driverClassificationCacheService);
    }

    @Test
    public void getDriver_unknownId_nullAsResult() {
        String uuid = UUID.randomUUID().toString();
        when(driverRepository.findById(uuid)).thenReturn(Optional.empty());

        assertNull(driverService.getDriverByDriverId(uuid));
    }

    @Test
    public void getDriver_knownId_nullAsResult() {
        DriverEntity driverEntity = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(TeamEntity.builder().build())
                .driverId(UUID.randomUUID().toString())
                .build();

        when(driverRepository.findById(driverEntity.getDriverId())).thenReturn(
                Optional.of(driverEntity));

        assertEquals(driverEntity, driverService.getDriverByDriverId(driverEntity.getDriverId()));
    }

    @Test
    public void createDriverFromPost_knownTeam_createdCorrectly() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();

        Driver driverToCreate = new Driver();
        driverToCreate.setName("Max");

        DriverEntity expectedDriverEntity = DriverEntity.builder()
                .name(driverToCreate.getName())
                .surename(null)
                .team(teamA)
                .build();

        when(teamService.getTeamEntityByTeamIdAndLeague(teamA.getTeamId(), leagueA.getLeagueId()))
                .thenReturn(teamA);

        DriverEntity createdDriverEntity = driverService
                .buildDriverEntityFromPost(leagueA.getLeagueId(), teamA.getTeamId(),
                        driverToCreate);

        assertEquals(expectedDriverEntity, createdDriverEntity);
    }

    @Test
    public void createDriverFromPost_unknownTeamOrUnknownLeague_NullReturned() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();

        Driver driverToCreate = new Driver();
        driverToCreate.setName("Max");

        when(teamService.getTeamEntityByTeamIdAndLeague(teamA.getTeamId(), leagueA.getLeagueId()))
                .thenReturn(null);

        DriverEntity createdDriverEntity = driverService
                .buildDriverEntityFromPost(leagueA.getLeagueId(), teamA.getTeamId(),
                        driverToCreate);

        assertNull(createdDriverEntity);
    }

    @Test
    public void createDriverFromPost_noNameProvided_NotSaved() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();

        Driver driverToCreate = new Driver();

        when(teamService.getTeamEntityByTeamIdAndLeague(teamA.getTeamId(), leagueA.getLeagueId()))
                .thenReturn(teamA);

        DriverEntity createdDriverEntity = driverService
                .buildDriverEntityFromPost(leagueA.getLeagueId(), teamA.getTeamId(),
                        driverToCreate);

        assertNull(createdDriverEntity);
    }

    @Test
    public void getDriversByTeam_oneDriverTeamIn_OneDriver() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();

        when(driverRepository.findAllByTeam(teamA))
                .thenReturn(List.of(DriverEntity.builder().build()));

        List<DriverEntity> driverEntities = driverService.getDriversByTeamEntity(teamA);

        assertAll(
                () -> assertEquals(1, driverEntities.size()),
                () -> assertEquals(List.of(DriverEntity.builder().build()), driverEntities)
        );
    }

    @Test
    public void updateDriver_givenValidDriver_expectSuccess() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();

        DriverEntity createdDriverEntity = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .surename("Bieleke")
                .name("Max")
                .team(teamA)
                .build();

        Driver driverWithUpdates = new Driver();
        driverWithUpdates.setName("Bieleke");
        driverWithUpdates.setSurename("Max");

        DriverEntity expectedDriverEntity = DriverEntity.builder()
                .driverId(createdDriverEntity.getDriverId())
                .surename(driverWithUpdates.getSurename())
                .name(driverWithUpdates.getName())
                .team(null)
                .build();

        DriverEntity updateDriverEntity =
                driverService.updateDriver(createdDriverEntity, driverWithUpdates, null);

        assertEquals(expectedDriverEntity, updateDriverEntity);
    }

    @Test
    public void updateDriver_givenValidDriverAndValidTeam_expectSuccess() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();

        TeamEntity teamB = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.bahn.com")
                .league(leagueA)
                .name("Team Bahn")
                .build();

        DriverEntity createdDriverEntity = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .surename("Bieleke")
                .name("Max")
                .team(teamA)
                .build();

        Driver driverWithUpdates = new Driver();
        driverWithUpdates.setName("Bieleke");
        driverWithUpdates.setSurename("Max");
        driverWithUpdates.setTeamId(teamB.getTeamId());

        DriverEntity expectedDriverEntity = DriverEntity.builder()
                .driverId(createdDriverEntity.getDriverId())
                .surename(driverWithUpdates.getSurename())
                .name(driverWithUpdates.getName())
                .team(teamB)
                .build();

        when(teamService.getTeamById(teamB.getTeamId())).thenReturn(teamB);

        DriverEntity updateDriverEntity =
                driverService
                        .updateDriver(createdDriverEntity, driverWithUpdates, teamB.getTeamId());

        assertEquals(expectedDriverEntity, updateDriverEntity);
    }

    @Test
    public void updateDriver_givenInvalidDriver_expectNull() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();

        DriverEntity createdDriverEntity = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .surename("Bieleke")
                .name("Max")
                .team(teamA)
                .build();

        Driver driverWithUpdates = new Driver();
        driverWithUpdates.setName("");
        driverWithUpdates.setSurename("Max");

        DriverEntity updateDriverEntity =
                driverService.updateDriver(createdDriverEntity, driverWithUpdates, null);

        assertNull(updateDriverEntity);
    }

    @Test
    public void getADriversTeamPartner_givenDriverAndTeam_CorrectPartner() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToTest = TeamEntity.builder()
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .build();

        DriverEntity driverToGetPartner = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(teamToTest)
                .build();
        DriverEntity partner = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(teamToTest)
                .build();

        when(driverRepository.findAllByTeam(teamToTest))
                .thenReturn(List.of(driverToGetPartner, partner));

        assertEquals(List.of(partner), driverService.getADriversTeamPartner(driverToGetPartner));
    }

    @Test
    public void calculateADriversTotalPoints_GivenRaceResults_CorrectCalculated() {
        RaceEntity raceEntity1 = RaceEntity.builder()
                .racePositionIndex(0)
                .raceId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity2 = RaceEntity.builder()
                .racePositionIndex(1)
                .raceId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity3 = RaceEntity.builder()
                .racePositionIndex(3)
                .raceId(UUID.randomUUID().toString())
                .build();

        DriverEntity driverToGetTotalPointsFrom = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(null)
                .build();

        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(null)
                .build();

        DriverEntity driverEntity3 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Tom")
                .surename("Waldautttt")
                .team(null)
                .build();

        DriverRaceResultEntity result1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.560))
                .endpostion(2)
                .driver(driverToGetTotalPointsFrom)
                .carInRace(Car.FERRARI)
                .build();

        DriverRaceResultEntity result2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(73.112))
                .endpostion(7)
                .driver(driverEntity2)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result3 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.568))
                .endpostion(3)
                .driver(driverEntity3)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result4 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(100.010))
                .endpostion(8)
                .driver(driverToGetTotalPointsFrom)
                .carInRace(Car.FERRARI)
                .build();

        DriverRaceResultEntity result5 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(99.880))
                .endpostion(7)
                .driver(driverEntity2)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result6 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(99.978))
                .endpostion(2)
                .driver(driverEntity3)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result7 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity3)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(60.101))
                .endpostion(12)
                .driver(driverToGetTotalPointsFrom)
                .carInRace(Car.FERRARI)
                .build();

        DriverRaceResultEntity result8 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity3)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(60.222))
                .endpostion(1)
                .driver(driverEntity2)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        DriverRaceResultEntity result9 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity3)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(60.111))
                .endpostion(2)
                .driver(driverEntity3)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        when(raceResultService.getAllResultsForGivenDriverEntity(driverToGetTotalPointsFrom))
                .thenReturn(List.of(result1, result4, result7));
        when(raceResultService.getAllResultsForGivenDriverEntity(driverEntity2))
                .thenReturn(List.of(result2, result5, result8));
        when(raceResultService.getAllResultsForGivenDriverEntity(driverEntity3))
                .thenReturn(List.of(result3, result6, result9));

        when(raceResultService.calculatePointsForRaceResult(any())).thenCallRealMethod();
        when(raceResultService.getPointsForPosition(any())).thenCallRealMethod();
        when(raceResultService.raceResultEntityHasFastestLap(any())).thenCallRealMethod();

        when(raceResultService.getRaceResultsByRaceId(raceEntity1.getRaceId()))
                .thenReturn(List.of(result1, result2, result3));
        when(raceResultService.getRaceResultsByRaceId(raceEntity2.getRaceId()))
                .thenReturn(List.of(result4, result5, result6));
        when(raceResultService.getRaceResultsByRaceId(raceEntity3.getRaceId()))
                .thenReturn(List.of(result7, result8, result9));

        assertAll(
                () -> assertEquals(0,
                        driverService.calculateADriversTotalPoints(driverToGetTotalPointsFrom,
                                -1)),
                () -> assertEquals(23,
                        driverService
                                .calculateADriversTotalPoints(driverToGetTotalPointsFrom)),
                () -> assertEquals(38,
                        driverService.calculateADriversTotalPoints(driverEntity2)),
                () -> assertEquals(52,
                        driverService.calculateADriversTotalPoints(driverEntity3)),
                () -> assertEquals(33,
                        driverService.calculateADriversTotalPoints(driverEntity3, 2)),
                () -> assertEquals(19,
                        driverService
                                .calculateADriversTotalPoints(driverToGetTotalPointsFrom, 0)),
                () -> assertEquals(23,
                        driverService
                                .calculateADriversTotalPoints(driverToGetTotalPointsFrom, 1))
        );
    }

    @Test
    public void getAmountsOfPositionsOnPlace_valid_success() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueEntity)
                .name("TeamA")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamA)
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverEntity driver2 = DriverEntity.builder()
                .name("Lindner")
                .surename("Moritz")
                .team(teamA)
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity1 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .racePositionIndex(0)
                .build();

        RaceEntity raceEntity2 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .racePositionIndex(1)
                .build();

        DriverRaceResultEntity driver1Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(60))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver1)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResultEntity driver2Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(3)
                .fastestLap(BigDecimal.valueOf(61))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver2)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResultEntity driver1Race2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(60))
                .endpostion(2)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver1)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResultEntity driver2Race2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(3)
                .fastestLap(BigDecimal.valueOf(61))
                .endpostion(3)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver2)
                .carInRace(Car.HAAS)
                .build();

        when(driverRepository.findAll()).thenReturn(List.of(driver1, driver2));
        when(raceResultService.getAllResultsForGivenDriverEntity(driver1))
                .thenReturn(List.of(driver1Race1, driver1Race2));
        when(raceResultService.getAllResultsForGivenDriverEntity(driver2))
                .thenReturn(List.of(driver2Race1, driver2Race2));
        when(raceResultService.calculatePointsForRaceResult(driver1Race1)).thenReturn(20);
        when(raceResultService.calculatePointsForRaceResult(driver2Race1)).thenReturn(25);
        when(raceResultService.calculatePointsForRaceResult(driver1Race2)).thenReturn(25);
        when(raceResultService.calculatePointsForRaceResult(driver2Race2)).thenReturn(20);


        assertAll(
                () -> assertEquals(1, driverService.getAmountsOfPositionsOnPlace(driver1, 1)),
                () -> assertEquals(1, driverService.getAmountsOfPositionsOnPlace(driver1, 2)),
                () -> assertEquals(0, driverService.getAmountsOfPositionsOnPlace(driver1, 3)),
                () -> assertEquals(1, driverService.getAmountsOfPositionsOnPlace(driver2, 1)),
                () -> assertEquals(0, driverService.getAmountsOfPositionsOnPlace(driver2, 2)),
                () -> assertEquals(1, driverService.getAmountsOfPositionsOnPlace(driver2, 3)),
                () -> assertEquals(List.of(driver1, driver2),
                        driverService.calculateDriverClassification(leagueEntity)),
                () -> assertEquals(List.of(driver2, driver1),
                        driverService.getDriverClassification(leagueEntity, 0)),
                () -> assertEquals(List.of(driver1, driver2),
                        driverService.getDriverClassification(leagueEntity, 1))
        );
    }

    @Test
    public void sortBasedOnPointsAndPosition_complexLeague_Success() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueEntity)
                .name("TeamA")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamA)
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverEntity driver2 = DriverEntity.builder()
                .name("Lindner")
                .surename("Moritz")
                .team(teamA)
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity1 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .racePositionIndex(0)
                .build();

        RaceEntity raceEntity2 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .racePositionIndex(1)
                .build();

        DriverRaceResultEntity driver1Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(60))
                .endpostion(11)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver1)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResultEntity driver2Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(3)
                .fastestLap(BigDecimal.valueOf(61))
                .endpostion(11)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver2)
                .carInRace(Car.HAAS)
                .build();

        when(raceResultService.getAllResultsForGivenDriverEntity(driver1))
                .thenReturn(List.of(driver1Race1));
        when(raceResultService.getAllResultsForGivenDriverEntity(driver2))
                .thenReturn(List.of(driver2Race1));

        assertEquals(0, driverService.sortBasedOnPointsAndPosition(driver1, driver2, 0, 0));

        driver1Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(60))
                .endpostion(10)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver1)
                .carInRace(Car.HAAS)
                .build();

        driver2Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(3)
                .fastestLap(BigDecimal.valueOf(61))
                .endpostion(11)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver2)
                .carInRace(Car.HAAS)
                .build();

        when(raceResultService.getAllResultsForGivenDriverEntity(driver1))
                .thenReturn(List.of(driver1Race1));
        when(raceResultService.getAllResultsForGivenDriverEntity(driver2))
                .thenReturn(List.of(driver2Race1));

        assertEquals(-1, driverService.sortBasedOnPointsAndPosition(driver1, driver2, 0, 0));
    }

    @Test
    public void driverClassification_DecisionOnThirdPlace_success() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueEntity)
                .name("TeamA")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamA)
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverEntity driver2 = DriverEntity.builder()
                .name("Lindner")
                .surename("Moritz")
                .team(teamA)
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity1 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .racePositionIndex(0)
                .build();

        RaceEntity raceEntity2 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .racePositionIndex(1)
                .build();

        DriverRaceResultEntity driver1Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(60))
                .endpostion(4)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver1)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResultEntity driver2Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(3)
                .fastestLap(BigDecimal.valueOf(61))
                .endpostion(5)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver2)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResultEntity driver1Race2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(60))
                .endpostion(3)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver1)
                .carInRace(Car.HAAS)
                .build();

        DriverRaceResultEntity driver2Race2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(3)
                .fastestLap(BigDecimal.valueOf(61))
                .endpostion(5)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver2)
                .carInRace(Car.HAAS)
                .build();

        when(driverRepository.findAll()).thenReturn(List.of(driver2, driver1));
        when(raceResultService.getAllResultsForGivenDriverEntity(driver1))
                .thenReturn(List.of(driver1Race1, driver1Race2));
        when(raceResultService.getAllResultsForGivenDriverEntity(driver2))
                .thenReturn(List.of(driver2Race1, driver2Race2));
        when(raceResultService.calculatePointsForRaceResult(driver1Race1)).thenReturn(20);
        when(raceResultService.calculatePointsForRaceResult(driver2Race1)).thenReturn(25);
        when(raceResultService.calculatePointsForRaceResult(driver1Race2)).thenReturn(25);
        when(raceResultService.calculatePointsForRaceResult(driver2Race2)).thenReturn(20);

        assertAll(
                () -> assertEquals(0, driverService.getAmountsOfPositionsOnPlace(driver1, 1)),
                () -> assertEquals(0, driverService.getAmountsOfPositionsOnPlace(driver1, 2)),
                () -> assertEquals(1, driverService.getAmountsOfPositionsOnPlace(driver1, 3)),
                () -> assertEquals(0, driverService.getAmountsOfPositionsOnPlace(driver2, 1)),
                () -> assertEquals(0, driverService.getAmountsOfPositionsOnPlace(driver2, 2)),
                () -> assertEquals(0, driverService.getAmountsOfPositionsOnPlace(driver2, 3)),
                () -> assertEquals(List.of(driver1, driver2),
                        driverService.calculateDriverClassification(leagueEntity))
        );
    }

    @Test
    public void getDriverClassificationAtRaceIndex_valid_success() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueEntity)
                .name("TeamA")
                .build();

        DriverEntity driver1 = DriverEntity.builder()
                .name("Bieleke")
                .surename("Max")
                .team(teamA)
                .driverId(UUID.randomUUID().toString())
                .build();

        DriverEntity driver2 = DriverEntity.builder()
                .name("Lindner")
                .surename("Moritz")
                .team(teamA)
                .driverId(UUID.randomUUID().toString())
                .build();

        RaceEntity raceEntity1 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .racePositionIndex(0)
                .build();

        RaceEntity raceEntity2 = RaceEntity.builder()
                .raceId(UUID.randomUUID().toString())
                .racePositionIndex(1)
                .build();

        // 19 total 19, p2
        DriverRaceResultEntity driver1Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(60))
                .endpostion(2)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver1)
                .carInRace(Car.HAAS)
                .build();

        // 25 25 p1
        DriverRaceResultEntity driver2Race1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(3)
                .fastestLap(BigDecimal.valueOf(61))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver2)
                .carInRace(Car.HAAS)
                .build();

        // 26 total 45 p1
        DriverRaceResultEntity driver1Race2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(2)
                .fastestLap(BigDecimal.valueOf(60))
                .endpostion(1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver1)
                .carInRace(Car.HAAS)
                .build();

        // 1 pkt (total 26) p2
        DriverRaceResultEntity driver2Race2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(3)
                .fastestLap(BigDecimal.valueOf(61))
                .endpostion(10)
                .driverRaceResultId(UUID.randomUUID().toString())
                .driver(driver2)
                .carInRace(Car.HAAS)
                .build();

        when(driverRepository.findAll()).thenReturn(List.of(driver1, driver2));
        when(raceResultService.getAllResultsForGivenDriverEntity(driver1))
                .thenReturn(List.of(driver1Race1, driver1Race2));
        when(raceResultService.getAllResultsForGivenDriverEntity(driver2))
                .thenReturn(List.of(driver2Race1, driver2Race2));
        when(raceResultService.calculatePointsForRaceResult(driver1Race1)).thenReturn(19);
        when(raceResultService.calculatePointsForRaceResult(driver2Race1)).thenReturn(25);
        when(raceResultService.calculatePointsForRaceResult(driver1Race2)).thenReturn(26);
        when(raceResultService.calculatePointsForRaceResult(driver2Race2)).thenReturn(1);

        assertAll(
                () -> assertEquals(2, driverService.getDriverPosition(driver1, 0)),
                () -> assertEquals(1, driverService.getDriverPosition(driver1, 1)),
                () -> assertEquals(1, driverService.getDriverPosition(driver2, 0)),
                () -> assertEquals(2, driverService.getDriverPosition(driver2, 1))
        );
    }

    @Test
    public void getAllDriverEntitiesInLeague_validLeague_Success() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        LeagueEntity leagueB = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity team1 = TeamEntity.builder()
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .build();

        TeamEntity team2 = TeamEntity.builder()
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .teamId(UUID.randomUUID().toString())
                .league(leagueB)
                .build();


        DriverEntity driverEntity1 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(team1)
                .build();

        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(team1)
                .build();

        DriverEntity driverEntity3 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Tom")
                .surename("Waldautttt")
                .team(team1)
                .build();

        DriverEntity driverEntity4 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Tom")
                .surename("Waldautttt")
                .team(team2)
                .build();


        when(driverRepository.findAll())
                .thenReturn(List.of(driverEntity1, driverEntity2, driverEntity3, driverEntity4));

        assertAll(
                () -> assertEquals(List.of(driverEntity1, driverEntity2, driverEntity3),
                        driverService.getAllDriverEntitiesInLeague(leagueA)),
                () -> assertEquals(List.of(driverEntity4),
                        driverService.getAllDriverEntitiesInLeague(leagueB))
        );
    }

    @Test
    public void getDriverClassification_validDriver_Correct() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .league(leagueA)
                .build();

        RaceEntity raceEntity1 = RaceEntity.builder().build();
        RaceEntity raceEntity2 = RaceEntity.builder().build();

        DriverEntity driverEntity1 = DriverEntity.builder()
                .name("Driver 1")
                .team(teamEntity)
                .build();
        DriverEntity driverEntity2 = DriverEntity.builder()
                .name("Driver 2")
                .team(teamEntity)
                .build();

        DriverRaceResultEntity result1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.560))
                .endpostion(1)
                .driver(driverEntity1)
                .carInRace(Car.FERRARI)
                .build();

        when(raceResultService.calculatePointsForRaceResult(result1)).thenReturn(10);

        DriverRaceResultEntity result2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(73.112))
                .endpostion(2)
                .driver(driverEntity2)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        when(raceResultService.calculatePointsForRaceResult(result2)).thenReturn(5);

        DriverRaceResultEntity result3 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(72.568))
                .endpostion(2)
                .driver(driverEntity1)
                .carInRace(Car.ALPHA_ROMEO)
                .build();

        when(raceResultService.calculatePointsForRaceResult(result3)).thenReturn(10);

        DriverRaceResultEntity result4 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .startposition(5)
                .fastestLap(BigDecimal.valueOf(100.010))
                .endpostion(8)
                .driver(driverEntity2)
                .carInRace(Car.FERRARI)
                .build();

        when(raceResultService.calculatePointsForRaceResult(result3)).thenReturn(5);

        when(driverRepository.findAll()).thenReturn(List.of(driverEntity2, driverEntity1));
        when(raceResultService.getAllResultsForGivenDriverEntity(driverEntity1))
                .thenReturn(List.of(result1, result3));
        when(raceResultService.getAllResultsForGivenDriverEntity(driverEntity2))
                .thenReturn(List.of(result2, result4));

        when(driverClassificationCacheService.getDriverClassification(leagueA))
                .thenReturn(List.of(driverEntity1, driverEntity2));

        assertAll(
                () -> assertEquals(List.of(driverEntity1, driverEntity2),
                        driverService.calculateDriverClassification(leagueA)),
                () -> assertEquals(1, driverService.getDriverPosition(driverEntity1)),
                () -> assertEquals(2, driverService.getDriverPosition(driverEntity2))
        );
    }

    @RepeatedTest(10)
    public void getDriverClassification_complexCase_Correct() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .league(leagueA)
                .build();

        RaceEntity raceEntity1 = RaceEntity.builder().build();

        List<DriverEntity> driverEntities = new ArrayList<>();
        List<DriverEntity> expected = new ArrayList<>();

        IntStream.range(0, 15).forEach(value -> {
            DriverEntity driverEntity = DriverEntity.builder()
                    .name("Driver " + value)
                    .team(teamEntity)
                    .driverId(UUID.randomUUID().toString())
                    .build();

            driverEntities.add(driverEntity);
            expected.add(driverEntity);
        });

        List<DriverRaceResultEntity> driverRaceResultEntities = new ArrayList<>();

        IntStream.range(0, 15).forEach(value -> {
            DriverRaceResultEntity result = DriverRaceResultEntity.builder()
                    .raceEntity(raceEntity1)
                    .startposition(value + 1)
                    .fastestLap(BigDecimal.valueOf(100.010))
                    .endpostion(value + 1)
                    .driver(driverEntities.get(value))
                    .carInRace(Car.FERRARI)
                    .build();

            driverRaceResultEntities.add(result);
        });

        IntStream.range(0, 15).forEach(value -> {
            when(raceResultService.getAllResultsForGivenDriverEntity(driverEntities.get(value)))
                    .thenReturn(List.of(driverRaceResultEntities.get(value)));
        });

        Collections.shuffle(driverEntities);
        when(driverRepository.findAll()).thenReturn(driverEntities);

        assertEquals(expected,
                driverService.calculateDriverClassification(leagueA));
    }

    @Test
    public void getDriverClassification_complexCase2_Correct() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity1 = TeamEntity.builder()
                .league(leagueA)
                .build();

        TeamEntity teamEntity2 = TeamEntity.builder()
                .league(leagueA)
                .build();

        RaceEntity raceEntity1 = RaceEntity.builder().build();
        RaceEntity raceEntity2 = RaceEntity.builder().build();
        RaceEntity raceEntity4 = RaceEntity.builder().build();
        RaceEntity raceEntity5 = RaceEntity.builder().build();
        RaceEntity raceEntity6 = RaceEntity.builder().build();

        DriverEntity jesse = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Jesse")
                .surename("Kmoch")
                .team(teamEntity1)
                .build();

        DriverRaceResultEntity driverRaceResultEntityJesse1 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity1)
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.MERCEDES)
                .startposition(10)
                .endpostion(14)
                .fastestLap(BigDecimal.valueOf(65))
                .driver(jesse)
                .build();

        DriverRaceResultEntity driverRaceResultEntityJesse2 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity2)
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.MERCEDES)
                .startposition(12)
                .endpostion(11)
                .fastestLap(BigDecimal.valueOf(65))
                .driver(jesse)
                .build();

        DriverRaceResultEntity driverRaceResultEntityJesse4 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity4)
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.MERCEDES)
                .startposition(11)
                .endpostion(12)
                .fastestLap(BigDecimal.valueOf(65))
                .driver(jesse)
                .build();

        DriverRaceResultEntity driverRaceResultEntityJesse5 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity5)
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.MERCEDES)
                .startposition(12)
                .endpostion(13)
                .fastestLap(BigDecimal.valueOf(65))
                .driver(jesse)
                .build();

        DriverRaceResultEntity driverRaceResultEntityJesse6 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity6)
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.MERCEDES)
                .startposition(13)
                .endpostion(13)
                .fastestLap(BigDecimal.valueOf(65))
                .driver(jesse)
                .build();

        when(raceResultService.getAllResultsForGivenDriverEntity(jesse))
                .thenReturn(List.of(driverRaceResultEntityJesse1, driverRaceResultEntityJesse2,
                        driverRaceResultEntityJesse4, driverRaceResultEntityJesse5,
                        driverRaceResultEntityJesse6));

        DriverEntity max = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Gros")
                .surename("Max")
                .team(teamEntity2)
                .build();

        DriverRaceResultEntity driverRaceResultEntityMax4 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity4)
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.HAAS)
                .startposition(15)
                .endpostion(11)
                .fastestLap(BigDecimal.valueOf(114.213))
                .driver(max)
                .build();

        DriverRaceResultEntity driverRaceResultEntityMax5 = DriverRaceResultEntity.builder()
                .raceEntity(raceEntity5)
                .driverRaceResultId(UUID.randomUUID().toString())
                .carInRace(Car.HAAS)
                .startposition(11)
                .endpostion(11)
                .fastestLap(BigDecimal.valueOf(76.513))
                .driver(max)
                .build();

        when(raceResultService.getAllResultsForGivenDriverEntity(max))
                .thenReturn(List.of(driverRaceResultEntityMax4, driverRaceResultEntityMax5));

        when(driverRepository.findAll()).thenReturn(List.of(jesse, max));

        assertEquals(List.of(max, jesse),
                driverService.calculateDriverClassification(leagueA));
    }
}