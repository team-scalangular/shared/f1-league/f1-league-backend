package com.gitlab.f1league.f1LeagueBackend.teams.repository;

import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.repository.LeagueRepository;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class TeamRepositoryTest {
    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private LeagueRepository leagueRepository;

    private LeagueEntity leagueA, leagueB;

    @BeforeEach
    public void setUp() {
        teamRepository.deleteAll();
        leagueRepository.deleteAll();

        leagueA = LeagueEntity
                .builder()
                .build();
        leagueB = LeagueEntity
                .builder()
                .build();

        leagueA = leagueRepository.save(leagueA);
        leagueB = leagueRepository.save(leagueB);
    }

    @Test
    public void getAllTeams_givenEmptyRepo_Expect0Results() {
        assertAll(
                () -> assertEquals(0, teamRepository.count()),
                () -> assertEquals(0, teamRepository.findAll().size())
        );
    }

    @Test
    public void getAllTeams_given2Entries_Expect2Results() {
        List<TeamEntity> teamEntities = Arrays.asList(TeamEntity.builder()
                .name("Team 1")
                .league(leagueA)
                .logoUrl("www.google.com")
                .build(), TeamEntity.builder()
                .name("Team 2")
                .league(leagueB)
                .logoUrl("www.google.com")
                .build());

        List<TeamEntity> savedTeamEntities = teamRepository.saveAll(teamEntities);

        assertAll(
                () -> assertEquals(2, teamRepository.count()),
                () -> assertEquals(2, teamRepository.findAll().size()),
                () -> assertEquals(teamEntities.get(0),
                        teamRepository.findById(savedTeamEntities.get(0).getTeamId()).get()),
                () -> assertEquals(teamEntities.get(1),
                        teamRepository.findById(savedTeamEntities.get(1).getTeamId()).get())
        );
    }


    @Test
    public void getTeamById_given2Entries_ExpectCorrectTeam() {
        List<TeamEntity> teamEntities = Arrays.asList(TeamEntity.builder()
                .name("Team 1")
                .league(leagueA)
                .logoUrl("www.google.com")
                .build(), TeamEntity.builder()
                .name("Team 2")
                .league(leagueB)
                .logoUrl("www.google.com")
                .build());

        List<TeamEntity> savedTeamEntities = teamRepository.saveAll(teamEntities);

        assertEquals(teamEntities.get(1),
                teamRepository.findById(savedTeamEntities.get(1).getTeamId()).get());
    }

    @Test
    public void getTeamById_givenUnknownId_ExpectEmpty() {
        List<TeamEntity> teamEntities = Arrays.asList(TeamEntity.builder()
                .name("Team 1")
                .league(leagueA)
                .logoUrl("www.google.com")
                .build(), TeamEntity.builder()
                .name("Team 2")
                .league(leagueB)
                .logoUrl("www.google.com")
                .build());

        teamRepository.saveAll(teamEntities);

        String unknownId = "abc11abc111";
        Optional<TeamEntity> empty = Optional.empty();

        assertEquals(empty, teamRepository.findById(unknownId));
    }

    @Test
    public void getTeamsByLeagueId_givenDifferentLeague_ExpectCorrectTeams() {
        List<TeamEntity> teamEntities = Arrays.asList(TeamEntity.builder()
                .name("Team 1")
                .league(leagueA)
                .logoUrl("www.google.com")
                .build(), TeamEntity.builder()
                .name("Team 2")
                .league(leagueB)
                .logoUrl("www.google.com")
                .build(), TeamEntity.builder()
                .name("Team 3")
                .league(leagueB)
                .logoUrl("www.google.com")
                .build());

        teamRepository.saveAll(teamEntities);

        assertAll(
                () -> assertEquals(1, teamRepository.findAllByLeague(leagueA).size()),
                () -> assertEquals(2, teamRepository.findAllByLeague(leagueB).size()),
                () -> assertEquals(teamEntities.subList(0, 1),
                        teamRepository.findAllByLeague(leagueA)),
                () -> assertEquals(teamEntities.subList(1, 3),
                        teamRepository.findAllByLeague(leagueB))
        );
    }

    @Test
    public void deleteAllTeams_given2Entries_Expect0Results() {
        List<TeamEntity> teamEntities = Arrays.asList(TeamEntity.builder()
                .name("Team 1")
                .league(leagueA)
                .logoUrl("www.google.com")
                .build(), TeamEntity.builder()
                .name("Team 2")
                .league(leagueB)
                .logoUrl("www.google.com")
                .build());

        teamRepository.saveAll(teamEntities);

        teamRepository.deleteAll();

        assertAll(
                () -> assertEquals(0, teamRepository.count()),
                () -> assertEquals(0, teamRepository.findAll().size())
        );
    }

    @Test
    public void getTeamByLeagueAndId_givenEntries_Expect1Results() {
        TeamEntity team = TeamEntity.builder()
                .name("Team 1")
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        team = teamRepository.save(team);

        TeamEntity finalTeam = team;
        assertAll(
                () -> assertEquals(
                        finalTeam,
                        teamRepository.findByTeamIdAndLeague(finalTeam.getTeamId(), leagueA)),
                () -> assertNull(
                        teamRepository.findByTeamIdAndLeague(finalTeam.getTeamId(), leagueB))
        );
    }

    @Test
    public void updateAndOverrideTeam_givenUpdates_ExpectSuccess() {
        TeamEntity team = TeamEntity.builder()
                .name("Team 1")
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        team = teamRepository.save(team);

        TeamEntity teamToUpdate = teamRepository.findById(team.getTeamId()).orElse(null);

        teamToUpdate.setName("Team Deutsche Bahn");
        teamToUpdate = teamRepository.save(teamToUpdate);

        TeamEntity finalTeam = teamToUpdate;
        TeamEntity finalTeam1 = team;

        assertAll(
                () -> assertEquals(1, teamRepository.count()),
                () -> assertEquals(finalTeam, teamRepository.findById(finalTeam1.getTeamId()).get())
        );
    }
}