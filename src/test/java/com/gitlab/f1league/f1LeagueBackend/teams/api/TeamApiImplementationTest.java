package com.gitlab.f1league.f1LeagueBackend.teams.api;

import com.gitlab.f1league.f1LeagueBackend.etc.AuthorizationService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Driver;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Team;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamModelConverter;
import com.gitlab.f1league.f1LeagueBackend.teams.services.TeamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TeamApiImplementationTest {
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    @LocalServerPort
    private int port;
    @MockBean
    private TeamService teamService;

    @MockBean
    private TeamModelConverter teamModelConverter;

    @MockBean
    private AuthorizationService authorizationService;

    @MockBean
    private LeagueService leagueService;

    @BeforeEach
    public void setUp() {
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("leagueEditorToken", "UhNwklP");
        when(authorizationService.isValidToken(any(), (LeagueEntity) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
        when(authorizationService.isValidToken(any(), (String) any()))
                .thenReturn(AuthorizationService.State.AUTHORIZED);
    }

    @Test
    public void deleteTeam_ById_Ok() {
        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("teamId", UUID.randomUUID())
                        .queryParam("leagueId", UUID.randomUUID());

        HttpEntity<Driver> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Driver> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity,
                        Driver.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void deleteTeam_WrongParamName_BadRequest() {
        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("driverId", UUID.randomUUID());

        HttpEntity<Driver> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Driver> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity,
                        Driver.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void postTeam_correctRequest_OkAndTeam() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity createdTeam = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .name("Deutsche Bahn")
                .build();

        Team teamToCreate = new Team();
        teamToCreate.setName(createdTeam.getName());

        Team expectedResponse = new Team();
        expectedResponse.setName(createdTeam.getName());
        expectedResponse.setTeamId(UUID.fromString(createdTeam.getTeamId()));
        expectedResponse.setPoints(0);
        expectedResponse.setListOfDriverIds(new ArrayList<>());

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueA.getLeagueId());

        HttpEntity<Team> entity = new HttpEntity<>(teamToCreate, headers);

        when(teamService.buildTeamEntityFromTeam(teamToCreate, leagueA.getLeagueId()))
                .thenReturn(createdTeam);
        when(teamService.saveTeamEntity(createdTeam))
                .thenReturn(createdTeam);
        when(teamModelConverter.internalModelToTransferModel(any())).thenReturn(expectedResponse);

        ResponseEntity<Team> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity,
                        Team.class);

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(expectedResponse, response.getBody())
        );
    }

    @Test
    public void postTeam_incorrecttRequest_BadRequest() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        Team teamToCreate = new Team();

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueA.getLeagueId());

        HttpEntity<Team> entity = new HttpEntity<>(teamToCreate, headers);

        ResponseEntity<Team> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity,
                        Team.class);

        assertAll(
                () -> assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void getTeamByTeamIdAndLeagueId_GivenTeamIdAndLeagueIdd_OkAndTeam() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToGet = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .build();

        Team expectedTeam = new Team();
        expectedTeam.setTeamId(UUID.fromString(teamToGet.getTeamId()));
        expectedTeam.setPoints(0);
        expectedTeam.setName(teamToGet.getName());
        expectedTeam.setLogoUrl(teamToGet.getLogoUrl());
        expectedTeam.setListOfDriverIds(new ArrayList<>());
        expectedTeam.setNextCar(Car.FERRARI);

        when(teamService
                .getTeamEntityByTeamIdAndLeague(teamToGet.getTeamId(), leagueA.getLeagueId()))
                .thenReturn(teamToGet);
        when(teamModelConverter.internalModelToTransferModel(teamToGet)).thenReturn(expectedTeam);
        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(leagueA);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueA.getLeagueId())
                        .queryParam("teamId", teamToGet.getTeamId());

        HttpEntity<Team> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<Team>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Team>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(List.of(expectedTeam), response.getBody())
        );
    }

    @Test
    public void getTeamByTeamIdAndLeagueId_GivenTeamIdAndLeagueIdMixed_OkAndTeam() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToGet = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .build();

        Team expectedTeam = new Team();
        expectedTeam.setTeamId(UUID.fromString(teamToGet.getTeamId()));
        expectedTeam.setPoints(0);
        expectedTeam.setName(teamToGet.getName());
        expectedTeam.setLogoUrl(teamToGet.getLogoUrl());
        expectedTeam.setListOfDriverIds(new ArrayList<>());
        expectedTeam.setNextCar(Car.FERRARI);

        when(teamService
                .getTeamEntityByTeamIdAndLeague(teamToGet.getTeamId(), leagueA.getLeagueId()))
                .thenReturn(teamToGet);
        when(teamModelConverter.internalModelToTransferModel(teamToGet)).thenReturn(expectedTeam);
        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(leagueA);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("teamId", teamToGet.getTeamId())
                        .queryParam("leagueId", leagueA.getLeagueId());

        HttpEntity<Team> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<Team>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Team>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(List.of(expectedTeam), response.getBody())
        );
    }

    @Test
    public void getTeamByTeamIdAndLeagueId_GivenLeagueId_OkAndListOfTeam() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToGet1 = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .build();

        TeamEntity teamToGet2 = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Was anderes")
                .build();

        Team expectedTeam1 = new Team();
        expectedTeam1.setTeamId(UUID.fromString(teamToGet1.getTeamId()));
        expectedTeam1.setPoints(0);
        expectedTeam1.setName(teamToGet1.getName());
        expectedTeam1.setLogoUrl(teamToGet1.getLogoUrl());
        expectedTeam1.setListOfDriverIds(new ArrayList<>());
        expectedTeam1.setNextCar(Car.FERRARI);

        Team expectedTeam2 = new Team();
        expectedTeam2.setTeamId(UUID.fromString(teamToGet2.getTeamId()));
        expectedTeam2.setPoints(4);
        expectedTeam2.setName(teamToGet2.getName());
        expectedTeam2.setLogoUrl(teamToGet2.getLogoUrl());
        expectedTeam2.setListOfDriverIds(new ArrayList<>());
        expectedTeam2.setNextCar(Car.FERRARI);

        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(leagueA);
        when(teamService
                .getAllTeamsByLeague(leagueA))
                .thenReturn(List.of(teamToGet1, teamToGet2));
        when(teamModelConverter.internalModelToTransferModel(teamToGet1)).thenReturn(expectedTeam1);
        when(teamModelConverter.internalModelToTransferModel(teamToGet2)).thenReturn(expectedTeam2);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueA.getLeagueId());

        HttpEntity<Team> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<Team>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Team>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(List.of(expectedTeam1, expectedTeam2), response.getBody())
        );
    }

    @Test
    public void getTeamByTeamIdAndLeagueId_LeagueIdWithNoTeams_NoContent() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToGet1 = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .build();

        TeamEntity teamToGet2 = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Was anderes")
                .build();

        Team expectedTeam1 = new Team();
        expectedTeam1.setTeamId(UUID.fromString(teamToGet1.getTeamId()));
        expectedTeam1.setPoints(0);
        expectedTeam1.setName(teamToGet1.getName());
        expectedTeam1.setLogoUrl(teamToGet1.getLogoUrl());
        expectedTeam1.setListOfDriverIds(new ArrayList<>());
        expectedTeam1.setNextCar(Car.FERRARI);

        Team expectedTeam2 = new Team();
        expectedTeam2.setTeamId(UUID.fromString(teamToGet2.getTeamId()));
        expectedTeam2.setPoints(4);
        expectedTeam2.setName(teamToGet2.getName());
        expectedTeam2.setLogoUrl(teamToGet2.getLogoUrl());
        expectedTeam2.setListOfDriverIds(new ArrayList<>());
        expectedTeam2.setNextCar(Car.FERRARI);

        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(leagueA);
        when(teamService
                .getAllTeamsByLeague(leagueA))
                .thenReturn(List.of());
        when(teamModelConverter.internalModelToTransferModel(teamToGet1)).thenReturn(expectedTeam1);
        when(teamModelConverter.internalModelToTransferModel(teamToGet2)).thenReturn(expectedTeam2);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueA.getLeagueId());

        HttpEntity<Team> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<Team>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Team>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void getTeamByTeamIdAndLeagueId_GivenUnknownLeagueId_BadRequest() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToGet1 = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .build();

        TeamEntity teamToGet2 = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Was anderes")
                .build();

        Team expectedTeam1 = new Team();
        expectedTeam1.setTeamId(UUID.fromString(teamToGet1.getTeamId()));
        expectedTeam1.setPoints(0);
        expectedTeam1.setName(teamToGet1.getName());
        expectedTeam1.setLogoUrl(teamToGet1.getLogoUrl());
        expectedTeam1.setListOfDriverIds(new ArrayList<>());
        expectedTeam1.setNextCar(Car.FERRARI);

        Team expectedTeam2 = new Team();
        expectedTeam2.setTeamId(UUID.fromString(teamToGet2.getTeamId()));
        expectedTeam2.setPoints(4);
        expectedTeam2.setName(teamToGet2.getName());
        expectedTeam2.setLogoUrl(teamToGet2.getLogoUrl());
        expectedTeam2.setListOfDriverIds(new ArrayList<>());
        expectedTeam2.setNextCar(Car.FERRARI);

        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(null);
        when(teamService
                .getAllTeamsByLeague(leagueA))
                .thenReturn(List.of());
        when(teamModelConverter.internalModelToTransferModel(teamToGet1)).thenReturn(expectedTeam1);
        when(teamModelConverter.internalModelToTransferModel(teamToGet2)).thenReturn(expectedTeam2);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueA.getLeagueId());

        HttpEntity<Team> entity = new HttpEntity<>(null, headers);

        ResponseEntity<List<Team>> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<Team>>() {
                        });

        assertAll(
                () -> assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void getTeamByTeamIdAndLeagueId_GivenUnknownTeam_NoContentAndNull() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToGet = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .build();

        Team expectedTeam = new Team();
        expectedTeam.setTeamId(UUID.fromString(teamToGet.getTeamId()));
        expectedTeam.setPoints(0);
        expectedTeam.setName(teamToGet.getName());
        expectedTeam.setLogoUrl(teamToGet.getLogoUrl());
        expectedTeam.setListOfDriverIds(new ArrayList<>());
        expectedTeam.setNextCar(Car.FERRARI);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueA.getLeagueId())
                        .queryParam("teamId", teamToGet.getTeamId());

        HttpEntity<Team> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Team> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        Team.class);

        assertAll(
                () -> assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void getTeamByTeamIdAndLeagueId_GivenTeamIdAndNoLeagueId_BadRequest() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToGet = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .build();

        Team expectedTeam = new Team();

        when(teamService
                .getTeamEntityByTeamIdAndLeague(teamToGet.getTeamId(), leagueA.getLeagueId()))
                .thenReturn(teamToGet);
        when(teamModelConverter.internalModelToTransferModel(teamToGet)).thenReturn(expectedTeam);
        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("teamId", teamToGet.getTeamId());

        HttpEntity<Team> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Team> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        Team.class);

        assertAll(
                () -> assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode()),
                () -> assertEquals(expectedTeam, response.getBody())
        );
    }

    @Test
    public void getTeamByTeamIdAndLeagueId_GivenNoTeamIdAndLeagueIdd_NoContentt() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToGet = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .build();

        Team expectedTeam = new Team();

        when(teamService
                .getTeamEntityByTeamIdAndLeague(teamToGet.getTeamId(), leagueA.getLeagueId()))
                .thenReturn(teamToGet);
        when(teamModelConverter.internalModelToTransferModel(teamToGet)).thenReturn(expectedTeam);
        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(leagueA);

        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueA.getLeagueId());

        HttpEntity<Team> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Team> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        Team.class);

        assertAll(
                () -> assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    @Test
    public void getTeamByTeamIdAndLeagueId_GivenUnknownTeamIdAndLeagueIdd_BadRequest() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToGet = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .build();

        Team expectedTeam = new Team();

        when(teamService
                .getTeamEntityByTeamIdAndLeague(teamToGet.getTeamId(), leagueA.getLeagueId()))
                .thenReturn(null);
        when(teamModelConverter.internalModelToTransferModel(teamToGet)).thenReturn(expectedTeam);
        UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl(createURLWithPort("team"))
                        .queryParam("leagueId", leagueA.getLeagueId())
                        .queryParam("teamId", teamToGet.getTeamId());


        HttpEntity<Team> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Team> response =
                restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,
                        Team.class);

        assertAll(
                () -> assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode()),
                () -> assertNull(response.getBody())
        );
    }

    // todo implement
    @Test
    public void patchTeam_givenTeam_OkAndCorrectTeam() {
    }

    // todo implement
    @Test
    public void patchTeam_givenInvalidTeam_BadRequestAndNull() {

    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/" + uri;
    }
}