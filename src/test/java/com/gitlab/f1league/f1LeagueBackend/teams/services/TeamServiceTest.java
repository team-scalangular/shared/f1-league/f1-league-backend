package com.gitlab.f1league.f1LeagueBackend.teams.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.drivers.service.DriverService;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Team;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.league.services.LeagueService;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.repository.TeamRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TeamServiceTest {

    TeamService teamService;

    @Mock
    LeagueService leagueService;

    @Mock
    TeamRepository teamRepository;

    @Mock
    DriverService driverService;

    @Mock
    TeamClassificationCacheService teamClassificationCacheService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        teamService = new TeamService(teamRepository, driverService);
        teamService.setLeagueService(leagueService);
        teamService.setTeamClassificationCacheService(teamClassificationCacheService);
    }

    @Test
    public void getTeamEntityByTeamIdAndLeague_KnownLeagueAndTeam_CorrectTeam() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(leagueA);
        when(teamRepository.findByTeamIdAndLeague(teamEntity.getTeamId(), leagueA))
                .thenReturn(teamEntity);

        TeamEntity foundTeamEntity = teamService
                .getTeamEntityByTeamIdAndLeague(teamEntity.getTeamId(), leagueA.getLeagueId());

        assertEquals(teamEntity, foundTeamEntity);
    }

    @Test
    public void getTeamEntityByTeamIdAndLeague_UnknownLeagueAndTeam_NullTeam() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(null);
        when(teamRepository.findByTeamIdAndLeague(teamEntity.getTeamId(), leagueA))
                .thenReturn(null);

        TeamEntity foundTeamEntity = teamService
                .getTeamEntityByTeamIdAndLeague(teamEntity.getTeamId(), leagueA.getLeagueId());

        assertNull(foundTeamEntity);
    }

    @Test
    public void getTeamEntityByTeamIdAndLeague_UnknownLeagueAndKnownTeam_NullTeam() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(null)
                .logoUrl("www.google.com")
                .build();

        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(null);
        when(teamRepository.findByTeamIdAndLeague(teamEntity.getTeamId(), leagueA))
                .thenReturn(teamEntity);

        TeamEntity foundTeamEntity = teamService
                .getTeamEntityByTeamIdAndLeague(teamEntity.getTeamId(), leagueA.getLeagueId());

        assertNull(foundTeamEntity);
    }

    @Test
    public void getTeamEntityByTeamIdAndLeague_KnownLeagueAndUnknownTeam_NullTeam() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(null)
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        when(leagueService.getLeagueById(leagueA.getLeagueId())).thenReturn(leagueA);
        when(teamRepository.findByTeamIdAndLeague(teamEntity.getTeamId(), leagueA))
                .thenReturn(null);

        TeamEntity foundTeamEntity = teamService
                .getTeamEntityByTeamIdAndLeague(teamEntity.getTeamId(), leagueA.getLeagueId());

        assertNull(foundTeamEntity);
    }

    @Test
    public void createTeamFromPost_knownLeague_createdCorrectly() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity expectedTeam = TeamEntity
                .builder()
                .logoUrl(null)
                .league(leagueA)
                .name("Deutsche Bahn")
                .build();

        Team teamToCreate = new Team();
        teamToCreate.setName("Deutsche Bahn");

        when(leagueService.getLeagueById(leagueA.getLeagueId()))
                .thenReturn(leagueA);

        TeamEntity createdTeamEntity =
                teamService.buildTeamEntityFromTeam(teamToCreate, leagueA.getLeagueId());

        assertEquals(expectedTeam, createdTeamEntity);
    }

    @Test
    public void createTeamFromPost_unknownLeague_NullReturned() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        Team teamToCreate = new Team();
        teamToCreate.setName("Deutsche Bahn");

        when(leagueService.getLeagueById(leagueA.getLeagueId()))
                .thenReturn(null);

        TeamEntity createdTeamEntity =
                teamService.buildTeamEntityFromTeam(teamToCreate, leagueA.getLeagueId());

        assertNull(createdTeamEntity);
    }

    @Test
    public void createTeamFromPost_noNameProvided_NotSaved() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        Team teamToCreate = new Team();

        when(leagueService.getLeagueById(leagueA.getLeagueId()))
                .thenReturn(leagueA);

        TeamEntity createdTeamEntity =
                teamService.buildTeamEntityFromTeam(teamToCreate, leagueA.getLeagueId());

        assertNull(createdTeamEntity);
    }

    @Test
    public void updateTeam_givenValidTeamWithUpdates_expectSuccess() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        Team teamWithUpdates = new Team();
        teamWithUpdates.setName("Deutsche Bahn");
        teamWithUpdates.setLogoUrl("www.deutschebahn.com");

        TeamEntity expectedTeam = TeamEntity.builder()
                .name(teamWithUpdates.getName())
                .teamId(teamEntity.getTeamId())
                .league(leagueA)
                .logoUrl(teamWithUpdates.getLogoUrl())
                .build();

        TeamEntity updatedTeamEntity = teamService.updateTeam(teamEntity, teamWithUpdates);

        assertEquals(expectedTeam.toString(), updatedTeamEntity.toString());
    }

    @Test
    public void calculateTeamPoints_DriverGiven_27Expected() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamToTest = TeamEntity.builder()
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .build();

        DriverEntity driverEntity1 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(teamToTest)
                .build();
        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(teamToTest)
                .build();

        when(teamRepository.findById(teamToTest.getTeamId())).thenReturn(
                java.util.Optional.of(teamToTest));
        when(driverService.getDriversByTeamEntity(teamToTest))
                .thenReturn(List.of(driverEntity1, driverEntity2));
        when(driverService.calculateADriversTotalPoints(driverEntity1)).thenReturn(15);
        when(driverService.calculateADriversTotalPoints(driverEntity2)).thenReturn(12);
        when(driverService.calculateADriversTotalPoints(driverEntity1, 1)).thenReturn(7);
        when(driverService.calculateADriversTotalPoints(driverEntity2, 1)).thenReturn(10);

        assertAll(
                () -> assertEquals(27, teamService.getTeamPoints(teamToTest)),
                () -> assertEquals(17, teamService.getTeamPoints(teamToTest, 1))
        );
    }

    @Test
    public void getTeamPosition_validInput_Correct() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity1 = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        TeamEntity teamEntity2 = TeamEntity.builder()
                .name("Team 2")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        DriverEntity driverEntity1 = DriverEntity.builder()
                .name("d1")
                .build();
        DriverEntity driverEntity2 = DriverEntity.builder()
                .name("d2")
                .build();

        when(teamRepository.findAllByLeague(leagueA)).thenReturn(List.of(teamEntity1, teamEntity2));
        when(driverService.getDriversByTeamEntity(teamEntity1)).thenReturn(List.of(driverEntity1));
        when(driverService.getDriversByTeamEntity(teamEntity2)).thenReturn(List.of(driverEntity2));
        when(driverService.calculateADriversTotalPoints(driverEntity1)).thenReturn(10);
        when(driverService.calculateADriversTotalPoints(driverEntity2)).thenReturn(11);

        when(teamClassificationCacheService.getTeamClassification(leagueA))
                .thenReturn(List.of(teamEntity2, teamEntity1));

        assertAll(
                () -> assertEquals(List.of(teamEntity2, teamEntity1),
                        teamService.calculateTeamClassification(teamEntity1.getLeague())),
                () -> assertEquals(1, teamService.getTeamPosition(teamEntity2)),
                () -> assertEquals(2, teamService.getTeamPosition(teamEntity1))
        );
    }

    @Test
    public void updateTeam_givenInvalidTeamWithUpdates_expectFailure() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        Team teamWithUpdates = new Team();
        teamWithUpdates.setName("");
        teamWithUpdates.setLogoUrl("www.deutschebahn.com");

        TeamEntity updatedTeamEntity = teamService.updateTeam(teamEntity, teamWithUpdates);

        assertNull(updatedTeamEntity);
    }

    @Test
    public void deleteTeam_givenTeam_expectDriversDeleted() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        DriverEntity driverEntity1 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(teamEntity)
                .build();
        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(teamEntity)
                .build();

        when(teamRepository.findById(teamEntity.getTeamId())).thenReturn(
                java.util.Optional.of(teamEntity));
        when(driverService.getDriversByTeamEntity(teamEntity))
                .thenReturn(List.of(driverEntity1, driverEntity2));

        teamService.deleteTeamById(teamEntity.getTeamId());

        assertAll(
                () -> verify(driverService, times(1)).getDriversByTeamEntity(teamEntity),
                () -> assertNull(driverEntity1.getTeam()),
                () -> assertNull(driverEntity2.getTeam())
        );
    }

    @Test
    public void deleteTeam_givenNoTeam_expectDriversDeleted() {
        LeagueEntity leagueA = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity teamEntity = TeamEntity.builder()
                .name("Team 1")
                .teamId(UUID.randomUUID().toString())
                .league(leagueA)
                .logoUrl("www.google.com")
                .build();

        DriverEntity driverEntity1 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(teamEntity)
                .build();
        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(teamEntity)
                .build();

        when(teamRepository.findById(teamEntity.getTeamId())).thenReturn(
                Optional.empty());
        when(driverService.getDriversByTeamEntity(teamEntity))
                .thenReturn(List.of(driverEntity1, driverEntity2));

        teamService.deleteTeamById(teamEntity.getTeamId());
    }

    @Test
    public void findTeamsByLeague_League_ListOfTeams() {
        LeagueEntity leagueEntity = LeagueEntity.builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        List<TeamEntity> teamEntities = List.of(TeamEntity.builder()
                .teamId(UUID.randomUUID().toString())
                .build());

        when(teamRepository.findAllByLeague(leagueEntity)).thenReturn(teamEntities);

        assertEquals(teamEntities, teamService.getAllTeamsByLeague(leagueEntity));
    }

    @Test
    public void getTeamsSortedByStandingInLeague_LeagueAndTeams_CorrectSorted() {
        LeagueEntity league = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity team1 = TeamEntity.builder()
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .teamId(UUID.randomUUID().toString())
                .league(league)
                .build();

        DriverEntity driverEntity1 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(team1)
                .build();
        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(team1)
                .build();

        TeamEntity team2 = TeamEntity.builder()
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .teamId(UUID.randomUUID().toString())
                .league(league)
                .build();

        DriverEntity driverEntity3 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(team2)
                .build();
        DriverEntity driverEntity4 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(team2)
                .build();

        when(teamRepository.findAllByLeague(league)).thenReturn(List.of(team2, team1));
        when(teamRepository.findById(team1.getTeamId())).thenReturn(
                java.util.Optional.of(team1));
        when(teamRepository.findById(team2.getTeamId())).thenReturn(
                java.util.Optional.of(team2));
        when(driverService.getDriversByTeamEntity(team1))
                .thenReturn(List.of(driverEntity1, driverEntity2));
        when(driverService.calculateADriversTotalPoints(driverEntity1)).thenReturn(8);
        when(driverService.calculateADriversTotalPoints(driverEntity2)).thenReturn(12);
        when(driverService.getDriversByTeamEntity(team2))
                .thenReturn(List.of(driverEntity3, driverEntity4));
        when(driverService.calculateADriversTotalPoints(driverEntity3)).thenReturn(8);
        when(driverService.calculateADriversTotalPoints(driverEntity4)).thenReturn(9);

        assertEquals(List.of(team1, team2), teamService.calculateTeamClassification(league));
    }

    @ParameterizedTest
    @CsvSource(
            {"1, WILLIAMS", "2, ALPHA_ROMEO", "3, TORRO_ROSSO", "4, RENAULT", "5, RACING_POINT",
                    "6, MCLAREN", "7, HAAS", "8, RED_BULL", "9, FERRARI",
                    "10, MERCEDES, -11 WILLIAMS"})
    public void getTeamsSortedByStandingInLeague_LeagueAndTeams_CorrectSorted(int position,
                                                                              Car car) {
        assertEquals(car, teamService.getCarBelongingToPosition(position));
    }

    @Test
    public void getTeamsNextCar_LeagueAndTeams_CorrectSorted() {
        LeagueEntity league = LeagueEntity
                .builder()
                .leagueId(UUID.randomUUID().toString())
                .build();

        TeamEntity team1 = TeamEntity.builder()
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .teamId(UUID.randomUUID().toString())
                .league(league)
                .build();

        DriverEntity driverEntity1 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(team1)
                .build();
        DriverEntity driverEntity2 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(team1)
                .build();

        TeamEntity team2 = TeamEntity.builder()
                .logoUrl("www.google.com")
                .name("Deutsche Bahn")
                .teamId(UUID.randomUUID().toString())
                .league(league)
                .build();

        DriverEntity driverEntity3 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Max")
                .surename("Bieleke")
                .team(team2)
                .build();
        DriverEntity driverEntity4 = DriverEntity.builder()
                .driverId(UUID.randomUUID().toString())
                .name("Moritz")
                .surename("Lindner")
                .team(team2)
                .build();

        when(teamRepository.findAllByLeague(league)).thenReturn(List.of(team2, team1));
        when(teamRepository.findById(team1.getTeamId())).thenReturn(
                java.util.Optional.of(team1));
        when(teamRepository.findById(team2.getTeamId())).thenReturn(
                java.util.Optional.of(team2));
        when(driverService.getDriversByTeamEntity(team1))
                .thenReturn(List.of(driverEntity1, driverEntity2));
        when(driverService.calculateADriversTotalPoints(driverEntity1)).thenReturn(8);
        when(driverService.calculateADriversTotalPoints(driverEntity2)).thenReturn(12);
        when(driverService.getDriversByTeamEntity(team2))
                .thenReturn(List.of(driverEntity3, driverEntity4));
        when(driverService.calculateADriversTotalPoints(driverEntity3)).thenReturn(8);
        when(driverService.calculateADriversTotalPoints(driverEntity4)).thenReturn(9);

        when(teamClassificationCacheService.getTeamClassification(league))
                .thenReturn(List.of(team1, team2));

        assertAll(
                () -> assertEquals(Car.WILLIAMS, teamService.getTeamsNextCar(team1)),
                () -> assertEquals(Car.ALPHA_ROMEO, teamService.getTeamsNextCar(team2))
        );
    }

    @Test
    public void findTeamEntityByTeam_ValidTeam_Success() {
        TeamEntity expectedTeamEntity = TeamEntity.builder()
                .teamId(UUID.randomUUID().toString())
                .build();

        Team teamToGetEntityFrom = new Team();
        teamToGetEntityFrom.setTeamId(UUID.fromString(expectedTeamEntity.getTeamId()));

        when(teamRepository.findById(expectedTeamEntity.getTeamId()))
                .thenReturn(Optional.of(expectedTeamEntity));

        assertEquals(Optional.of(expectedTeamEntity),
                teamService.findTeamEntityByTeam(teamToGetEntityFrom));
    }

    @Test
    public void save_TeamEntity_Success() {
        TeamEntity teamEntityToSave = TeamEntity.builder()
                .teamId(UUID.randomUUID().toString())
                .build();

        when(teamRepository.save(teamEntityToSave)).thenReturn(teamEntityToSave);

        assertEquals(teamEntityToSave, teamService.saveTeamEntity(teamEntityToSave));
    }

    @Test
    public void getTeamById_teamId_Success() {
        String teamId = UUID.randomUUID().toString();
        TeamEntity teamEntity = TeamEntity.builder()
                .teamId(teamId)
                .build();

        when(teamRepository.findById(teamId)).thenReturn(Optional.of(teamEntity));

        assertEquals(teamEntity, teamService.getTeamById(teamId));
    }
}