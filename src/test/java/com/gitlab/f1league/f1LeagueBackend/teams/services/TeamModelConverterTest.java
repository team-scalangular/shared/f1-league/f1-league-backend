package com.gitlab.f1league.f1LeagueBackend.teams.services;

import com.gitlab.f1league.f1LeagueBackend.drivers.entity.DriverEntity;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Car;
import com.gitlab.f1league.f1LeagueBackend.generated.transfermodel.Team;
import com.gitlab.f1league.f1LeagueBackend.league.entity.LeagueEntity;
import com.gitlab.f1league.f1LeagueBackend.teams.entity.TeamEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class TeamModelConverterTest {
    TeamModelConverter teamModelConverter;

    @MockBean
    TeamService teamService;
    private TeamEntity teamA;
    private LeagueEntity leagueA, leagueB;

    @BeforeEach
    public void setUp() {
        teamModelConverter = new TeamModelConverter(teamService);

        leagueA = LeagueEntity
                .builder()
                .build();
        leagueB = LeagueEntity
                .builder()
                .build();

        teamA = TeamEntity
                .builder()
                .teamId(UUID.randomUUID().toString())
                .logoUrl("www.google.com")
                .league(leagueA)
                .name("TeamA")
                .build();
    }

    @Test
    public void convertTToE_KnownTeam_CorrectlyConverted() {
        Team team = new Team();
        team.setListOfDriverIds(
                List.of(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        team.setName("TeamA");
        team.setLogoUrl("www.google.com");
        team.setNextCar(Car.FERRARI);
        team.setPoints(5);
        team.setTeamId(UUID.fromString(teamA.getTeamId()));

        when(teamService.findTeamEntityByTeam(team)).thenReturn(Optional.of(teamA));

        TeamEntity convertedTeam = teamModelConverter.transferModelToInternalModel(team);

        assertEquals(teamA, convertedTeam);
    }

    @Test
    public void convertTToE_UnknownTeam_CorrectlyConverted() {
        teamA.setLeague(null);

        Team team = new Team();
        team.setListOfDriverIds(
                List.of(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        team.setName("TeamA");
        team.setLogoUrl("www.google.com");
        team.setNextCar(Car.FERRARI);
        team.setPoints(5);
        team.setTeamId(UUID.fromString(teamA.getTeamId()));

        when(teamService.findTeamEntityByTeam(team)).thenReturn(Optional.empty());

        TeamEntity convertedTeam = teamModelConverter.transferModelToInternalModel(team);

        assertEquals(teamA, convertedTeam);
    }

    @Test
    public void convertEToT_KnownTeam_CorrectlyConverted() {
        List<DriverEntity> driverEntities =
                List.of(DriverEntity.builder().build(), DriverEntity.builder().build());
        driverEntities.forEach(driver -> driver.setDriverId(UUID.randomUUID().toString()));

        when(teamService.getTeamPoints(teamA)).thenReturn(5);
        when(teamService.getTeamsNextCar(teamA)).thenReturn(Car.FERRARI);
        when(teamService.getATeamsDrivers(teamA))
                .thenReturn(driverEntities);

        Team expectedTeam = new Team();
        expectedTeam.setNextCar(Car.FERRARI);
        expectedTeam.setPoints(5);
        expectedTeam.setPosition(0);
        expectedTeam.setName(teamA.getName());
        expectedTeam.setLogoUrl(teamA.getLogoUrl());
        expectedTeam.setTeamId(UUID.fromString(teamA.getTeamId()));
        expectedTeam.setListOfDriverIds(
                driverEntities.stream().map(DriverEntity::getDriverId)
                        .collect(Collectors.toList()));

        Team convertedTeam = teamModelConverter.internalModelToTransferModel(teamA);

        assertEquals(expectedTeam, convertedTeam);
    }
}